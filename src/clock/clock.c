#include <sys/time.h>
#include <stdio.h>

#include "clock.h"

static double get_elapsed_seconds_since_epoch()
{
  struct timeval tv;

  gettimeofday(&tv, NULL);
  return (double)tv.tv_sec + ((double)tv.tv_usec * 1e-6);
}

void	clock_init(my_clock_t *clock)
{
  clock->start = get_elapsed_seconds_since_epoch();
}

void	clock_reset(my_clock_t *clock)
{
  clock->start = get_elapsed_seconds_since_epoch();
}

double	clock_get_elapsed_seconds(my_clock_t const *clock)
{
  return get_elapsed_seconds_since_epoch() - clock->start;
}
