#ifndef __CLOCK_H__
# define __CLOCK_H__

typedef struct	my_clock_s
{
  double	start;
}		my_clock_t;

void	clock_init(my_clock_t *clock);
void	clock_reset(my_clock_t *clock);
double	clock_get_elapsed_seconds(my_clock_t const *clock);

#endif
