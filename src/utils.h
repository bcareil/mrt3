#pragma once
#ifndef __UTILS_H__
# define __UTILS_H__

# include <stdlib.h>
# include <math.h>

# define STRUCT_ALLIGNED __attribute__((__aligned__((16))))

# define EPSILON 0.0001

# define MIN(a, b) (((a) < (b)) ? (a) : (b))
# define MAX(a, b) (((a) > (b)) ? (a) : (b))

# define POW2(a) ((a) * (a))

# define DEG_TO_RAD(d) ((d) * (M_PI / 180.0))

# define SIZEOF_ARRAY(a) (sizeof(a) / sizeof(*a))

# define DELTA(a, b, c) (POW2(b) - (4.f * (a) * (c)))

typedef enum axis_e {
	AXIS_X,
	AXIS_Y,
	AXIS_Z,

	AXIS_COUNT
} axis_t;

void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t new_size);

/**
 ** Allocate a buffer of @a n bytes and initialize
 ** it with the first n bytes of @a s.
 **
 ** If the allocation fail, abort is called.
 */
void *xmemdup(void *s, size_t n);

/**
 ** Copy at most @a dest_size - 1 bytes of the string inside
 ** @a src into @a dest or stop at the first nul-byte found in
 ** src. dest will be terminated by a nul-byte in any cases.
 */
size_t my_strlcpy(char *dest, char const *src, size_t dest_size);

#endif
