#ifndef __WAVEFORM_H__
# define __WAVEFORM_H__

# include <stdlib.h>
# include <stdio.h>

#include "vect/vect.h"
#include "scene/scene.h"

typedef struct wf_material_s {
	char name[64];
	color_t ambiant_color;
	color_t diffuse_color;
	color_t specular_color;
	float ns;
	float ni;
	float d;
	int illum;
} wf_material_t;

typedef struct wf_material_ref_s {
	size_t material_idx;
	size_t faces_start;
	size_t faces_stop;
} wf_material_ref_t;

typedef struct wf_object_s {
	char name[64];
	int smooth;
	size_t vertices_start;
	size_t vertices_stop;
	size_t faces_start;
	size_t faces_stop;
	size_t normals_start;
	size_t normals_stop;
	size_t nb_material_refs;
	wf_material_ref_t material_refs[64];
} wf_object_t;

typedef struct wf_face_s {
	int has_normals;
	size_t v0;
	size_t n0;
	size_t v1;
	size_t n1;
	size_t v2;
	size_t n2;
} wf_face_t;

typedef struct waveform_obj_s {
	size_t nb_objects;
	size_t nb_vertices;
	size_t nb_faces;
	size_t nb_materials;
	size_t nb_normals;
	wf_object_t *objects;
	vect3_t *vertices;
	wf_face_t *faces;
	wf_material_t *materials;
	vect3_t *normals;
} waveform_obj_t;

int waveform_mtl_parse_file(waveform_obj_t *result, FILE *file);
int waveform_obj_parse_file(waveform_obj_t *result, FILE *file);
void clean_waveform_obj(waveform_obj_t *wf_obj);

void waveform_obj_to_scene(scene_t *scene, waveform_obj_t *wf_obj);

#endif
