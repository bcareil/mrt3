#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <assert.h>

#include "waveform/waveform.h"

#include "utils.h"

#define MAX_OBJECTS   32
#define MAX_VERTICES  102400
#define MAX_NORMALS   102400
#define MAX_FACES     51200
#define MAX_MATERIALS 64

/****************************************
 * Line consumption utilities
****************************************/

static char const * const WF_READ_FLOAT_ERROR_STR[] = {
  "Success",
  "Expected more value to read",
  "Could not convert one of the values",
  "Conversion caused over or underflow"
};

static int wf_read_float(float *array, size_t nb)
{
  size_t i;
  char *start;
  char *stop;

  for (i = 0; i < nb; ++i) {
    float result;

    start = strtok(NULL, " ");
    if (start == NULL) {
      return 1;
    }

    errno = 0;
    result = strtof(start, &stop);
    if (start == stop) {
      return 2;
    }
    if (errno == ERANGE) {
      return 3;
    }

    array[i] = result;
  }

  return 0;
}

static int wf_read_integer(int *array, size_t nb)
{
  size_t i;
  char *start;
  char *stop;

  for (i = 0; i < nb; ++i) {
    long int result;

    start = strtok(NULL, " ");
    if (start == NULL) {
      return 1;
    }

    errno = 0;
    result = strtol(start, &stop, 10);
    if (start == stop) {
      return 2;
    } else if (errno == ERANGE) {
      return 3;
    }

    array[i] = (int)result;
  }

  return 0;
}

static int wf_read_to_eol(char **str)
{
  char *tmp;

  tmp = strtok(NULL, "\n");
  if (tmp == NULL) {
    return 1;
  }
  *str = tmp;
  return 0;
}

static int wf_read_eol()
{
  char *tmp;

  tmp = strtok(NULL, "");
  return (tmp == NULL || tmp[0] == '\n' || tmp[0] == '\0');
}

static int wf_read_1f(float *f)
{
  int error;
  float read_f;

  error = wf_read_float(&read_f, 1);
  if (error) {
    fprintf(
        stderr,
        "wf_read_float: %s\n", WF_READ_FLOAT_ERROR_STR[error]
        );
    return 1;
  }

  if (wf_read_eol() == 0) {
    fprintf(stderr, "Expected EOL after 1 float\n");
    return 1;
  }

  *f = read_f;
  return 0;
}

static int wf_read_color(color_t *color)
{
  int error;
  size_t i;
  float components[3];

  error = wf_read_float(components, 3);
  if (error) {
    fprintf(
        stderr,
        "wf_read_float: %s\n"
        "wf_read_color: Expected 3 floats\n",
        WF_READ_FLOAT_ERROR_STR[error]
        );
    return 1;
  }

  for (i = 0; i < 3; ++i) {
    if (components[i] < 0.f || components[i] > 1.f) {
      fprintf(
          stderr,
          "wf_read_color: Expected float value between 0 and 1. "
          "Got %.3f at component #%zu\n",
          components[i], i
          );
      return 1;
    }
  }

  if (wf_read_eol() == 0) {
    fprintf(stderr, "Expected EOL after 3 floats\n");
    return 1;
  }

  COLOR_INIT((*color), components[0], components[1], components[2]);

  return 0;
}

/****************************************
 * Waveform .obj parsing functions
****************************************/

static int wf_obj_finalize_prev_material_ref(waveform_obj_t *result, wf_object_t *object)
{
  wf_material_ref_t *material_ref;

  // check for material ref
  if (object->nb_material_refs <= 0) {
    return 1;
  }
  material_ref = &object->material_refs[object->nb_material_refs - 1];

  if (result->nb_faces == material_ref->faces_start) {
    fprintf(stdout, "warning: material reference not associated to any faces.\n");
    object->nb_material_refs -= 1;
    return 2;
  }
  material_ref->faces_stop = result->nb_faces;
  return 0;
}

static int wf_finalize_prev_object(waveform_obj_t *result)
{
  wf_object_t *object;

  if (result->nb_objects <= 0) {
    fprintf(stderr, "wf_finalize_prev_object: no prev object\n");
    return 0x110;
  }
  object = &result->objects[result->nb_objects - 1];
  if (object->vertices_start == result->nb_vertices) {
    fprintf(stderr, "wf_finalize_prev_object: object %s has no vertices\n",
        object->name);
    return 0x120;
  }
  if (object->faces_start == result->nb_faces) {
    fprintf(stderr, "wf_finalize_prev_object: object %s has no faces\n",
        object->name);
    return 0x130;
  }

  object->vertices_stop = result->nb_vertices;
  object->faces_stop = result->nb_faces;
  object->normals_stop = result->nb_normals;

  if (object->smooth &&
      ((object->vertices_stop - object->vertices_start) !=
       (object->normals_stop - object->normals_start))) {
    fprintf(stderr, "wf_finalize_prev_object: expected the same number of "
        "vertices than normals\n");
    fprintf(stderr, "wf_finalize_prev_object: got %zu vertices and %zu "
        "normals for object %s\n",
        (object->vertices_stop - object->vertices_start),
        (object->normals_stop - object->normals_start),
        object->name);
    //return 0x140;
  }

  wf_obj_finalize_prev_material_ref(result, object);
  return 0;
}

static int wf_read_object(waveform_obj_t *result)
{
  char *name;
  wf_object_t *object;

  if (wf_read_to_eol(&name)) {
    fprintf(stderr, "Object defenition has no name\n");
    return 0x210;
  } else if (result->nb_objects >= MAX_OBJECTS) {
    fprintf(stderr, "Max objects defenition of %u reached\n", MAX_OBJECTS);
    return 0x220;
  } else if (result->nb_objects > 0) {
    int error = wf_finalize_prev_object(result);
    if (error) {
      return error;
    }
  }
  object = &result->objects[result->nb_objects];

  strncpy(object->name, name, sizeof(object->name) - 1);
  object->name[sizeof(object->name) - 1] = '\0';
  object->smooth = 0;
  object->vertices_start = result->nb_vertices;
  object->vertices_stop = 0;
  object->faces_start = result->nb_faces;
  object->faces_stop = 0;
  object->normals_start = result->nb_normals;
  object->normals_stop = 0;
  object->nb_material_refs = 0;

  result->nb_objects += 1;
  return 0;
}

static int wf_read_vertice(waveform_obj_t *result)
{
  int error;
  float components[4];
  vect3_t *vect;

  if (result->nb_vertices >= MAX_VERTICES) {
    fprintf(stderr, "Max vertices definition of %u reached\n", MAX_VERTICES);
    return 0x310;
  }

  // read x, y and z components
  error = wf_read_float(components, 3);
  switch (error) {
    case 1:
      fprintf(stderr, "Expected at least three floats\n");
      return 0x320;
    case 2:
    case 3:
      fprintf(stderr, "Conversion error: %s\n", WF_READ_FLOAT_ERROR_STR[error]);
      return 0x330;
    default:
      break;
  }

  // read optional w component
  error = wf_read_float(&components[3], 1);
  switch (error) {
    case 0:
      break;
    case 2:
    case 3:
      fprintf(stderr, "Conversion error: %s\n", WF_READ_FLOAT_ERROR_STR[error]);
      return 0x340;
    default:
      components[3] = 1.0;
  }

  if (wf_read_eol() == 0) {
    fprintf(stderr, "Expected end of line\n");
    return 0x350;
  }

  // store extracted values
  vect = &result->vertices[result->nb_vertices];
  vect->x = components[0];
  vect->y = components[1];
  vect->z = components[2];
  vect->w = components[3];
  result->nb_vertices += 1;

  return 0;
}

static int wf_add_face(
    waveform_obj_t *result,
    int has_normals,
    size_t v0, size_t v1, size_t v2,
    size_t n0, size_t n1, size_t n2
    )
{
  wf_object_t *object;
  wf_face_t *face;
  size_t min;
  size_t max;

  if (result->nb_faces >= MAX_FACES) {
    fprintf(stderr, "Max faces definitions of %u reached\n", MAX_FACES);
    return 0x410;
  } else if (result->nb_objects <= 0) {
    fprintf(stderr, "No object to associate the face with\n");
    return 0x420;
  }

  // check vertices indices validity
  object = &result->objects[result->nb_objects - 1];
  min = object->vertices_start;
  max = result->nb_vertices;
  if (v0 < min || v0 >= max ||
      v1 < min || v1 >= max ||
      v2 < min || v2 >= max) {
    fprintf(stderr, "face %zu %zu %zu refers to vertices which are "
        "uninitialized or not associated with the current object "
        "(%zu, %zu)\n",
        v0, v1, v2, min, max);
    return 0x430;
  }

  // check normals indices validity
  if (has_normals) {
    min = object->normals_start;
    max = result->nb_normals;
    if (n0 < min || n0 > max ||
        n1 < min || n1 > max ||
        n2 < min || n2 > max) {
      fprintf(
          stderr, "Face definition refer to normals which are "
          "uninitialized or not associated with the current object "
          );
      return 0x440;
    }
  }

  face = &result->faces[result->nb_faces];
  face->has_normals = has_normals;
  face->v0 = v0;
  face->n0 = n0;
  face->v1 = v1;
  face->n1 = n1;
  face->v2 = v2;
  face->n2 = n2;
  result->nb_faces += 1;

  return 0;
}

static int wf_read_face(waveform_obj_t *result)
{ 
  size_t i;
  size_t vertices[4];
  size_t normals[4];
  size_t texture_coordinates[4];
  size_t has_texture_coordinates;
  size_t has_normals;
  char *word;
  char *start;
  char *stop;

  // TODO: actually use extracted texture coordinates
  (void) texture_coordinates;

  has_texture_coordinates = 0;
  has_normals = 0;
  // read up to 4 vertices references
  for (i = 0; i < 4; ++i) {
    size_t j;
    long values[3];

    // get a word, either "[0-9]+" or "[0-9]+//[0-9]+" are supported
    word = strtok(NULL, " \n");
    if (word == NULL && i == 3) {
      break; // only three values on this line
    } else if (word == NULL) {
      fprintf(stderr, "Expected a number, got EOL\n");
      return 0x510;
    }

    // read one or more two integer, separated by slashes
    start = word;
    stop = NULL;
    bzero(values, sizeof(values));
    j = 0;
    while (j < 3) {
      long v;

      // read an integer
      errno = 0;
      v = strtol(start, &stop, 10);
      if (start == stop) {
        if (i == 3 && start[0] == '\n') {
          break;
        }
        fprintf(stderr, "Could not convert '%s' to an unsigned integer\n", start);
        return 0x520;
      } else if (errno == ERANGE) {
        fprintf(stderr, "Value '%s' caused an over or underflow\n", start);
        return 0x530;
      } else if (v <= 0) {
        fprintf(stderr, "Invalid vertex indice %li\n", v);
        return 0x540;
      }
      values[j] = v;
      ++j;

      // check for end of definition
      start = stop;
      if (start[0] == '\0') {
        break;      // no more numbers
      } else if (j == 2) {
        // there should not be any number left
        fprintf(stderr, "Expected space got '%s'\n", start);
        return 0x550;
      }
      
      // try to read slashes
      if (start[0] != '/') {
        fprintf(stderr, "Expected a /, got '%s'\n", start);
        return 0x560;
      }
      
      // skip slash
      start += 1;
      if (j == 1 && start[0] == '/') { // second slash in a row
        // skip a number, there is no texture coordiates
        start += 1;
        values[j] = -1;
        j += 1; 
      }
    }

    // store extracted values
    assert(j > 0);
    vertices[i] = (size_t)(values[0] - 1);
    if (j >= 1 && values[1] > 0) {
      has_texture_coordinates += 1;
      texture_coordinates[i] = (size_t)(values[1] - 1);
    }
    if (j >= 2 && values[2] > 0) {
      has_normals += 1;
      normals[i] = (size_t)(values[2] - 1);
    }
  }

  // be sure we reached EOL
  if (wf_read_eol() == 0) {
    fprintf(stderr, "Expected end of line\n");
    return 0x570;
  }

  // we need at least three vertices
  if (i < 3) {
    fprintf(stderr, "Not enough vertices (%zu) for a face\n", i);
    return 0x580;
  }

  // check texture coordinates and normals consistency
  // (there must be as much of each as vertices, or none)
  if ((has_texture_coordinates != 0 && has_texture_coordinates != i)
      || (has_normals != 0 && has_normals != i)) {
    fprintf(stderr, "Inconsistent face definition\n");
    return 0x590;
  }

  // store in data structure (will check indices validity)
  {
    int error;

    if (i == 3) {
      error = wf_add_face(
          result, has_normals == i,
          vertices[0], vertices[1], vertices[2],
          normals[0], normals[1], normals[2]
          );
    } else {
      error = wf_add_face(
          result, has_normals == i,
          vertices[0], vertices[1], vertices[3],
          normals[0], normals[1], normals[3]
          );
      if (error) {
        return error;
      }
      error = wf_add_face(
          result, has_normals == i,
          vertices[1], vertices[2], vertices[3],
          normals[1], normals[2], normals[3]
          );
    }
    return error;
  }
}

static int wf_parse_material_file(waveform_obj_t *result)
{
  int error;
  FILE *file;
  char *name;

  if (wf_read_to_eol(&name)) {
    fprintf(stderr, "mtllib directive has no filename\n");
    return 0x610;
  }

  printf("Loading materials from lib '%s'\n", name);
  file = fopen(name, "r");
  if (file == NULL) {
    perror("Could not open material library");
    return 0x620;
  }

  error = waveform_mtl_parse_file(result, file);
  fclose(file);
  if (error) {
    fprintf(stderr, "Could not load material library %s\n", name);
    return 0x630;
  }
  return 0;
}

static int wf_read_material_ref(waveform_obj_t *result)
{
  char *name;
  size_t mat_idx;
  wf_object_t *object;
  wf_material_ref_t *material_ref;

  // get current object
  if (result->nb_objects <= 0) {
    fprintf(stderr, "Expected an object definition prior to the usemtl directive\n");
    return 0x710;
  }
  object = &result->objects[result->nb_objects - 1];

  // finalize prev material_ref
  if (object->nb_material_refs > 0) {
    wf_obj_finalize_prev_material_ref(result, object);
  }

  // get new material_ref
  if (object->nb_material_refs >= SIZEOF_ARRAY(object->material_refs)) {
    fprintf(stderr, "Maximum number of material ref of %zu for an object reached\n",
        object->nb_material_refs);
    return 0x720;
  }
  material_ref = &object->material_refs[object->nb_material_refs];

  // get material name
  if (wf_read_to_eol(&name)) {
    fprintf(stderr, "usemtl directive does not refer to any material\n");
    return 0x730;
  }

  // get material idx
  for (mat_idx = 0; mat_idx < result->nb_materials; ++mat_idx) {
    if (strcmp(name, result->materials[mat_idx].name) == 0) {
      break;
    }
  }

  // check we found the material
  if (mat_idx >= result->nb_materials) {
    fprintf(stderr, "material '%s' not found\n", name);
    return 0x740;
  }

  // init material ref
  material_ref->material_idx = mat_idx;
  material_ref->faces_start = result->nb_faces;
  material_ref->faces_stop = 0;
  object->nb_material_refs += 1;

  return 0;
}

static int wf_obj_read_vertice_normals(waveform_obj_t *result)
{
  int error;
  float components[3];
  vect3_t *vect;

  if (result->nb_normals > MAX_NORMALS) {
    fprintf(stderr, "Max normal definition of %u reached\n", MAX_NORMALS);
    return 0x810;
  }

  // read x, y and z components
  error = wf_read_float(components, 3);
  switch (error) {
    case 1:
      fprintf(stderr, "Expected 3 floats\n");
      return 0x820;
    case 2:
    case 3:
      fprintf(stderr, "Conversion error: %s\n", WF_READ_FLOAT_ERROR_STR[error]);
      return 0x830;
    default:
      break;
  }

  if (wf_read_eol() == 0) {
    fprintf(stderr, "Expected end of line\n");
    return 0x840;
  }

  // store extracted values
  vect = &result->normals[result->nb_normals];
  vect->x = components[0];
  vect->y = components[1];
  vect->z = components[2];
  vect->w = 0.0;
  result->nb_normals += 1;

  return 0;
}






static int wf_obj_read_smooth(waveform_obj_t *result) {
  wf_object_t *object;
  char *line;

  if (result->nb_objects <= 0) {
    fprintf(stderr, "Expected an object definition first\n");
    return 0x910;
  }
  object = &result->objects[result->nb_objects - 1];

  if (wf_read_to_eol(&line)) {
    fprintf(stderr, "Missing argument\n");
    return 0x920;
  }

  if (strcmp(line, "1") == 0) {
    object->smooth = 1;
  } else if (strcmp(line, "off") == 0) {
    object->smooth = 0;
  } else {
    fprintf(stderr, "Unrecognized argument %s\n", line);
    return 0x930;
  }

  return 0;
}

/****************************************
 * Waveform .mtl parsing functions
 ***************************************/

static void wf_init_default_material(wf_material_t *material)
{
  bzero(material, sizeof(*material));

  my_strlcpy(material->name, "default_material", sizeof(material->name));
  COLOR_INIT(material->ambiant_color, 0, 0, 0);
  COLOR_INIT(material->diffuse_color, 1.0, 0.0, 1.0);
  COLOR_INIT(material->specular_color, 1.0, 1.0, 1.0);
  material->ns = 0.f;
  material->ni = 0.f;
  material->d = 0.f;
  material->illum = 1;
}

static int wf_mtl_read_newmtl(waveform_obj_t *result) {
  char *name;
  int error;
  wf_material_t *material;

  if (result->nb_materials >= MAX_MATERIALS) {
    fprintf(stderr, "Max number of material definition of %i reached\n", MAX_MATERIALS);
    return 0x110;
  }
  material = &result->materials[result->nb_materials];
  wf_init_default_material(material);

  error = wf_read_to_eol(&name);
  if (error) {
    return 0x120;
  }

  my_strlcpy(material->name, name, sizeof(material->name));
  result->nb_materials += 1;
  return 0;
}

static wf_material_t * wf_mtl_get_last_material(waveform_obj_t *result)
{
  if (result->nb_materials <= 0) {
    fprintf(stderr, "Expected a newmtl directive first\n");
    return NULL;
  }
  return &result->materials[result->nb_materials - 1];
}

static int wf_mtl_read_ns(waveform_obj_t *result)
{
  wf_material_t *mat;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x210;
  }

  if (wf_read_1f(&mat->ns)) {
    return 0x220;
  }
  return 0;
}

static int wf_mtl_read_ni(waveform_obj_t *result)
{
  wf_material_t *mat;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x310;
  }

  if (wf_read_1f(&mat->ni)) {
    return 0x320;
  }
  return 0;
}

static int wf_mtl_read_d(waveform_obj_t *result)
{
  wf_material_t *mat;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x410;
  }

  if (wf_read_1f(&mat->d)) {
    return 0x420;
  }
  return 0;
}

static int wf_mtl_read_color_ambiant(waveform_obj_t *result)
{
  wf_material_t *mat;
  color_t color;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x510;
  }

  if (wf_read_color(&color)) {
    return 0x520;
  }

  mat->ambiant_color = color;

  return 0;
}

static int wf_mtl_read_color_diffuse(waveform_obj_t *result)
{
  wf_material_t *mat;
  color_t color;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x610;
  }

  if (wf_read_color(&color)) {
    return 0x620;
  }

  mat->diffuse_color = color;

  return 0;
}

static int wf_mtl_read_color_specular(waveform_obj_t *result)
{
  wf_material_t *mat;
  color_t color;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x710;
  }

  if (wf_read_color(&color)) {
    return 0x720;
  }

  mat->specular_color = color;

  return 0;
}

static int wf_mtl_read_illum(waveform_obj_t *result)
{
  wf_material_t *mat;
  int illum;

  mat = wf_mtl_get_last_material(result);
  if (mat == NULL) {
    return 0x510;
  }

  if (wf_read_integer(&illum, 1)) {
    return 0x820;
  }

  mat->illum = illum;

  return 0;
}

/****************************************
 * Waveform main parsing functions
****************************************/

int waveform_mtl_parse_file(waveform_obj_t *result, FILE *file)
{
  int error;
  char buffer[1024];
  char *line;
  size_t line_nb;

  line = NULL;
  line_nb = 0;
  error = 0;
  while (1) {
    char *first;

    line_nb += 1;
    line = fgets(buffer, sizeof(buffer), file);
    // EOF
    if (line == NULL) {
      break;
    }

    // split line
    first = strtok(line, " \n");
    if (first != NULL) {
      if (strcmp(first, "newmtl") == 0) {
        error = wf_mtl_read_newmtl(result);
      } else if (strcmp(first, "Ns") == 0) {
        error = wf_mtl_read_ns(result);
      } else if (strcmp(first, "Ka") == 0) {
        error = wf_mtl_read_color_ambiant(result);
      } else if (strcmp(first, "Kd") == 0) {
        error = wf_mtl_read_color_diffuse(result);
      } else if (strcmp(first, "Ks") == 0) {
        error = wf_mtl_read_color_specular(result);
      } else if (strcmp(first, "Ni") == 0) {
        error = wf_mtl_read_ni(result);
      } else if (strcmp(first, "d") == 0) {
        error = wf_mtl_read_d(result);
      } else if (strcmp(first, "illum") == 0) {
        error = wf_mtl_read_illum(result);
      } else if (first[0] != '#') {
        printf(
            "waveform_mtl_parse_file: "
            "Ignoring directive %s at line %zu\n",
            first, line_nb
            );
      }

      if (error) {
        fprintf(
            stderr,
            "waveform_mtl_parse_file: "
            "Error 0x%04x at line %zu\n",
            error, line_nb
            );
        return 1;
      }
    }
  }

  return 0;
}

int waveform_obj_parse_file(waveform_obj_t *result, FILE *file)
{
  int error;
  char buffer[1024];
  char *line;
  size_t line_nb;

  result->nb_objects = 0;
  result->nb_vertices = 0;
  result->nb_faces = 0;
  result->nb_materials = 0;
  result->nb_normals = 0;
  result->objects = xmalloc(sizeof(*result->objects) * MAX_OBJECTS);
  result->vertices = xmalloc(sizeof(*result->vertices) * MAX_VERTICES);
  result->faces = xmalloc(sizeof(*result->faces) * MAX_FACES);
  result->materials = xmalloc(sizeof(*result->materials) * MAX_MATERIALS);
  result->normals = xmalloc(sizeof(*result->normals) * MAX_NORMALS);

  line = NULL;
  line_nb = 0;
  error = 0;
  while (1) {
    char *first;

    line_nb += 1;
    line = fgets(buffer, sizeof(buffer), file);
    // EOF
    if (line == NULL) {
      break;
    }
    // split the line
    first = strtok(line, " \n");
    if (first != NULL) {
      if (strcmp(first, "o") == 0) {
        error = wf_read_object(result);
      } else if (strcmp(first, "v") == 0) {
        error = wf_read_vertice(result);
      } else if (strcmp(first, "vn") == 0) {
        error = wf_obj_read_vertice_normals(result);
      } else if (strcmp(first, "f") == 0) {
        error = wf_read_face(result);
      } else if (strcmp(first, "s") == 0) {
        error = wf_obj_read_smooth(result);
      } else if (strcmp(first, "mtllib") == 0) {
        error = wf_parse_material_file(result);
      } else if (strcmp(first, "usemtl") == 0) {
        error = wf_read_material_ref(result);
      } else if (first[0] != '#') {
        printf(
            "waveform_obj_parse_file: "
            "Ignoring directive %s at line %zu\n", first, line_nb
            );
      }
      if (error) {
        fprintf(
            stderr,
            "waveform_obj_parse_file: "
            "Error 0x%04x at line %zu\n", error, line_nb
            );
        return error;
      }
    }
  }

  return wf_finalize_prev_object(result);
}

void clean_waveform_obj(waveform_obj_t *wf_obj)
{
  free(wf_obj->objects);
  free(wf_obj->vertices);
  free(wf_obj->faces);
  free(wf_obj->materials);
  free(wf_obj->normals);
  bzero(wf_obj, sizeof(*wf_obj));
}
