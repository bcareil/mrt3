#include <string.h>
#include <strings.h>

#include "mrt3.h"
#include "clock/clock.h"
#include "vect/vect.h"
#include "scene/scene.h"
#include "waveform/waveform.h"

static void calculate_aa_bbox(
    bbox_aa_t *bbox,
    triangle_t const *triangles,
    size_t triangles_count
    )
{
  size_t i;
  vect3_t min = VECT3_POS_INF;
  vect3_t max = VECT3_MIN_INF;

  // init bbox
  for (i = 0; i < triangles_count; ++i) {
    triangle_t const *triangle;

    triangle = &triangles[i];
    min.x = MIN(min.x, MIN(triangle->v0.x, MIN(triangle->v1.x, triangle->v2.x)));
    max.x = MAX(max.x, MAX(triangle->v0.x, MAX(triangle->v1.x, triangle->v2.x)));
    min.y = MIN(min.y, MIN(triangle->v0.y, MIN(triangle->v1.y, triangle->v2.y)));
    max.y = MAX(max.y, MAX(triangle->v0.y, MAX(triangle->v1.y, triangle->v2.y)));
    min.z = MIN(min.z, MIN(triangle->v0.z, MIN(triangle->v1.z, triangle->v2.z)));
    max.z = MAX(max.z, MAX(triangle->v0.z, MAX(triangle->v1.z, triangle->v2.z)));
  }
  bbox->origin = min;
  VECT3_SUB(bbox->dimensions, max, min);
}

static void load_material_refs_to_scene(
    scene_t *scene,
    waveform_obj_t *wf
    )
{
  size_t i;
  size_t nb_material_refs;
  material_ref_t *mat_ref;
  material_t *materials;

  nb_material_refs = 0;
  for (i = 0; i < wf->nb_objects; ++i) {
    nb_material_refs += wf->objects[i].nb_material_refs;
  }

  mat_ref = xmalloc(sizeof(*scene->material_refs) * nb_material_refs);
  materials = scene->materials;
  scene->nb_material_refs = nb_material_refs;
  scene->material_refs = mat_ref;
  for (i = 0; i < wf->nb_objects; ++i) {
    size_t j;
    wf_object_t *object;

    object = &wf->objects[i];
    for (j = 0; j < object->nb_material_refs; ++j) {
      wf_material_ref_t *wf_mat_ref;

      wf_mat_ref = &object->material_refs[j];
      mat_ref->material = &materials[wf_mat_ref->material_idx];
      mat_ref->triangles_start = wf_mat_ref->faces_start;
      mat_ref->triangles_stop = wf_mat_ref->faces_stop;
      mat_ref += 1;
    }
  }
}

static void load_materials_to_scene(
    scene_t *scene,
    waveform_obj_t *wf
    )
{
  size_t i;

  scene->nb_materials = wf->nb_materials;
  scene->materials = xmalloc(sizeof(*scene->materials) * scene->nb_materials);

  for (i = 0; i < scene->nb_materials; ++i) {
    wf_material_t *wf_mat;
    material_t *mat;

    wf_mat = &wf->materials[i];
    mat = &scene->materials[i];

    mat->is_light = (wf_mat->illum == 0);
    mat->diffuse_color = wf_mat->diffuse_color;
    mat->specular_color = wf_mat->specular_color;
  }
}

static void load_lights_to_scene(
    scene_t *scene
    )
{
  size_t i;
  size_t nb_lights;
  light_t *lights;

  nb_lights = 0;
  for (i = 0; i < scene->nb_material_refs; ++i) {
    nb_lights += (scene->material_refs[i].material->is_light != 0);
  }

  lights = xmalloc(sizeof(*scene->lights) * nb_lights);
  scene->nb_lights = nb_lights;
  scene->lights = lights;
  for (i = 0; i < scene->nb_material_refs; ++i) {
    material_ref_t *mat_ref;

    mat_ref = &scene->material_refs[i];
    if (mat_ref->material->is_light) {
      lights->material = mat_ref->material;
      lights->triangles_start = mat_ref->triangles_start;
      lights->triangles_stop = mat_ref->triangles_stop;
      calculate_aa_bbox(
          &lights->bbox,
          &scene->triangles[mat_ref->triangles_start],
          mat_ref->triangles_stop - mat_ref->triangles_start
          );
      lights += 1;
    }
  }
}

static void load_objects_to_scene(
    scene_t *scene,
    waveform_obj_t *wf
    )
{
  size_t i;

  scene->nb_objects = wf->nb_objects;
  scene->objects = xmalloc(sizeof(*scene->objects) * scene->nb_objects);
  for (i = 0; i < scene->nb_objects; ++i) {
    wf_object_t *wf_object;
    object_t *object;

    wf_object = &wf->objects[i];
    object = &scene->objects[i];
    bzero(object->name, sizeof(wf_object->name));
    strncpy(object->name, wf_object->name, sizeof(object->name) - 1);
    object->smooth = wf_object->smooth;
    object->triangles_start = wf_object->faces_start;
    object->triangles_stop = wf_object->faces_stop;
    object->normals_start = wf_object->normals_start;
    object->normals_stop = wf_object->normals_stop;

    calculate_aa_bbox(
        &object->bbox,
        &scene->triangles[object->triangles_start],
        object->triangles_stop - object->triangles_start
        );
 }
}

static void load_triangles_to_scene(
    scene_t *scene,
    waveform_obj_t *wf,
    vect3_t *bounds_min,
    vect3_t *bounds_max
    )
{
  size_t i;

  // init bounds to infinity
  VECT3_INIT(*bounds_min, INFINITY, INFINITY, INFINITY);
  *bounds_max = *bounds_min;
  VECT3_OPPOSITE(*bounds_max);

  // allocate triangles in scene
  scene->nb_triangles = wf->nb_faces;
  scene->triangles = xmalloc(sizeof(*scene->triangles) * scene->nb_triangles);
  scene->nb_triangle_normals = scene->nb_triangles;
  scene->triangle_normals = xmalloc(
      sizeof(*scene->triangle_normals) * scene->nb_triangle_normals
      );
  
  for (i = 0; i < scene->nb_triangles; ++i) {
    triangle_t *normals;
    triangle_t *triangle;
    wf_face_t *face;

    triangle = &scene->triangles[i];
    normals = &scene->triangle_normals[i];
    face = &wf->faces[i];

    triangle->v0 = wf->vertices[face->v0];
    triangle->v1 = wf->vertices[face->v1];
    triangle->v2 = wf->vertices[face->v2];

    VECT3_MIN(*bounds_min, *bounds_min, triangle->v0);
    VECT3_MIN(*bounds_min, *bounds_min, triangle->v1);
    VECT3_MIN(*bounds_min, *bounds_min, triangle->v2);
    VECT3_MAX(*bounds_max, *bounds_max, triangle->v0);
    VECT3_MAX(*bounds_max, *bounds_max, triangle->v1);
    VECT3_MAX(*bounds_max, *bounds_max, triangle->v2);

    if (face->has_normals) {
      normals->v0 = wf->normals[face->n0];
      normals->v1 = wf->normals[face->n1];
      normals->v2 = wf->normals[face->n2];
    } else {
      VECT3_INIT(normals->v0, 0, 0, 0);
      VECT3_INIT(normals->v1, 0, 0, 0);
      VECT3_INIT(normals->v2, 0, 0, 0);
    }
  }
}

void waveform_obj_to_scene(scene_t *scene, waveform_obj_t *wf_obj)
{
  my_clock_t clock;
  vect3_t bounds_min;
  vect3_t bounds_max;
  bbox_aa_t bbox;

  // init camera
  clock_init(&clock);
  init_camera(
      &scene->camera,
      // resolution
      SCREEN_WIDTH, SCREEN_HEIGHT,
      // origin
      9, 0, 0,
      // direction
      -1, 0, 0,
      // vertical
      0, 1, 0
      );
  printf("Camera initialized in %.6fs\n", clock_get_elapsed_seconds(&clock));

  // copy from wf to scene
  clock_init(&clock);
  load_triangles_to_scene(scene, wf_obj, &bounds_min, &bounds_max);
  load_materials_to_scene(scene, wf_obj);
  load_material_refs_to_scene(scene, wf_obj);
  load_lights_to_scene(scene);
  load_objects_to_scene(scene, wf_obj);
  printf("Objects loaded from Waveform in %.6fs\n", clock_get_elapsed_seconds(&clock));

  // init kdtree
  clock_init(&clock);
  kdtree_init(&scene->kdtree, scene->triangles, scene->nb_triangles);
  printf("KDTree initialized in %.6fs\n", clock_get_elapsed_seconds(&clock));
  // DEBUG
  kdtree_dump_to_file(&scene->kdtree, "kdtree.obj");

  // init oct tree
  clock_init(&clock);
  bbox_from_points(&bbox, &bounds_min, &bounds_max);
  bbox_grow(&bbox, 0.2f);
  otree_init(&scene->otree, &bbox, 4, scene->nb_lights * sizeof(light_stat_t));
  printf("OTree initialized in %.6fs\n", clock_get_elapsed_seconds(&clock));
}

