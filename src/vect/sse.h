#ifndef __SSE_H__
# define __SSE_H__

# include <smmintrin.h>

typedef struct sse_vect3_packet_s {
  __m128 x, y, z;
} sse_vect3_packet_t;

typedef struct sse_ray_packet_s {
  sse_vect3_packet_t org;
  sse_vect3_packet_t dir;
} sse_ray_packet_t;

# define SSE_PF_STR "(%.3f, %.3f, %.3f, %.3f)"

# define SSE_PF_EX(f4) f4[0], f4[1], f4[2], f4[3]

# define SSE_PRINTF(name, m128) do {                      \
    float f__[4];                                         \
    _mm_store_ps(f__, m128);                              \
    printf(name "=" SSE_PF_STR "\n", SSE_PF_EX(f__));     \
  } while (0)

# define VECT3_TO_SSE(v)    \
  _mm_load_ps((float *)(&v))

# define VECT3_TO_SSE_VECT3_PACKET(sse_vect3_packet, m128) do { \
    sse_vect3_packet.x = _mm_shuffle_ps(m128, m128, 0x00);      \
    sse_vect3_packet.y = _mm_shuffle_ps(m128, m128, 0x55);      \
    sse_vect3_packet.z = _mm_shuffle_ps(m128, m128, 0xAA);      \
  } while (0)

# define RAY_PACKET_TO_SSE(sse_ray_packet, ray_packet) do {  \
    sse_ray_packet.org.x = _mm_load_ps((ray_packet).org.x);  \
    sse_ray_packet.org.y = _mm_load_ps((ray_packet).org.y);  \
    sse_ray_packet.org.z = _mm_load_ps((ray_packet).org.z);  \
    sse_ray_packet.dir.x = _mm_load_ps((ray_packet).dir.x);  \
    sse_ray_packet.dir.y = _mm_load_ps((ray_packet).dir.y);  \
    sse_ray_packet.dir.z = _mm_load_ps((ray_packet).dir.z);  \
  } while (0)

# define SSE_PACKED_SUB(r, a, b) do {   \
    (r).x = _mm_sub_ps((a).x, (b).x);   \
    (r).y = _mm_sub_ps((a).y, (b).y);   \
    (r).z = _mm_sub_ps((a).z, (b).z);   \
  } while (0)

# define SSE_PACKED_DOT_PRODUCT(a, b)           \
  _mm_add_ps(                                   \
      _mm_add_ps(                               \
          _mm_mul_ps(a.x, b.x),                 \
          _mm_mul_ps(a.y, b.y)                  \
          ),                                    \
      _mm_mul_ps(a.z, b.z)                      \
      )

# define SSE_PACKED_CROSS_PRODUCT_PV(r, p, v) do { \
    r.x = _mm_sub_ps(                              \
        _mm_mul_ps(p.y, v.z),                      \
        _mm_mul_ps(p.z, v.y)                       \
        );                                         \
    r.y = _mm_sub_ps(                              \
        _mm_mul_ps(p.z, v.x),                      \
        _mm_mul_ps(p.x, v.z)                       \
        );                                         \
    r.z = _mm_sub_ps(                              \
        _mm_mul_ps(p.x, v.y),                      \
        _mm_mul_ps(p.y, v.x)                       \
        );                                         \
  } while (0)

# define SSE_VECT3_DOT_PRODUCT(a, b)            \
  _mm_dp_ps(a, b, 0x71)

# define SSE_VECT3_CROSS_PRODUCT(r, a, b) do {      \
    r = _mm_sub_ps(                                 \
        _mm_mul_ps(a, _mm_shuffle_ps(b, b, 0xC9)),  \
        _mm_mul_ps(_mm_shuffle_ps(a, a, 0xC9), b)   \
        );                                          \
    r = _mm_shuffle_ps(r, r, 0xC9);                 \
  } while (0)

#endif /* __SSE_H__ */
