#ifndef __VECT_H__
# define __VECT_H__

# include "../utils.h"

typedef struct STRUCT_ALLIGNED vect3_s {
  float x;
  float y;
  float z;
  float w;
} vect3_t;

typedef struct STRUCT_ALLIGNED vect3_packet_s {
  float x[4];
  float y[4];
  float z[4];
} vect3_packet_t;

typedef struct vect3_a_s {
  float *x;
  float *y;
  float *z;
} vect3_a_t;

typedef struct STRUCT_ALLIGNED ray_s {
  vect3_t org;
  vect3_t dir;
} ray_t;

typedef struct STRUCT_ALLIGNED ray_packet_s {
  vect3_packet_t org;
  vect3_packet_t dir;
} ray_packet_t;

// printf related for vect3

# define VECT3_PF_STR "(%.3f, %.3f, %.3f)"

# define VECT3_PF_EX(v) (v).x, (v).y, (v).z

# define VECT3_PRINTF(name, v) \
  printf(name "=" VECT3_PF_STR "\n", VECT3_PF_EX(v))

// printf related for ray

# define RAY_PF_STR "(org=" VECT3_PF_STR ";dir=" VECT3_PF_STR ")"

# define RAY_PF_EX(r) VECT3_PF_EX((r).org), VECT3_PF_EX((r).dir)

# define RAY_PRINTF(name, r) \
  printf(name "=" RAY_PF_STR "\n", RAY_PF_EX(r))

// general purpose index based vector's component access
# define VECT_COMP(v, i) (((float *)(&(v)))[i])

// extract a ray from a ray packet
# define RAY_PACKET_TO_RAY(ray, packet, idx) {  \
    (ray).org.x = (packet).org.x[idx];          \
    (ray).org.y = (packet).org.y[idx];          \
    (ray).org.z = (packet).org.z[idx];          \
    (ray).dir.x = (packet).dir.x[idx];          \
    (ray).dir.y = (packet).dir.y[idx];          \
    (ray).dir.z = (packet).dir.z[idx];          \
  } while (0)

// some vect3 values for initialization
# define VECT3_ZERO  {0, 0, 0, 0}
# define VECT3_ONE  {1, 1, 1, 0}
# define VECT3_POS_INF  {INFINITY, INFINITY, INFINITY, 0}
# define VECT3_MIN_INF  {-INFINITY, -INFINITY, -INFINITY, 0}

/****************************************
 * Single vector operation
****************************************/

# define VECT3_INIT(v, x_, y_, z_) do {         \
    (v).x = x_;                                 \
    (v).y = y_;                                 \
    (v).z = z_;                                 \
    (v).w = 0;                                  \
  } while (0)

# define VECT3_BINARY_OPERATION(r, a, b, o) do {                  \
    int i__;                                                      \
    for (i__ = 0; i__ < 4; ++i__) {                               \
      VECT_COMP(r, i__) = VECT_COMP(a, i__) o VECT_COMP(b, i__);  \
    }                                                             \
  } while (0)

# define VECT3_BINARY_OPERATION_S(r, v, s, o) do {   \
    int i__;                                         \
    for (i__ = 0; i__ < 4; ++i__) {                  \
      VECT_COMP(r, i__) = VECT_COMP(v, i__) o s;     \
    }                                                \
  } while (0)

# define VECT3_BINARY_FUNCTION(r, a, b, f) do {      \
    int i__;                                         \
    for (i__ = 0; i__ < 4; ++i__) {                  \
      VECT_COMP(r, i__) = f(                         \
          VECT_COMP(a, i__), VECT_COMP(b, i__)       \
          );                                         \
    }                                                \
  } while (0)


# define VECT3_ADD(r, a, b) VECT3_BINARY_OPERATION(r, a, b, +)

# define VECT3_ADD_S(r, v, s) VECT3_BINARY_OPERATION_S(r, v, s, +)

# define VECT3_SUB(r, a, b) VECT3_BINARY_OPERATION(r, a, b, -)

# define VECT3_SUB_S(r, v, s) VECT3_BINARY_OPERATION_S(r, v, s, -)

# define VECT3_MUL(r, a, b) VECT3_BINARY_OPERATION(r, a, b, *)

# define VECT3_MUL_S(r, v, s) VECT3_BINARY_OPERATION_S(r, v, s, *)

# define VECT3_DIV_S(r, v, s) VECT3_BINARY_OPERATION_S(r, v, s, /)

# define VECT3_OPPOSITE(v) do {  \
    (v).x = -(v).x;              \
    (v).y = -(v).y;              \
    (v).z = -(v).z;              \
    (v).w = -(v).w;              \
  } while (0);

# define VECT3_DOT(a, b) (  \
      ((a).x * (b).x) +     \
      ((a).y * (b).y) +     \
      ((a).z * (b).z)       \
      )

# define VECT3_LENGTH_SQ(a) (   \
      POW2((a).x) +             \
      POW2((a).y) +             \
      POW2((a).z)               \
      )

# define VECT3_LENGTH(a)                        \
  sqrt(VECT3_LENGTH_SQ(a))

# define VECT3_NORM(a) do {                     \
    register float length__;                    \
    length__ = VECT3_LENGTH(a);                 \
    VECT3_DIV_S(a, a, length__);                \
  } while (0)

# define VECT3_CROSS(r, a, b) do {              \
    (r).x = ((a).y * (b).z) - ((a).z * (b).y);  \
    (r).y = ((a).z * (b).x) - ((a).x * (b).z);  \
    (r).z = ((a).x * (b).y) - ((a).y * (b).x);  \
    (r).w = 0;                                  \
  } while (0)

# define VECT3_MIN(r, a, b)                     \
  VECT3_BINARY_FUNCTION(r, a, b, MIN)

# define VECT3_MAX(r, a, b)                     \
  VECT3_BINARY_FUNCTION(r, a, b, MAX)

#endif
