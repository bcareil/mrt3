#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void	*xmalloc(size_t size)
{
  register void *ptr;

  ptr = malloc(size);
  if (ptr != NULL) {
    return ptr;
  }
  perror("xmalloc");
  abort();
}

void *xrealloc(void *ptr, size_t new_size)
{
  ptr = realloc(ptr, new_size);
  if (ptr != NULL) {
    return ptr;
  }
  perror("realloc");
  abort();
}

void *xmemdup(void *s, size_t n)
{
  void *d;

  d = xmalloc(n);
  return memcpy(d, s, n);
}

size_t my_strlcpy(char *dest, char const *src, size_t dest_size)
{
  size_t i;

  for (i = 0; i < dest_size; ++i) {
    dest[i] = src[i];
    if (dest[i] == '\0') {
      return i;
    }
  }
  dest[i - 1] = '\0';
  return i;
}
