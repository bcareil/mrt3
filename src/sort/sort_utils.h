#pragma once
#include <stdlib.h>

/**
 ** Arrange the values in @a source inside @a dest such as
 ** dest[i] = source[indices[i]].
 **
 ** Do not handle inplace operations.
 */
void arrange_from_indices(
    void *dest,
    void const *source,
    size_t const *indices,
    size_t const nb_elements,
    size_t const element_size
    );

/**
 ** @see_also arrange_from_indices
 */
void float_arrage_from_indices(
    float *dest,
    float const *source,
    size_t const *indices,
    size_t const size
    );

/**
 ** Initialize and sort @a sorted_indices such as
 ** array[sorted_indices[i]] is sorted.
 */
void float_argsort(
    size_t *sorted_indices,
    float const *array,
    size_t const size
    );

/**
 ** Abort if the specified indices array is not
 ** sorted.
 */
void float_indices_assert_sorted(
    float const *arr,
    size_t const *indices,
    size_t size
    );

/**
 ** Abort if the specified array is not sorted
 */
void float_assert_sorted(
    float const *arr,
    size_t size
    );
