#ifndef __SELECT_H__
# define __SELECT_H__

# include <stdlib.h>

void quickselect(
    void *arr, 
    size_t element_size, 
    size_t nb_elements, 
    size_t k, 
    int (*cmp)(void const *, void const *)
    );

#endif
