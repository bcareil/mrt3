#include <assert.h>

#include "sort_utils.h"
#include "../utils.h"

#define SORT_NAME sort_float_pointers
#define SORT_TYPE float const *
#define SORT_CMP(a, b) ((*(a)) - (*(b)))
#include "sort/sort.h"

void float_arrage_from_indices(
    float *dest,
    float const *source,
    size_t const *indices,
    size_t const size
    )
{
  size_t i;

  // do not handle inplace sort
  assert(dest != source);
  for (i = 0; i < size; ++i) {
    dest[i] = source[indices[i]];
  }
}

void arrange_from_indices(
    void *dest,
    void const *source,
    size_t const *indices,
    size_t const nb_elements,
    size_t const element_size
    )
{
  size_t i;
  size_t j;

  // do not handle inplace sort
  assert(dest != source);
  for (i = 0; i < nb_elements; ++i) {
    size_t const src_index = indices[i] * element_size;
    size_t const dst_index = i * element_size;

    for (j = 0; j < element_size; ++j) {
      ((char *)dest)[dst_index + j] = ((char *)source)[src_index + j];
    }
  }
}

void float_argsort(
    size_t *sorted_indices,
    float const *array,
    size_t const size
    )
{
  size_t i;
  float const **pointers;

  // do not bother with zero-length array
  if (size <= 0) {
    return;
  }

  // init pointer array
  pointers = xmalloc(sizeof(*pointers) * size);
  for (i = 0; i < size; ++i) {
    pointers[i] = &array[i];
  }

  // sort pointer array using timsort
  sort_float_pointers_heap_sort(pointers, size);

  // convert pointers to indices
  for (i = 0; i < size; ++i) {
    sorted_indices[i] = (size_t)(pointers[i] - array);
  }

  // free array
  free(pointers);
}

#ifndef NDEBUG

void float_indices_assert_sorted(
    float const *arr,
    size_t const *indices,
    size_t size
    )
{
  size_t i;

  if (size == 0) {
    return;
  }

  for (i = 1; i < size; ++i) {
    assert(arr[indices[i - 1]] <= arr[indices[i]]);
  }
}

void float_assert_sorted(
    float const *arr,
    size_t size
    )
{
  size_t i;

  if (size == 0) {
    return;
  }

  for (i = 1; i < size; ++i) {
    assert(arr[i - 1] <= arr[i]);
  }
}

#endif // NDEBUG
