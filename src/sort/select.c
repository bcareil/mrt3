#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "select.h"

/**
 * byte-wise swap two segment of memory
 */
static void swap(
    void *a,
    void *b,
    size_t element_size
    )
{
  char *a_ptr;
  char *b_ptr;
  char t;
  size_t i;

  a_ptr = (char *)(a);
  b_ptr = (char *)(b);
  for (i = 0; i < element_size; ++i) {
    t = a_ptr[i];
    a_ptr[i] = b_ptr[i];
    b_ptr[i] = t;
  }
}

static size_t partition(
    void *arr,
    size_t element_size,
    size_t start,
    size_t stop,
    size_t pivot,
    int (*cmp)(void const *, void const *)
    )
{
  uint8_t *buffer = arr;
  uint8_t *store_index;
  uint8_t *pivot_value;
  uint8_t *i_value;

  // put the pivot at the end of the segment
  pivot_value = buffer + stop * element_size;
  store_index = buffer + start * element_size;
  swap(buffer + pivot * element_size, pivot_value, element_size);

  // store all value less than pivot at the begining of the segment
  for (i_value = buffer + start * element_size;
      i_value < pivot_value;
      i_value += element_size
      ) {
    if (cmp(i_value, pivot_value) < 0) {
      swap(store_index, i_value, element_size);
      store_index += element_size;
    }
  }

  // put the pivot back in place
  swap(store_index, pivot_value, element_size);
  return pivot;
}

void quickselect(
    void *arr,
    size_t element_size,
    size_t nb_elements,
    size_t k,
    int (*cmp)(void const *, void const *)
    )
{
  size_t pivot;
  size_t left;
  size_t right;

  if (nb_elements <= 1) {
    return;
  }

  left = 0;
  right = nb_elements - 1;
  while (1) {
    pivot = left + ((right - left) >> 1);
    pivot = partition(arr, element_size, left, right, pivot, cmp);
    if (pivot == k) {
      return;
    } else if (pivot < k) {
      left = pivot + 1;
    } else {
      right = pivot - 1;
    }
  }
}
