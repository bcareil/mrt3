#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#include "vect/vect.h"
#include "scene/scene.h"
#include "intersects.h"
#include "mrt3.h"

/****************************************
 * Vector generation
****************************************/

static void generate_diffuse_reflection_vector_from_normal(
    vect3_t *diff_vec,
    vect3_t const *normal
    )
{
  const vect3_t x_axis = { 1, 0, 0, 0 };
  const vect3_t y_axis = { 0, 1, 0, 0 };
  float u = drand48();
  float v = drand48();
  float r = sqrt(u);
  float sqrt_1_m_u = sqrt(1.f - u);
  float angle = v * (2.0 * M_PI);
  vect3_t tdir;
  vect3_t sdir;
  vect3_t result;

  if (fabs(normal->x) < 0.5f) {
    VECT3_CROSS(sdir, (*normal), x_axis);
  } else {
    VECT3_CROSS(sdir, (*normal), y_axis);
  }
  VECT3_CROSS(tdir, (*normal), sdir);

  VECT3_MUL_S(result, (*normal), sqrt_1_m_u);
  VECT3_MUL_S(sdir, sdir, r * cosf(angle));
  VECT3_MUL_S(tdir, tdir, r * sinf(angle));
  VECT3_ADD(result, result, sdir);
  VECT3_ADD(result, result, tdir);
  *diff_vec = result;
}

static void generate_point_inside_aa_bbox(vect3_t *point, bbox_aa_t const *bbox)
{
  point->x = drand48();
  point->y = drand48();
  point->z = drand48();
  VECT3_MUL((*point), (*point), bbox->dimensions);
  VECT3_ADD((*point), (*point), bbox->origin);
}

/****************************************
 * Meta data lookup
****************************************/

static material_t *get_material_for_triangle_idx(
    scene_t const *scene,
    size_t triangle_idx
    )
{
  size_t i;

  for (i = 0; i < scene->nb_material_refs; ++i) {
    material_ref_t *mat_ref;

    mat_ref = &scene->material_refs[i];
    if (triangle_idx >= mat_ref->triangles_start
        && triangle_idx < mat_ref->triangles_stop) {
      return mat_ref->material;
    }
  }

  fprintf(stderr, "get_material_for_triangle_idx: Failed to find material\n");
  abort();

  return NULL;
}

static object_t *get_object_for_triangle_idx(
    scene_t const *scene,
    size_t triangle_idx
    )
{
  size_t i;

  for (i = 0; i < scene->nb_objects; ++i) {
    object_t *object;

    object = &scene->objects[i];
    if (triangle_idx >= object->triangles_start &&
        triangle_idx < object->triangles_stop) {
      return object;
    }
  }

  fprintf(stderr, "get_object_for_triangle_idx: Faild to find object\n");
  abort();

  return NULL;
}

/****************************************
 * Normal computation
****************************************/

static void calculate_default_triangle_normal(
    vect3_t *normal,
    triangle_t const *triangle
    )
{
  vect3_t n;
  vect3_t e1;
  vect3_t e2;

  VECT3_SUB(e1, triangle->v1, triangle->v0);
  VECT3_SUB(e2, triangle->v2, triangle->v0);
  VECT3_CROSS(n, e1, e2);

  VECT3_NORM(n);
  *normal = n;
}

static void calculate_interpolated_triangle_normal(
    vect3_t *normal,
    triangle_t const *normals,
    float u,
    float v
    )
{
  float one_m_uv;
  vect3_t n0;
  vect3_t n1;
  vect3_t n2;

  one_m_uv = 1.f - (u + v);

  n0 = normals->v0;
  n1 = normals->v1;
  n2 = normals->v2;

  VECT3_MUL_S(n0, n0, one_m_uv);
  VECT3_MUL_S(n1, n1, u);
  VECT3_MUL_S(n2, n2, v);
  VECT3_ADD(n0, n0, n1);
  VECT3_ADD(n0, n0, n2);

  *normal = n0;
}

/****************************************
 * Direct lightning
****************************************/

static void calculate_direct_lightning(
    color_t *result,
    scene_t *scene,
    ray_t const *ray,
    size_t triangle_idx,
    vect3_t const *intersection,
    vect3_t const *normal,
    material_t const *material
    )
{
  size_t i;
  color_t color = COLOR_BLACK;
  color_t tmp_color;
#ifdef DIRECT_LIGHTNING_PREDICTION
  light_stat_t fake_light_stat;

  // used in case of error in otree
  memset(&fake_light_stat, 0, sizeof(fake_light_stat));
#endif

  // calculate direct lightning
  for (i = 0; i < scene->nb_lights; ++i)
  {
    ray_t light_ray;
    light_t *light;
    triangle_intersect_data_t object_intersection;
    float expected_light_distance;
    size_t light_hit_triangle_idx;
#ifdef DIRECT_LIGHTNING_PREDICTION
    void *tmp_light_stat;
    light_stat_t *light_stat;
    int do_intersection_check;

    // fetch stat on light
    do_intersection_check = 1;
    light_stat = &fake_light_stat;
    tmp_light_stat = otree_get_node_data(
        &scene->otree,
        intersection->x,
        intersection->y,
        intersection->z
        );
    if (tmp_light_stat != NULL) {
      int hit, miss, sum;

      light_stat = i + (light_stat_t *)(tmp_light_stat);
      // fetch/cache var for thread safe usage
      // NOTE: there is no need for both of them
      //       to be aquired sequentialy, it won't
      //       affect unexpectedly the result.
      hit = light_stat->hit;
      miss = light_stat->miss;

      sum = hit + miss;
      if (sum > 96) {
        if (hit == 0) {
          // no hit reg
          if (sum > 1024 || drand48() > (sum / 1024.f)) {
            // assume miss
            //puts("miss assumed");
            continue;
          }
        } else if (miss == 0) {
          // no miss reg
          if (sum > 1024 || drand48() > (sum / 1024.f)) {
            // assume hit
            //puts("hit assumed");
            do_intersection_check = 0;
          }
        }
        if (hit > 0x7f000000) {
          light_stat->hit = 1024;
        }
        if (miss > 0x7f000000) {
          light_stat->miss = 1024;
        }
      }
    } else {
      puts("not in octree");
    }
#endif

    // get an origin for the light ray inside the light bounding box
    // NOTE: All lights are expected to be an axis aligned bounding box
    light = &scene->lights[i];
    generate_point_inside_aa_bbox(&light_ray.org, &light->bbox);

    // orient the normalized ray from the light to the current object
    VECT3_SUB(light_ray.dir, (*intersection), light_ray.org);
    expected_light_distance = VECT3_LENGTH(light_ray.dir);
    VECT3_DIV_S(light_ray.dir, light_ray.dir, expected_light_distance);

#ifdef DIRECT_LIGHTNING_PREDICTION
    if (do_intersection_check)
#endif
    {
      // check for objects between the light and the current object
      if (mrt_find_intersection(
            &object_intersection,
            &light_hit_triangle_idx,
            &light_ray,
            scene
            ) == 0) {
#ifdef DIRECT_LIGHTNING_PREDICTION
# pragma omp atomic
        light_stat->miss += 1;
#endif
        // should not happend
        //puts("SHOULD_NOT_HAPPEND");
        //but actually happend D:
        continue;
      }

      // check if distances matches (if distance is greater than expected,
      // we assume it actually hit the current object)
      if (triangle_idx != light_hit_triangle_idx
          && object_intersection.distance < (expected_light_distance - 0.01)) {
        // debug output
        //printf("light did not hit expected object %zu at a distance of %.3f: %.3f, %zu\n",
        //    triangle_idx, expected_light_distance, light_distance, light_hit_triangle_idx
        //    );
#ifdef DIRECT_LIGHTNING_PREDICTION
# pragma omp atomic
        light_stat->miss += 1;
#endif
        continue;
      }
#ifdef DIRECT_LIGHTNING_PREDICTION
#pragma omp atomic
      light_stat->hit += 1;
#endif
    }

    // light actually hit the object, proceed to Phong relection model
    {
      vect3_t reflected_light_vec;
      vect3_t opposit_ray_dir;
      vect3_t object_light_vec;
      float factor;

      // diffuse component
      object_light_vec = light_ray.dir;
      VECT3_OPPOSITE(object_light_vec);

      factor = VECT3_DOT(object_light_vec, (*normal));
      if (factor > 0.001f) {
        COLOR_MUL(tmp_color, material->diffuse_color, light->material->diffuse_color);
        COLOR_MUL_S(tmp_color, factor * DIFFUSE_FACTOR);
        COLOR_ADD(color, tmp_color);

        // specular component

        // compute reflected light ray
        factor *= 2.f; // 2 * (N dot L)
        VECT3_MUL_S(reflected_light_vec, (*normal), factor);
        VECT3_SUB(reflected_light_vec, reflected_light_vec, object_light_vec);
        // compute ray from object to "eye"
        opposit_ray_dir = ray->dir;
        VECT3_OPPOSITE(opposit_ray_dir);

        factor = VECT3_DOT(opposit_ray_dir, reflected_light_vec);
        //factor = VECT3_DOT(reflected_light_vec, object_light_vec);
        if (factor > 0.4) {
          // power factor by alpha, the shininess
          // (greater alpha -> more mirror like)
          factor = powf(factor, 8.f);
          factor *= SPECULAR_FACTOR;

          COLOR_MUL(tmp_color, material->specular_color, light->material->specular_color);
          COLOR_MUL_S(tmp_color, factor);
          COLOR_ADD(color, tmp_color);
        }
      }
    }
  }
  *result = color;
}

/****************************************
 * Single ray processing
****************************************/

static void mrt_process_ray(
    color_t *result,
    ray_t const *ray,
    scene_t *scene,
    float contribution,
    size_t depth
    )
{
  triangle_intersect_data_t intersect_data;
  float max_reflexion;
  size_t triangle_idx;
  material_t *material;

  // check for intersection
  if (mrt_find_intersection(&intersect_data, &triangle_idx, ray, scene) == 0) {
    COLOR_INIT_BLACK((*result));
    return;
  }

  // fetch triangle's material
  material = get_material_for_triangle_idx(scene, triangle_idx);
  max_reflexion = COLOR_MAX_COMPONENT(material->diffuse_color);

  if (material->is_light) {
    (*result) = material->diffuse_color;
  } else if (
      (depth > 2 && drand48() > max_reflexion)  // russian roulette
      || contribution < 0.01                    // current object's contribution too low
      || depth >= MAX_DEPTH                     // max recursion reached
      ) {
    COLOR_INIT_BLACK((*result));
  } else {
    object_t *object;
    triangle_t triangle;
    color_t color = BASE_AMBIANT_COLOR;
    color_t tmp_color;
    vect3_t normal;
    vect3_t intersection;

    // calculate normal
    triangle = scene->triangles[triangle_idx];
    object = get_object_for_triangle_idx(scene, triangle_idx);
    if (object->smooth) {
      calculate_interpolated_triangle_normal(
          &normal,
          &scene->triangle_normals[triangle_idx],
          intersect_data.u,
          intersect_data.v
          );
    } else {
      calculate_default_triangle_normal(&normal, &triangle);
    }
    //VECT3_PRINTF("triangle normal", normal);

    // calculate intersection point
    VECT3_MUL_S(intersection, ray->dir, intersect_data.distance);
    VECT3_ADD(intersection, intersection, ray->org);

    // calculate diffuse reflection color
    {
      ray_t diff_ref_ray;

      diff_ref_ray.org = intersection;
      generate_diffuse_reflection_vector_from_normal(&diff_ref_ray.dir, &normal);

      mrt_process_ray(&tmp_color, &diff_ref_ray, scene, contribution * max_reflexion, depth + 1);
      COLOR_MUL(tmp_color, tmp_color, material->diffuse_color);
      COLOR_MUL_S(tmp_color, AMBIANT_FACTOR);
      COLOR_ADD(color, tmp_color);
    }

    // direct lightning
    COLOR_INIT_BLACK(tmp_color);
    calculate_direct_lightning(
        &tmp_color,
        scene,
        ray,
        triangle_idx,
        &intersection,
        &normal,
        material
        );
    COLOR_ADD(color, tmp_color);

    *result = color;
  }
}

/****************************************
 * Ray packet processing
****************************************/

#ifdef PACKET_RAYS

static void mrt_process_ray_packet(
    color_t colors[4],
    ray_packet_t const *packet,
    scene_t *scene
    )
{
  size_t i;
  int post_process_mask;
  material_t *materials[4];
  object_t *objects[4];
  vect3_t normals[4];
  intersection_t intersections[4];

  mrt_find_packet_intersection(intersections, packet, scene);

  // fetch materials
  post_process_mask = 0;
  for (i = 0; i < SIZEOF_ARRAY(intersections); ++i) {
    if (intersections[i].triangle_idx == (size_t)(-1)) {
      materials[i] = NULL;
      objects[i] = NULL;
      colors[i] = SKY_COLOR;
    } else {
      materials[i] = get_material_for_triangle_idx(scene, intersections[i].triangle_idx);
      objects[i] = get_object_for_triangle_idx(scene, intersections[i].triangle_idx);
      if (materials[i]->is_light) {
        colors[i] = materials[i]->diffuse_color;
      } else {
        post_process_mask |= 1 << i;
      }
    }
  }

  if (post_process_mask == 0) {
    return;
  }

  // compute normals
  for (i = 0; i < SIZEOF_ARRAY(intersections); ++i) {
    if ((post_process_mask & (1 << i)) != 0) {
      if (objects[i]->smooth) {
        calculate_interpolated_triangle_normal(
            &normals[i],
            &scene->triangle_normals[intersections[i].triangle_idx],
            intersections[i].data.u,
            intersections[i].data.v
            );
      } else {
        calculate_default_triangle_normal(
            &normals[i],
            &scene->triangles[intersections[i].triangle_idx]
            );
      }
    }
  }

  // compute lighting
  // TODO: vectorize
  for (i = 0; i < SIZEOF_ARRAY(intersections); ++i) {
    if ((post_process_mask & (1 << i)) == 0) {
      colors[i] = SKY_COLOR;
    } else {
      ray_t ray, new_ray_diff;
      color_t tmp_color;
      vect3_t intersection;
      float max_reflection;

      COLOR_INIT_BLACK(colors[i]);
      max_reflection = COLOR_MAX_COMPONENT(materials[i]->diffuse_color);
      // compute intersection point
      RAY_PACKET_TO_RAY(ray, (*packet), i);
      VECT3_MUL_S(intersection, ray.dir, intersections[i].data.distance);
      VECT3_ADD(intersection, intersection, ray.org);
      // compute indirect lightning
      COLOR_INIT_BLACK(tmp_color);
      new_ray_diff.org = intersection;
      generate_diffuse_reflection_vector_from_normal(&new_ray_diff.dir, &normals[i]);
      mrt_process_ray(&tmp_color, &new_ray_diff, scene, max_reflection, 1);
      COLOR_MUL(tmp_color, tmp_color, materials[i]->diffuse_color);
      COLOR_MUL_S(tmp_color, AMBIANT_FACTOR);
      COLOR_ADD(colors[i], tmp_color);
      // compute direct lightning
      COLOR_INIT_BLACK(tmp_color);
      calculate_direct_lightning(
          &tmp_color,
          scene,
          &ray,
          intersections[i].triangle_idx,
          &intersection,
          &normals[i],
          materials[i]
          );
      COLOR_ADD(colors[i], tmp_color);
    }
  }

}

#endif

/****************************************
 * Main processing function
****************************************/

void mrt_process(context_t *context)
{
  size_t i;
  size_t imax;
  color_t *prev_colors;

  // update iteration count
  if (context->iteration >= context->max_iteration) {
    return;
  }
  context->iteration += 1;

  // init local variable for quick lookup
  prev_colors = context->render_buffer;

#ifdef PACKET_RAYS
  {
    ray_packet_t *packets;

    packets = context->scene->camera.packets;
    imax = context->scene->camera.nb_packets;
# ifdef MULTI_THREAD
#  pragma omp parallel for schedule(dynamic, SCREEN_WIDTH)
# endif
    for (i = 0; i < imax; ++i) {
      size_t j;
      color_t res_colors[4];
      color_t old_colors[4];
      float res_color_factor;
      float old_color_factor;

      mrt_process_ray_packet(res_colors, &packets[i], context->scene);

      res_color_factor = 1.f / (float)(context->iteration);
      old_color_factor = 1.f - res_color_factor;

      for (j = 0; j < 4; ++j) {
        old_colors[j] = prev_colors[i * 4 + j];
        COLOR_MUL_S(old_colors[j], old_color_factor);
        COLOR_MUL_S(res_colors[j], res_color_factor);
        COLOR_ADD(res_colors[j], old_colors[j]);
        prev_colors[i * 4 + j] = res_colors[j];
      }
    }
  }
#else
  {
    vect3_t *vectors;

    imax = context->scene->camera.width * context->scene->camera.height;
    vectors = context->scene->camera.vectors;
    vectors += imax * (context->iteration % POW2(SSAA_FACTOR));
# ifdef MULTI_THREAD
#  pragma omp parallel for schedule(dynamic, SCREEN_WIDTH)
# endif
    for (i = 0; i < imax; ++i) {
      ray_t ray;
      color_t cur_color;
      color_t old_color;
      float cur_color_factor;
      float old_color_factor;

      ray.org = context->scene->camera.org;
      ray.dir = vectors[i];
      mrt_process_ray(&cur_color, &ray, context->scene, 1.f, 0) ;

      old_color = prev_colors[i];
      cur_color_factor = 1.f / (float)(context->iteration);
      old_color_factor = 1.f - cur_color_factor;
      COLOR_MUL_S(old_color, old_color_factor);
      COLOR_MUL_S(cur_color, cur_color_factor);
      COLOR_ADD(cur_color, old_color);
      prev_colors[i] = cur_color;
    }
  }
#endif /* PACKET_RAYS */
}
