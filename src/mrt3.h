#ifndef __MRT3_H__
# define __MRT3_H__

# include <SDL2/SDL.h>

# include "color/color.h"
# include "vect/vect.h"
# include "scene/scene.h"

// resolution
# define SCREEN_WIDTH	640
# define SCREEN_HEIGHT	480

// super-sampling anti-aliasing
# ifndef SSAA_FACTOR
#  define SSAA_FACTOR 1
# endif

// default values
#define DEFAULT_MAX_ITER 256
#define MAX_ITER_MAX 1024
#define MAX_DEPTH ((size_t)5)

// lightning balance
#define BASE_AMBIANT_COLOR {0.f, 0.f, 0.f, 1.f}
#define AMBIANT_FACTOR    0.8f
#define SPECULAR_FACTOR   0.8f
#define DIFFUSE_FACTOR    0.8f

// kdtree heuristic
#define SAH_COST_TRAVERSAL 3.2f
#define SAH_COST_INTERSECT 1.f

extern color_t const SKY_COLOR;

typedef struct sdl_context_s {
  SDL_Window* window;
  SDL_Renderer* renderer;
} sdl_context_t;

typedef struct light_stat_s {
  int hit;
  int miss;
} light_stat_t;

typedef struct context_s {
  size_t iteration;
  size_t max_iteration;
  size_t nb_pixels;
  scene_t *scene;
  color_t *render_buffer;
} context_t;

typedef struct args_s {
  char error[1024];
  char *filename;
  int max_iteration;
  int quit_immediatly;
} args_t;

void mrt_process(context_t *context);

int mrt(sdl_context_t sdl, scene_t *scene, args_t const *args);

#endif /* __MRT3_H__ */
