#include <stdlib.h>
#include <stdio.h>

#include "triangle/triangle.h"
#include "scene/scene.h"

#include "intersects.h"

int mrt_find_intersection(
    triangle_intersect_data_t *intersect,
    size_t *triangle_idx,
    ray_t const *ray,
    scene_t const *scene
    )
{
  size_t i;
  float min_k;

#ifdef USE_KDTREE
# ifdef USE_KDTREE_V2
#  ifdef USE_KDTREE_ORDERED_COLLISION_CHECK
#   warning "Ordered collision check not available for KDTREE_V2"
#  endif

  kd_tree_it_t it;
  kd_tree_node_data_t *data;

  min_k = INFINITY;
  // iterate kdtree
  kdtree_iterate(&it, &scene->kdtree);
  while (1) {
    // fetch next kdtree node
    data = kdtree_next_colliding_unordered(&it, ray);
    if (data == NULL) {
      break;
    }

    // iterate triangles
    for (i = 0; i < data->triangles.nb_triangles; ++i) {
      triangle_intersect_data_t tmp_intersect;

      // test intersection
      if (triangle_ray_intersects(
            &tmp_intersect,
            ray,
            &data->triangles.triangles[i]
            )) {
        if (tmp_intersect.distance < min_k) {
          min_k = tmp_intersect.distance;
          *intersect = tmp_intersect;
          *triangle_idx = data->triangles.indices[i];
        }
      }
    }
  }

# else // USE_KDTREE_V2 | kdtree v1 below
#  ifdef USE_KDTREE_ORDERED_COLLISION_CHECK
  kd_tree_it_t it;
  kd_tree_node_data_t *data;
  float max_dist;
  int can_break;

  min_k = INFINITY;
  // iterate kdtree
  kdtree_iterate(&it, &scene->kdtree);
  while (NULL != (data = kdtree_next_colliding_ordered(
          &can_break, &max_dist, &it, ray
          ))) {
    // iterate triangle
    for (i = 0; i < data->nb_triangles; ++i) {
      triangle_intersect_data_t tmp_intersect;

      if (triangle_ray_intersects(
            &tmp_intersect,
            ray,
            &data->triangles[i].triangle)
          ) {
        if (tmp_intersect.distance < min_k) {
          if (tmp_intersect.distance > max_dist) {
            can_break = 0;
          }
          min_k = tmp_intersect.distance;
          *intersect = tmp_intersect;
          *triangle_idx = data->triangles[i].index;
        }
      }
    }

    if (can_break && min_k < INFINITY) {
      break;
    }
  }

#  else // unordered kd-tree collision check
//# define CHEAP_HASH_TABLE
#   ifdef CHEAP_HASH_TABLE
  size_t j;
  size_t seen_triangles_idx;
  unsigned int seen_triangles[8];
#   endif
  kd_tree_it_t it;
  kd_tree_node_data_t *data;

  min_k = INFINITY;
#   ifdef CHEAP_HASH_TABLE
  seen_triangles_idx = 0;
  for (j = 0; j < SIZEOF_ARRAY(seen_triangles); ++j) {
    seen_triangles[j] = UINT32_MAX;
  }
#   endif
  // iterate kdtree
  kdtree_iterate(&it, &scene->kdtree);
  while (1) {
    // fetch next kdtree node
    data = kdtree_next_colliding_unordered(&it, ray);
    if (data == NULL) {
      break;
    }

    // iterate triangles
    for (i = 0; i < data->nb_triangles; ++i) {
      triangle_intersect_data_t tmp_intersect;

#   ifdef CHEAP_HASH_TABLE
#    if 1
      // if the triangle is in the seen list, a intersection
      // check has already been made and failled
      int seen = 0;
      unsigned int triangle_index = data->triangles[i].index;
      for (j = 0; j < SIZEOF_ARRAY(seen_triangles); ++j) {
        seen = seen || (seen_triangles[j] == triangle_index);
      }
      if (seen) {
        continue;
      }
#    endif
#   endif

      // test intersection
      if (triangle_ray_intersects(
            &tmp_intersect,
            ray,
            &data->triangles[i].triangle
            )) {
        if (tmp_intersect.distance < min_k) {
          min_k = tmp_intersect.distance;
          *intersect = tmp_intersect;
          *triangle_idx = data->triangles[i].index;
        }
#   ifdef CHEAP_HASH_TABLE
      } else {
        // if the ray did not intersect the triangle, add the triangle
        // to the seen list
        seen_triangles[seen_triangles_idx] = data->triangles[i].index;
        seen_triangles_idx += 1;
        if (seen_triangles_idx >= SIZEOF_ARRAY(seen_triangles)) {
          seen_triangles_idx = 0;
        }
#   endif
      }
    }
  }

#  endif // USE_KDTREE_ORDERED_COLLISION_CHECK
# endif // USE_KDTREE_V2
#else // old implementation

  // find nearest triangle
  min_k = INFINITY;
  for (i = 0; i < scene->nb_triangles; ++i) {
    triangle_intersect_data_t tmp_intersect;

    if (triangle_ray_intersects(&tmp_intersect, ray, &scene->triangles[i])) {
      if (tmp_intersect.distance < min_k) {
        min_k = tmp_intersect.distance;
        *intersect = tmp_intersect;
        *triangle_idx = i;
      }
    }
  }
#endif // USE_KDTREE

  // no hit
  if (min_k == INFINITY) {
    return 0;
  }

  return 1;
}

#ifdef PACKET_RAYS

void mrt_find_packet_intersection(
    intersection_t intersections[4],
    ray_packet_t const *packet,
    scene_t const *scene
    )
{
# ifdef USE_KDTREE
#  ifdef USE_KDTREE_V2
#   error "not implemented"
#  endif
  size_t i;
  kd_tree_it_t it;

  // init intersections argument
  for (i = 0; i < 4; ++i) {
    intersections[i].data.distance = INFINITY;
    intersections[i].data.u = 0.f;
    intersections[i].data.v = 0.f;
    intersections[i].triangle_idx = (size_t)-1;
  }

  kdtree_iterate(&it, &scene->kdtree);
  while (1) {
    size_t j;
    kd_tree_node_data_t *data;

    // fetch kdtree node
    data = kdtree_next_packet_colliding_unordered(&it, packet);
    if (data == NULL) {
      break;
    }

    // iterate triangles
    for (i = 0; i < data->nb_triangles; ++i) {
      triangle_intersect_packet_data_t new_intersections;

      triangle_ray_packet_intersects(
          &new_intersections,
          packet,
          &data->triangles[i].triangle
          );

      // TODO: optimize
      for (j = 0; j < 4; ++j) {
        if (new_intersections.distance[j] > 0.f &&
            new_intersections.distance[j] < intersections[j].data.distance) {
          intersections[j].data.distance = new_intersections.distance[j];
          intersections[j].data.u = new_intersections.u[j];
          intersections[j].data.v = new_intersections.v[j];
          intersections[j].triangle_idx = data->triangles[i].index;
        }
      }
    }
  }
# else
#  error "not implemented"
# endif /* USE_KDTREE */
}

#endif /* RAY_PACKET */
