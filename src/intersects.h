#ifndef __INTERSECTS_H__
# define __INTERSECTS_H__ 

# include "triangle/triangle.h"

typedef struct intersection_s {
	triangle_intersect_data_t data;
	size_t triangle_idx;
} intersection_t;

int mrt_find_intersection(
    triangle_intersect_data_t *intersect,
    size_t *triangle_idx,
    ray_t const *ray,
    scene_t const *scene
    );

void mrt_find_packet_intersection(
    intersection_t intersections[4],
    ray_packet_t const *packet,
    scene_t const *scene
    );

#endif /* __INTERSECTS_H__ */
