#include <assert.h>
#include <stdio.h>
#include <string.h>

#if MULTI_THREAD
# include <omp.h>
#endif

#include "otree.h"

#include "vect/sse.h"

static size_t qtree_compute_nb_nodes(size_t depth)
{
  if (depth == 0) {
    return 1;
  }
  return qtree_compute_nb_nodes(depth - 1) + pow(8, depth);
}

static void otree_init_node(
    o_tree_t *tree,
    o_tree_node_t *node,
    bbox_aa_t const *bounds,
    size_t remaining_depths
    )
{
  // set node bbox
  node->bounds.origin = bounds->origin;
  VECT3_ADD(node->bounds.opposit, bounds->origin, bounds->dimensions);

  if (remaining_depths == 0) {
    // if leaf, allocate data and update data index
    node->data = tree->data + tree->total_data_size;
    tree->total_data_size += tree->data_size;
    memset(node->children, 0, sizeof(node->children));
  } else {
    // if node, compute each bbox and recurse
    int i;
    bbox_aa_t new_bounds;
    vect3_t new_position[2];

    // compute the dimension of the new bounding box
    // and its diffenrent possible positions
    // inside new_position = { min, max }
    // where new_position[0] is the position of the bbox
    // at a corner of the 2x2x2 cube and new_position[1]
    // is the opposit one.
    VECT3_DIV_S(new_bounds.dimensions, bounds->dimensions, 2);
    new_position[0] = bounds->origin;
    VECT3_ADD(new_position[1], new_position[0], new_bounds.dimensions);
    // slightly grow the bbox to avoid rounding errors
    //VECT3_ADD_S(new_bounds.dimensions, new_bounds.dimensions, EPSILON);
    // compute the 8 differente origins of the bbox and recurse
    for (i = 0; i < 8; ++i) {
      o_tree_node_t *new_node;
      int x, y, z;

      x = i & 0x1;
      y = (i & 0x2) >> 1;
      z = (i & 0x4) >> 2;
      new_bounds.origin.x = new_position[x].x;
      new_bounds.origin.y = new_position[y].y;
      new_bounds.origin.z = new_position[z].z;

      tree->nb_nodes++;
      new_node = tree->nodes + tree->nb_nodes;
      node->children[i] = new_node;

      otree_init_node(tree, new_node, &new_bounds, remaining_depths - 1);
    }
  }
}

static o_tree_node_t *otree_get_first_leaf(
    o_tree_node_t *node
    )
{
  if (node->children[0] == NULL) {
    return node;
  }
  return otree_get_first_leaf(node->children[0]);
}

void otree_init(
    o_tree_t *tree,
    bbox_aa_t const *bounds,
    size_t depth,
    size_t data_size
    )
{
#ifdef MULTI_THREAD
  size_t i;
  o_tree_node_t *first_leaf;
#endif
  size_t nb_nodes;
  size_t full_data_size;

  assert(depth < 11);
  full_data_size = data_size * (size_t)pow(8, depth);
  nb_nodes = qtree_compute_nb_nodes(depth);

  tree->nodes = xmalloc(sizeof(*tree->nodes) * nb_nodes);
  tree->data = xmalloc(full_data_size);
  memset(tree->data, 0, full_data_size);

  tree->data_size = data_size;
  tree->depth = depth;
  // used by initialization
  tree->nb_nodes = 0;
  tree->total_data_size = 0;

  otree_init_node(tree, tree->nodes, bounds, depth);

#ifdef MULTI_THREAD
// for profiling
//  tree->cache_hit = 0;
//  tree->cache_miss = 0;
  tree->last_returned_counter = 0;
  first_leaf = otree_get_first_leaf(tree->nodes);
  for (i = 0; i < SIZEOF_ARRAY(tree->last_returned); ++i) {
    tree->last_returned[i] = first_leaf;
  }
#else
  tree->last_returned = otree_get_first_leaf(tree->nodes);
#endif
}

void clean_otree(o_tree_t *tree)
{
  free(tree->nodes);
  free(tree->data);
  memset(tree, 0, sizeof(*tree));
}

static int otree_is_point_outside_node(
    o_tree_node_t *node,
    float x,
    float y,
    float z
    )
{
  return
    node->bounds.origin.x > x || x > node->bounds.opposit.x ||
    node->bounds.origin.y > y || y > node->bounds.opposit.y ||
    node->bounds.origin.z > z || z > node->bounds.opposit.z
    ;
}

static o_tree_node_t *otree_get_node_rec(
    o_tree_node_t *node,
    float x,
    float y,
    float z
    )
{
  int i;

  // is outside ?
  if (otree_is_point_outside_node(node, x, y, z)) {
    return NULL;
  }

  // is leaf ?
  if (node->children[0] == NULL) {
    return node;
  }

  // recurse
  for (i = 0; i < 8; ++i) {
    o_tree_node_t *res;

    res = otree_get_node_rec(node->children[i], x, y, z);
    if (res != NULL) {
      return res;
    }
  }

  puts("otree_get_node_rec: inside bounds but not in children:");
  BBOX_AA_P_PRINTF("bounds", node->bounds);
  printf("vect(%7.3f, %7.3f, %7.3f)", x, y, z);
  return NULL;
}

void *otree_get_node_data(o_tree_t *tree, float x, float y, float z)
{
#ifdef MULTI_THREAD
  int idx;
  unsigned int i;
  o_tree_node_t **ptr;
  o_tree_node_t *ret;

  // try to find a valid node in cache
  ret = NULL;
  ptr = tree->last_returned;
  for (i = 0; i < SIZEOF_ARRAY(tree->last_returned); ++i) {
    if (otree_is_point_outside_node(ptr[i], x, y, z) == 0) {
// for profiling
//# pragma omp atomic
//      tree->cache_hit++;
      return ptr[i]->data;
    }
  }

// for profiling
//# pragma omp atomic
//  tree->cache_miss++;

  // not in cache, search a node
  ret = otree_get_node_rec(tree->nodes, x, y, z);
  if (ret == NULL) {
    // no node found
    return NULL;
  }

  // write node in cache
# pragma omp atomic capture
  idx = tree->last_returned_counter++;
  ptr[idx & (int)(SIZEOF_ARRAY(tree->last_returned) - 1)] = ret;

#if 0
  // for profiling
# define TMP_STEP 65536
  if (idx > TMP_STEP)
  {
# pragma omp critical
    {
      idx = tree->last_returned_counter;
      if (idx > TMP_STEP) {
        printf("%i (%.3f) cache hit, %i cache miss\n",
            tree->cache_hit,
            (float)tree->cache_hit / (float)(tree->cache_hit + tree->cache_miss),
            tree->cache_miss
            );
        fflush(stdout);
        tree->last_returned_counter -= TMP_STEP;
        tree->cache_hit = 0;
        tree->cache_miss = 0;
      }
    }
  }
#endif

  // return node's data
  return ret->data;
#else
  o_tree_node_t *ret;

  // try the cached value
  ret = tree->last_returned;
  if (otree_is_point_outside_node(ret, x, y, z)) {
    // cached value didnt worked, search for a valid node
    ret = otree_get_node_rec(tree->nodes, x, y, z);
    if (ret == NULL) {
      // no node found
      return NULL;
    }
    // node found, cache it
    tree->last_returned = ret;
  }
  // return node's data
  return ret->data;
#endif
}

