#pragma once

#ifdef USE_KDTREE_V2
# include "kdtree2.h"
#else
# include "kdtree.h"
#endif
