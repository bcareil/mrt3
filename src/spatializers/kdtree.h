#ifndef __KDTREE_H__
# define __KDTREE_H__ 

# include "bbox.h"
# include "vect/vect.h"
# include "triangle/triangle.h"

typedef struct triangle_ref_s {
	size_t index;
	triangle_t triangle;
} triangle_ref_t;

typedef struct kd_tree_node_data_s {
	bbox_aa_t bbox;
	axis_t split_axis;
	size_t nb_triangles;
	triangle_ref_t *triangles;
} kd_tree_node_data_t;

typedef struct kd_tree_node_s {
	kd_tree_node_data_t data;
	struct kd_tree_node_s *parent;
	struct kd_tree_node_s *left;
	struct kd_tree_node_s *right;
} kd_tree_node_t;

typedef struct kd_tree_s {
	kd_tree_node_t *root;
	size_t depth;
	size_t node_pool_size;
	size_t node_pool_usage;
	kd_tree_node_t *node_pool;
} kd_tree_t;

typedef struct kd_tree_it_state_s {
	unsigned int state;
	float max_dist;
} kd_tree_it_state_t;

typedef struct kd_tree_it_s {
	kd_tree_node_t *current;
	size_t current_state;
	kd_tree_it_state_t states[24];
} kd_tree_it_t;

/**
 * Initialize a kd-tree with the given triangles
 */
void kdtree_init(
    kd_tree_t *tree,
    triangle_t const *triangles,
    size_t nb_triangles
    );

/**
 * free memory allocated by kdtree_init
 */
void clean_kdtree(
    kd_tree_t *tree
    );

/**
 * initialize a kd-tree iterator for its use in
 * kdtree_next or kdtree_next_colliding
 */
void kdtree_iterate(
    kd_tree_it_t *iterator,
    kd_tree_t const *tree
    );

/**
 * return the next node's data, from root to leaf and left to right
 */
kd_tree_node_data_t *kdtree_next_node(
    kd_tree_it_t *iterator
    );

/**
 * return the next leaf node's data, from left to right
 */
kd_tree_node_data_t *kdtree_next_leaf(
    kd_tree_it_t *iterator
    );

/**
 * return the next leaf node's data which bounding box
 * intersects the given ray.
 *
 * The ordered parameter is set to 1 if this function does
 * not need to be called again if one of the return triangle
 * is hit.
 *
 * max_dist is the maximum distance the ray has to travel to get
 * out of the bounding box and therfore is the maximum distance
 * where an oredered collision is guaranteed. If the closest
 * object intersecting object found is farther than max_dist,
 * kdtree_next_colliding must be called again.
 */
kd_tree_node_data_t *kdtree_next_colliding_ordered(
    int *ordered,
    float *max_dist,
    kd_tree_it_t *iterator,
    ray_t const *ray
    );

kd_tree_node_data_t *kdtree_next_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_t const *ray
    );

kd_tree_node_data_t *kdtree_next_packet_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_packet_t const *packet
    );

/**
 * Dump the kdtree in a waveform .obj file
 */
void kdtree_dump_to_file(kd_tree_t const *tree, char const *fname);

#endif /* __KDTREE_H__ */
