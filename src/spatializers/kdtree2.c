#ifdef USE_KDTREE_V2

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <strings.h>
#include <math.h>

#include "../mrt3.h"
#include "../sort/select.h"
#include "../sort/sort_utils.h"

#include "kdtree2.h"

/*****************************************
 ** Constants and types
 *****************************************/

// Number of triangles at which point a node is not split.
// As to be tweaked based on the ray/aabb / ray/triangle cost ratio.
#define MIN_TRIANGLES_PER_LEAF 32

typedef struct precmp_triangle_s {
  vect3_a_t min;
  vect3_a_t max;
} precmp_triangle_t;

typedef enum node_side_e {
  NS_OVERLAPPING,
  NS_LEFT,
  NS_RIGHT,
  NS_IN_PLAN,

  NS_COUNT
} node_side_t;

/*****************************************
 ** Compute AABB from triangles
 *****************************************/

static void calculate_bbox_from_triangles(
    bbox_aa_t *bbox,
    precmp_triangle_t *triangles,
    size_t nb_triangles
    )
{
  size_t i;
  vect3_t min = VECT3_POS_INF;
  vect3_t max = VECT3_MIN_INF;

  for (i = 0; i < nb_triangles; ++i) {
    min.x = MIN(min.x, triangles->min.x[i]);
    min.y = MIN(min.y, triangles->min.y[i]);
    min.z = MIN(min.z, triangles->min.z[i]);
    max.x = MAX(max.x, triangles->max.x[i]);
    max.y = MAX(max.y, triangles->max.y[i]);
    max.z = MAX(max.z, triangles->max.z[i]);
  }
  bbox->origin = min;
  VECT3_SUB(bbox->dimensions, max, min);
}

/*****************************************
 ** Precomputed triangles values related
 *****************************************/

static void calculate_triangles(
    precmp_triangle_t *precmp_triangles,
    triangle_t const *triangles,
    size_t nb_triangles
    )
{
  size_t i;

  for (i = 0; i < nb_triangles; ++i) {
    triangle_t const *t;

    t = &triangles[i];

    precmp_triangles->min.x[i] = MIN(t->v0.x, MIN(t->v1.x, t->v2.x));
    precmp_triangles->min.y[i] = MIN(t->v0.y, MIN(t->v1.y, t->v2.y));
    precmp_triangles->min.z[i] = MIN(t->v0.z, MIN(t->v1.z, t->v2.z));

    precmp_triangles->max.x[i] = MAX(t->v0.x, MAX(t->v1.x, t->v2.x));
    precmp_triangles->max.y[i] = MAX(t->v0.y, MAX(t->v1.y, t->v2.y));
    precmp_triangles->max.z[i] = MAX(t->v0.z, MAX(t->v1.z, t->v2.z));
  }
}

static void sort_precmp_triangles(
    triangles_refs_t *tr,
    precmp_triangle_t *pt,
    node_side_t *ts,
    size_t nb_triangles,
    size_t nb_triangles_overlapping,
    size_t nb_triangles_on_left
    )
{
  size_t i;
  size_t swap_dest;
  size_t remaining_t_on_side;
  node_side_t current_side;

  //printf("sizeof(precmp_triangle_t)=%zu\n", sizeof(precmp_triangle_t));
  swap_dest = nb_triangles - 1;
  current_side = NS_OVERLAPPING;
  remaining_t_on_side = nb_triangles_overlapping;
  for (i = 0; i < nb_triangles; ++i) {
    if (ts[i] != current_side) {
      while (remaining_t_on_side == 0) {
        // change current side
        swap_dest = nb_triangles - 1;
        current_side += 1;
        if (current_side == NS_LEFT) {
          remaining_t_on_side = nb_triangles_on_left;
        } else if (current_side == NS_RIGHT) {
          remaining_t_on_side = nb_triangles - i;
        } else {
          // there shouldn't be any NS_IN_PLAN
          assert(current_side > NS_RIGHT);
        }
      }

      // check again in case it changed with the above conditions
      if (ts[i] != current_side) {
        size_t j;
        float tmp_f;
        size_t tmp_s;
        triangle_t tmp_t;
        node_side_t tmp_ns;
        float **pt_as_fa;

        // swap current t with one of the current side
        while (swap_dest > i && ts[swap_dest] != current_side) {
          swap_dest--;
        }
        assert(swap_dest > i);

        // swap precomputed values
        pt_as_fa = (float **)pt;
        for (j = 0; j < 6; ++j) {
          tmp_f = pt_as_fa[j][i];
          pt_as_fa[j][i] = pt_as_fa[j][swap_dest];
          pt_as_fa[j][swap_dest] = tmp_f;
        }

        // swap triangles references
        tmp_s = tr->indices[i];
        tr->indices[i] = tr->indices[swap_dest];
        tr->indices[swap_dest] = tmp_s;
        tmp_t = tr->triangles[i];
        tr->triangles[i] = tr->triangles[swap_dest];
        tr->triangles[swap_dest] = tmp_t;

        // swap node side
        tmp_ns = ts[i];
        ts[i] = ts[swap_dest];
        ts[swap_dest] = tmp_ns;
      }
    }
    // update remaining triangles for the current side
    remaining_t_on_side--;
  }
}

/*****************************************
 ** Heuristic related
 *****************************************/

static float surface_area(
    bbox_aa_t const *voxel
    )
{
  return 2.f * (
      (voxel->dimensions.x * voxel->dimensions.y) +
      (voxel->dimensions.x * voxel->dimensions.z) +
      (voxel->dimensions.y * voxel->dimensions.z));
}

static float surface_area_heuristic(
    bbox_aa_t const *voxel_r,
    bbox_aa_t const *voxel_a,
    size_t triangles_in_a,
    bbox_aa_t const *voxel_b,
    size_t triangles_in_b
    )
{
  float root_sa;
  float score;

  root_sa = surface_area(voxel_r);
  score = SAH_COST_TRAVERSAL + SAH_COST_INTERSECT * (
      (surface_area(voxel_a) / root_sa * (float)triangles_in_a) +
      (surface_area(voxel_b) / root_sa * (float)triangles_in_b));
  if (triangles_in_a == 0 || triangles_in_b == 0) {
    score *= 0.8;
  }
  return score;
}

static float surface_area_heuristic_leaf(
    bbox_aa_t const *voxel,
    size_t nb_triangles
    )
{
  (void) voxel;
  return SAH_COST_INTERSECT * nb_triangles;
}

static float surface_area_heuristic_3(
    bbox_aa_t const *voxel_r,
    size_t triangles_in_r,
    bbox_aa_t const *voxel_a,
    size_t triangles_in_a,
    bbox_aa_t const *voxel_b,
    size_t triangles_in_b
    )
{
  return surface_area_heuristic(
      voxel_r, voxel_a, triangles_in_a, voxel_b, triangles_in_b
      ) + surface_area_heuristic_leaf(voxel_r, triangles_in_r);
}

struct fbcp_result_s {
  axis_t axis;
  float plan;
  float score;
  size_t triangles_on_left;
  size_t triangles_overlapping;
  size_t triangles_on_right;
  bbox_aa_t left_bbox;
  bbox_aa_t right_bbox;
};

static void find_best_cutting_plan(
    struct fbcp_result_s *result,
    node_side_t *result_sides,
    bbox_aa_t const *bbox,
    precmp_triangle_t const *precmp_triangles,
    size_t nb_triangles
    )
{
  axis_t axis;
  size_t i;
  size_t *indices;
  size_t min_max_count;
  float *min_max;
  float *sorted_min_max;
  unsigned char *is_max;
  unsigned char *sorted_is_max;
  bbox_aa_t left_bbox;
  bbox_aa_t right_bbox;
  node_side_t result_in_plan_side;
  node_side_t *current_sides;

  // init result to default values
  result->axis = AXIS_COUNT;
  result->plan = NAN;
  result->score = INFINITY;
  result->triangles_on_left = 0;
  result->triangles_overlapping = 0;
  result->triangles_on_right = 0;
  result_in_plan_side = NS_COUNT;

  // allocate arrays
  min_max_count = nb_triangles * 2;
  indices = xmalloc(sizeof(*indices) * min_max_count);
  min_max = xmalloc(sizeof(*min_max) * min_max_count);
  is_max = xmalloc(sizeof(*is_max) * min_max_count);
  sorted_min_max = xmalloc(sizeof(*sorted_min_max) * min_max_count);
  sorted_is_max = xmalloc(sizeof(*sorted_is_max) * min_max_count);
  current_sides = xmalloc(sizeof(*current_sides) * nb_triangles);

  // find best cut plan for every axis
  for (axis = 0; axis < AXIS_COUNT; ++axis) {
    size_t j, k;
    size_t prev_i;
    size_t passed[2];
    size_t triangles_on_left;
    size_t triangles_on_right;
    size_t triangles_overlapping;
    size_t triangles_in_plan;
    float bbox_lower_bound;
    float bbox_upper_bound;

    // init bbox to default values
    left_bbox = *bbox;
    right_bbox = *bbox;

    // axis-specific values
    bbox_lower_bound = VECT_COMP(bbox->origin, axis);
    bbox_upper_bound = bbox_lower_bound + VECT_COMP(bbox->dimensions, axis);

    // init current_sides, all triangles are on the right
    for (i = 0; i < nb_triangles; ++i) {
      current_sides[i] = NS_RIGHT;
    }

    // init min_max, min is copied before max
    for (i = 0; i < nb_triangles; ++i) {
      min_max[i] = (&precmp_triangles->min.x)[axis][i];
    }
    for (i = 0; i < nb_triangles; ++i) {
      min_max[i + nb_triangles] = (&precmp_triangles->max.x)[axis][i];
    }

    // init is_max accordingly
    for (i = 0; i < nb_triangles; ++i) {
      is_max[i] = 0;
      is_max[i + nb_triangles] = 1;
    }

    // get sorted indices for min_max
    float_argsort(indices, min_max, min_max_count);

    // sort min_max and is_max using the sorted indices
    float_arrage_from_indices(
        sorted_min_max, min_max, indices, min_max_count
        );
    arrange_from_indices(
        sorted_is_max, is_max, indices, min_max_count, sizeof(*is_max)
        );

    // debug
#ifndef NDEBUG
    float_indices_assert_sorted(min_max, indices, min_max_count);
    float_assert_sorted(sorted_min_max, min_max_count);
#endif

    // init initial state
    triangles_on_right = nb_triangles;
    triangles_overlapping = 0;
    triangles_on_left = 0;

    // loop over sorted_min_max
    i = 0;
    prev_i = 0;
    while (i < min_max_count) {
      float current;

      // count the number of min and max which have the same values
      current = sorted_min_max[i];
      passed[0] = 0;
      passed[1] = 0;
      triangles_in_plan = 0;
      for (j = i; j < min_max_count && current == sorted_min_max[j]; ++j) {
        int in_plan;

        // categorize triangle depending on which ends we meet
        // NOTE: there is a case where we can meet the max before the min.
        //       This case is handled below
        passed[sorted_is_max[j]] += 1;
        if (sorted_is_max[j]) {
          current_sides[indices[j] % nb_triangles] = NS_LEFT;
        }

        // determine if triangle is in the plane
        in_plan = 0;
        for (k = i; k < j && in_plan == 0; ++k) {
          in_plan = (indices[k] == indices[j]
                     || (indices[k] + nb_triangles) == indices[j]
                     || indices[k] == (indices[j] + nb_triangles));
        }
        if (in_plan) {
          triangles_in_plan++;
          current_sides[indices[j] % nb_triangles] = NS_IN_PLAN;
        }
      }
      i = j;

      // sanity checks
      assert(passed[0] >= triangles_in_plan);
      assert(passed[1] >= triangles_in_plan);
      assert(triangles_on_right >= triangles_in_plan);
      assert(triangles_overlapping >= (passed[1] - triangles_in_plan));

      // Update the triangles count for each category:
      // - for each max encountered without a corresponding min
      // ---> TL += 1; TO -= 1;
      // - for each max encountered with a corresponding min
      // ---> TR -= 1; TI += 1;
      // - for each remaining min encountered
      // ---> NOOP, will be processed later
      // Where
      // TR = triangles_on_right
      // TL = triangles_on_left
      // TO = triangles_overlapping
      // TI = triangles_in_plan
      // passed[0] = encountered min
      // passed[1] = encountered max
      // But TI is already computed
      passed[0] -= triangles_in_plan;
      passed[1] -= triangles_in_plan;
      triangles_on_right -= triangles_in_plan;
      triangles_on_left += passed[1];
      triangles_overlapping -= passed[1];

      // ensure the plan is inside the bbox
      if (current > bbox_lower_bound && current < bbox_upper_bound) {
        node_side_t in_plan_side;
        float score_in_plan_left;
        float score_in_plan_right;
        float score;

        // update bbox
        VECT_COMP(left_bbox.dimensions, axis) =
            current - bbox_lower_bound;
        VECT_COMP(right_bbox.origin, axis) = current;
        VECT_COMP(right_bbox.dimensions, axis) =
            bbox_upper_bound - current;

        // compute heuristics
        /* score = surface_area_heuristic( */
        /*     bbox, */
        /*     &left_bbox, */
        /*     triangles_on_left + triangles_overlapping, */
        /*     &right_bbox, */
        /*     triangles_on_right + triangles_overlapping */
        /*     ); */
        score_in_plan_left = surface_area_heuristic_3(
            bbox,
            triangles_overlapping,
            &left_bbox,
            triangles_on_left + triangles_in_plan,
            &right_bbox,
            triangles_on_right
            );
        score_in_plan_right = surface_area_heuristic_3(
            bbox,
            triangles_overlapping,
            &left_bbox,
            triangles_on_left,
            &right_bbox,
            triangles_on_right + triangles_in_plan
            );

        // determine which side goes the triangles inside the cutting plans
        if (score_in_plan_left < score_in_plan_right) {
          score = score_in_plan_left;
          in_plan_side = NS_LEFT;
        } else {
          score = score_in_plan_right;
          in_plan_side = NS_RIGHT;
        }

        // debug
        /* printf("%u %5zu %8.2f %i %.3f %zu|%zu|%zu\n", */
        /*        axis, i, score, in_plan_side, current, */
        /*        triangles_on_left, triangles_overlapping, triangles_on_right); */

        // update best score
        if (score < result->score) {
          result->score = score;
          result->axis = axis;
          result->plan = current;
          result->triangles_on_left = triangles_on_left;
          result->triangles_on_right = triangles_on_right;
          result->triangles_overlapping = triangles_overlapping;
          result->left_bbox = left_bbox;
          result->right_bbox = right_bbox;
          memcpy(result_sides, current_sides, nb_triangles * sizeof(*current_sides));
          result_in_plan_side = in_plan_side;
          if (in_plan_side == NS_LEFT) {
            result->triangles_on_left += triangles_in_plan;
          } else {
            result->triangles_on_right += triangles_in_plan;
          }

          // sanity check
          assert(nb_triangles == (result->triangles_on_left
                                  + result->triangles_on_right
                                  + result->triangles_overlapping));
        }
      }

      // Triangle category post-update:
      // previously in-plan triangles are now on the left
      // and triangles on the right are now overlapping
      triangles_overlapping += passed[0];
      triangles_on_right -= passed[0];
      triangles_on_left += triangles_in_plan;
      for (j = prev_i; j < i; ++j) {
        const size_t idx = indices[j] % nb_triangles;
        if (current_sides[idx] == NS_IN_PLAN) {
          current_sides[idx] = NS_LEFT;
        } else if (current_sides[idx] == NS_RIGHT) {
          current_sides[idx] = NS_OVERLAPPING;
        }
      }
      prev_i = i;
    }
  }

  // free arrays
  free(indices);
  free(min_max);
  free(is_max);
  free(sorted_min_max);
  free(sorted_is_max);
  free(current_sides);

  // sanity check
  assert(result_in_plan_side == NS_LEFT || result_in_plan_side == NS_RIGHT);

  // move the in-plans triangles inside the correct side
  for (i = 0; i < nb_triangles; ++i) {
    if (result_sides[i] == NS_IN_PLAN) {
      result_sides[i] = result_in_plan_side;
    }
  }
}

/*****************************************
 ** Debug related
 *****************************************/

#if 0

static int float_not_equal(float const a, float const b)
{
  return fabs(a - b) > 0.0001;
}

static void print_precomputed_values_mismatch(
    kd_tree_t const *tree,
    precmp_triangle_t const *precmp_triangles,
    triangles_refs_t const *triangles_refs,
    size_t nb_triangles
    )
{
  size_t i, j;
  precmp_triangle_t local_precmp_ts;
  float **local_pts;
  float * const *given_pts;

  local_precmp_ts.min.x = xmalloc(sizeof(float) * 6 * nb_triangles);
  local_precmp_ts.min.y = local_precmp_ts.min.x + nb_triangles;
  local_precmp_ts.min.z = local_precmp_ts.min.y + nb_triangles;
  local_precmp_ts.max.x = local_precmp_ts.min.z + nb_triangles;
  local_precmp_ts.max.y = local_precmp_ts.max.x + nb_triangles;
  local_precmp_ts.max.z = local_precmp_ts.max.y + nb_triangles;

  calculate_triangles(&local_precmp_ts, triangles_refs->triangles, nb_triangles);

  local_pts = &local_precmp_ts.min.x;
  given_pts = &precmp_triangles->min.x;
  for (i = 0; i < nb_triangles; ++i) {
    for (j = 0; j < 6; ++j) {
      if (float_not_equal(local_pts[j][i], given_pts[j][i])) {
	printf("** precomputed values for triangle %zu missmatch **\n",
	       triangles_refs->indices[i]);
	printf("local index: %zu\n", i);
	printf("global index: %zu\n",
	       &triangles_refs->triangles[i] - tree->triangles_refs.triangles);
	TRIANGLE_PRINTF("t", triangles_refs->triangles[i]);
	printf("Associated precomputed values:\n");
	printf("- min (%.3f, %.3f, %.3f)\n",
	       precmp_triangles->min.x[i],
	       precmp_triangles->min.y[i],
	       precmp_triangles->min.z[i]);
	printf("- max (%.3f, %.3f, %.3f)\n",
	       precmp_triangles->max.x[i],
	       precmp_triangles->max.y[i],
	       precmp_triangles->max.z[i]);
	// skip other precomputed values for this triangle
	break;
      }
    }
  }

  free(local_precmp_ts.min.x);
}

static void print_triangles_outside_bbox(
    kd_tree_t const *tree,
    bbox_aa_t const *bbox,
    precmp_triangle_t *precmp_triangles,
    triangles_refs_t const *triangles_refs,
    size_t nb_triangles
    )
{
  size_t i, j;
  int printed_bbox;

  printed_bbox = 0;
  for (i = 0; i < nb_triangles; ++i) {
    vect3_t *vertices;

    vertices = &triangles_refs->triangles[i].v0;
    for (j = 0; j < 3; ++j) {
      if (vertices[j].x < bbox->origin.x
	  || vertices[j].y < bbox->origin.y
	  || vertices[j].z < bbox->origin.z
	  || vertices[j].x > bbox->origin.x + bbox->dimensions.x
	  || vertices[j].y > bbox->origin.y + bbox->dimensions.y
	  || vertices[j].z > bbox->origin.z + bbox->dimensions.z
	  ) {
	if (printed_bbox == 0) {
	  printed_bbox = 1;
	  BBOX_AA_PRINTF("bbox", *bbox);
	}
	printf("** triangle %zu outside of bbox **\n", triangles_refs->indices[i]);
	printf("local index: %zu\n", i);
	printf("global index: %zu\n",
	       &triangles_refs->triangles[i] - tree->triangles_refs.triangles);
	TRIANGLE_PRINTF("t", triangles_refs->triangles[i]);
	printf("Associated precomputed values:\n");
	printf("- min (%.3f, %.3f, %.3f)\n",
	       precmp_triangles->min.x[i],
	       precmp_triangles->min.y[i],
	       precmp_triangles->min.z[i]);
	printf("- max (%.3f, %.3f, %.3f)\n",
	       precmp_triangles->max.x[i],
	       precmp_triangles->max.y[i],
	       precmp_triangles->max.z[i]);
	// skip other vertices
	break;
      }
    }
  }
}

#endif

/*****************************************
 ** Tree building
 *****************************************/

// forward declarations
static kd_tree_node_t *fill_rec(
    kd_tree_t *tree,
    bbox_aa_t const *bbox,
    size_t nb_triangles,
    triangles_refs_t *triangles_refs,
    precmp_triangle_t *precmp_triangles,
    size_t remaining_depth
    );

static void make_leaf(
    kd_tree_node_t *node,
    bbox_aa_t const *bbox,
    triangles_refs_t *triangles_refs,
    size_t nb_triangles
    )
{
  // this is a leaf node or forced to be a leaf if it cannot be split
  node->left = NULL;
  node->right = NULL;
  node->data.bbox = *bbox;
  node->data.split_axis = AXIS_COUNT;

  node->data.triangles.nb_triangles = nb_triangles;
  if (nb_triangles == 0) {
    /* puts("INFO: kdtree: leaf has no nodes"); */
    node->data.triangles.indices = NULL;
    node->data.triangles.triangles = NULL;
  } else {
    node->data.triangles = *triangles_refs;
  }
}

static void split_node(
    kd_tree_t *tree,
    kd_tree_node_t *node,
    struct fbcp_result_s const *fbcp_result,
    node_side_t *triangles_sides,
    precmp_triangle_t *precmp_triangles,
    triangles_refs_t *triangles_refs,
    size_t nb_triangles,
    size_t remaining_depth
    )
{
  precmp_triangle_t precmp_triangles_left;
  precmp_triangle_t precmp_triangles_right;
  triangles_refs_t left_node_tr;
  triangles_refs_t right_node_tr;

  // set split axis
  node->data.split_axis = fbcp_result->axis;

  // sort precomputed triangles values
  sort_precmp_triangles(
      triangles_refs,
      precmp_triangles,
      triangles_sides,
      nb_triangles,
      fbcp_result->triangles_overlapping,
      fbcp_result->triangles_on_left
      );

  // fetch triangles empl in tree triangles refs
  node->data.triangles.nb_triangles = fbcp_result->triangles_overlapping;
  node->data.triangles.indices = triangles_refs->indices;
  node->data.triangles.triangles = triangles_refs->triangles;

  // init triangles_refs for children
  left_node_tr.nb_triangles = fbcp_result->triangles_on_left;
  left_node_tr.indices =
      node->data.triangles.indices + fbcp_result->triangles_overlapping;
  left_node_tr.triangles =
      node->data.triangles.triangles + fbcp_result->triangles_overlapping;

  right_node_tr.nb_triangles = fbcp_result->triangles_on_right;
  right_node_tr.indices =
      left_node_tr.indices + fbcp_result->triangles_on_left;
  right_node_tr.triangles =
      left_node_tr.triangles + fbcp_result->triangles_on_left;

  // init precmp_triangles for children
  precmp_triangles_left = *precmp_triangles;

  precmp_triangles_left.min.x += fbcp_result->triangles_overlapping;
  precmp_triangles_left.min.y += fbcp_result->triangles_overlapping;
  precmp_triangles_left.min.z += fbcp_result->triangles_overlapping;
  precmp_triangles_left.max.x += fbcp_result->triangles_overlapping;
  precmp_triangles_left.max.y += fbcp_result->triangles_overlapping;
  precmp_triangles_left.max.z += fbcp_result->triangles_overlapping;

  precmp_triangles_right = precmp_triangles_left;

  precmp_triangles_right.min.x += fbcp_result->triangles_on_left;
  precmp_triangles_right.min.y += fbcp_result->triangles_on_left;
  precmp_triangles_right.min.z += fbcp_result->triangles_on_left;
  precmp_triangles_right.max.x += fbcp_result->triangles_on_left;
  precmp_triangles_right.max.y += fbcp_result->triangles_on_left;
  precmp_triangles_right.max.z += fbcp_result->triangles_on_left;

  // recurse on the left
  node->left = fill_rec(
      tree,
      &fbcp_result->left_bbox,
      fbcp_result->triangles_on_left,
      &left_node_tr,
      &precmp_triangles_left,
      remaining_depth - 1
      );
  node->left->parent = node;

  // recurse on the right
  node->right = fill_rec(
      tree,
      &fbcp_result->right_bbox,
      fbcp_result->triangles_on_right,
      &right_node_tr,
      &precmp_triangles_right,
      remaining_depth - 1
      );
  node->right->parent = node;
}

static kd_tree_node_t *fill_rec(
    kd_tree_t *tree,
    bbox_aa_t const *bbox,
    size_t nb_triangles,
    triangles_refs_t *triangles_refs,
    precmp_triangle_t *precmp_triangles,
    size_t remaining_depth
    )
{
  kd_tree_node_t *node;

  // debug
  /* puts("================================="); */
  /* printf("fill_rec at depth %i\n", -(int)remaining_depth); */
  /* printf("recieved %zu triangles\n", nb_triangles); */
  assert(triangles_refs->nb_triangles == nb_triangles);

  // fetch a pre-allocated node from the node pool
  assert(tree->node_pool_usage < tree->node_pool_size);
  node = &tree->node_pool[tree->node_pool_usage];
  tree->node_pool_usage += 1;

  // assign data from args
  node->data.bbox = *bbox;

  // default values
  node->data.triangles.nb_triangles = 0;
  node->data.triangles.indices = NULL;
  node->data.triangles.triangles = NULL;

#if 0
  // debug
  print_precomputed_values_mismatch(
      tree, precmp_triangles, triangles_refs, nb_triangles
      );
  print_triangles_outside_bbox(
      tree, bbox, precmp_triangles, triangles_refs, nb_triangles
      );
#endif

  if (remaining_depth == 0 ||
      nb_triangles < MIN_TRIANGLES_PER_LEAF) {
    make_leaf(node, bbox, triangles_refs, nb_triangles);
  } else {
    struct fbcp_result_s fbcp_result;
    node_side_t *sides;
    float score_as_leaf;

    // allocate array
    sides = xmalloc(sizeof(*sides) * nb_triangles);

    // compute heuristics
    find_best_cutting_plan(&fbcp_result, sides, bbox, precmp_triangles, nb_triangles);
    score_as_leaf = surface_area_heuristic_leaf(bbox, nb_triangles);

    // debug
    /* printf("heuristic as a leaf: %.3f\n", score_as_leaf); */
    /* printf("best heuristic: %u %8.3f %+5.3f %zu|%zu|%zu\n", */
    /* 	   fbcp_result.axis, fbcp_result.score, fbcp_result.plan, */
    /* 	   fbcp_result.triangles_on_left, fbcp_result.triangles_overlapping, */
    /* 	   fbcp_result.triangles_on_right); */

    if (score_as_leaf < fbcp_result.score) {
      /* puts("Converting to leaf"); */
      make_leaf(node, bbox, triangles_refs, nb_triangles);
    } else {
      split_node(
	  tree,
	  node,
	  &fbcp_result,
	  sides,
	  precmp_triangles,
	  triangles_refs,
	  nb_triangles,
	  remaining_depth
	  );
    }

    // free array
    free(sides);
  }

  // return the used node
  return node;
}

 static void prioritized_node_spliting(
     kd_tree_t *tree
     )
 {
   // TODO: better prioritizing
   kd_tree_it_t it;
   kd_tree_node_t *node;
   node_side_t *sides;
   int done;

   // get a valid pointer for realloc
   sides = xmalloc(0);

   done = 1;
   while (done && (tree->node_pool_size - tree->node_pool_usage) > 2) {
     // while there is still some non-allocated nodes
     kdtree_iterate(&it, tree);

     // find and split first candiate
     done = 0;
     while ((node = kdtree_next_leaf(&it)) != NULL
	 && (tree->node_pool_size - tree->node_pool_usage) > 2) {
       if (node->data.triangles.nb_triangles > MIN_TRIANGLES_PER_LEAF) {
	 size_t const nb_triangles = node->data.triangles.nb_triangles;
	 struct fbcp_result_s fbcp_result;
	 precmp_triangle_t precmp_triangles;
	 float score_as_leaf;

	 // update array size
	 sides = xrealloc(sides, sizeof(*sides) * nb_triangles);

	 precmp_triangles.min.x = xmalloc(sizeof(float) * nb_triangles * 6);
	 precmp_triangles.min.y = precmp_triangles.min.x + nb_triangles;
	 precmp_triangles.min.z = precmp_triangles.min.y + nb_triangles;
	 precmp_triangles.max.x = precmp_triangles.min.z + nb_triangles;
	 precmp_triangles.max.y = precmp_triangles.max.x + nb_triangles;
	 precmp_triangles.max.z = precmp_triangles.max.y + nb_triangles;

	 calculate_triangles(
	     &precmp_triangles, node->data.triangles.triangles, nb_triangles
	     );

	 find_best_cutting_plan(
	     &fbcp_result, sides, &node->data.bbox, &precmp_triangles, nb_triangles
	     );

	 score_as_leaf = surface_area_heuristic_leaf(
	     &node->data.bbox, nb_triangles
	     );

	 // debug
	 /* printf("heuristic as a leaf: %.3f\n", score_as_leaf); */
	 /* printf("best heuristic: %u %8.3f %+5.3f %zu|%zu|%zu\n", */
	 /* 	fbcp_result.axis, fbcp_result.score, fbcp_result.plan, */
	 /* 	fbcp_result.triangles_on_left, fbcp_result.triangles_overlapping, */
	 /* 	fbcp_result.triangles_on_right); */

	 // split node if revelant
	 if (score_as_leaf >= fbcp_result.score) {
	   split_node(
	       tree,
	       node,
	       &fbcp_result,
	       sides,
	       &precmp_triangles,
	       &node->data.triangles,
	       nb_triangles,
	       1
	       );
	   done = 1;
	 }

	 // cleanup
	 free(precmp_triangles.min.x);

	 if (done) {
	   break;
	 }
       }
     }
   }

   // free array
   free(sides);
 }

void kdtree_init(
    kd_tree_t *tree,
    triangle_t const *triangles,
    size_t nb_triangles
    )
{
  size_t i;
  size_t max_depth;
  bbox_aa_t root_bbox;
  precmp_triangle_t precmp_triangles;

  // pre-compute some value for triangle's process
  precmp_triangles.min.x = xmalloc(sizeof(float) * nb_triangles * 6);
  precmp_triangles.min.y = precmp_triangles.min.x + nb_triangles;
  precmp_triangles.min.z = precmp_triangles.min.y + nb_triangles;

  precmp_triangles.max.x = precmp_triangles.min.z + nb_triangles;
  precmp_triangles.max.y = precmp_triangles.max.x + nb_triangles;
  precmp_triangles.max.z = precmp_triangles.max.y + nb_triangles;

  calculate_triangles(&precmp_triangles, triangles, nb_triangles);

  // allocate tree's triangles ref
  tree->triangles_refs.nb_triangles = nb_triangles;
  tree->triangles_refs.indices = xmalloc(
      sizeof(*tree->triangles_refs.indices) * nb_triangles
      );
  tree->triangles_refs.triangles = xmalloc(
      sizeof(*tree->triangles_refs.triangles) * nb_triangles
      );

  for (i = 0; i < nb_triangles; ++i) {
    tree->triangles_refs.indices[i] = i;
    tree->triangles_refs.triangles[i] = triangles[i];
  }

  // compute root node's bbox
  calculate_bbox_from_triangles(&root_bbox, &precmp_triangles, nb_triangles);
  bbox_grow(&root_bbox, 0.002f);

  // evaluate maximum depth
  max_depth = MAX(1.f, 1.4f * logf(MAX(1, nb_triangles)));

  // allocate node pool
  tree->depth = max_depth;
  tree->node_pool_size = pow(2, max_depth);
  tree->node_pool_usage = 0;
  tree->node_pool = xmalloc(sizeof(*tree->node_pool) * tree->node_pool_size);

  // debug
  /* printf("kdtree: initializing a tree of max depth %zu and containing %zu triangles\n", */
  /* 	 max_depth, nb_triangles); */

  // fill tree recursively
  tree->root = fill_rec(
      tree,
      &root_bbox,
      nb_triangles,
      &tree->triangles_refs,
      &precmp_triangles,
      max_depth - 1
      );
  tree->root->parent = NULL;

  // cleanup
  free(precmp_triangles.min.x);

  // continue spliting node as long as we have free nodes
  /* puts("=========================================="); */
  /* puts("==          Post processing             =="); */
  /* puts("=========================================="); */
  prioritized_node_spliting(tree);
}

void clean_kdtree(
    kd_tree_t *tree
    )
{
  free(tree->node_pool);
  free(tree->triangles_refs.indices);
  free(tree->triangles_refs.triangles);
  bzero(tree, sizeof(*tree));
}

/****************************************
 * Kd-tree standard iteration functions
****************************************/

void kdtree_iterate(
    kd_tree_it_t *iterator,
    kd_tree_t const *tree
    )
{
  bzero(iterator, sizeof(*iterator));
  iterator->current = tree->root;
}

kd_tree_node_data_t *kdtree_next_node(
    kd_tree_it_t *iterator
    )
{
  kd_tree_node_data_t *data;

  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  // pointer to return
  data = &iterator->current->data;

  // try go left first
  if (iterator->current->left != NULL) {
    iterator->current = iterator->current->left;
    return data;
  }

  // we are at a leaf, go to the parent
  while (1) {
    if (iterator->current->parent == NULL) {
      // we came back at the root
      iterator->current = NULL;
      break;
    } else if (iterator->current->parent->right != iterator->current) {
      // if we are not the node on the right, go right
      iterator->current = iterator->current->parent->right;
      break;
    } else {
      // if we are the node on the right, go back
      iterator->current = iterator->current->parent;
    }
  }

  // return saved pointer
  return data;
}

kd_tree_node_t *kdtree_next_leaf(
    kd_tree_it_t *iterator
    )
{
  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  // if we cant go to the left
  if (iterator->current->left == NULL) {
    if (iterator->current->right == NULL) {
      // if we cant go to the right right-away, get to the parent
      kd_tree_node_t *old;

      do {
        old = iterator->current;
        iterator->current = old->parent;
        // if we got back to the root, return
        if (iterator->current == NULL) {
          return NULL;
        }
        // otherwise, continue to get back as long as we come from the right
      } while (iterator->current->right == old
          || iterator->current->right == NULL);
    }
    // if we can go to the right, go to the right once
    iterator->current = iterator->current->right;
  }

  // go to the leaf node the most to the left
  while (iterator->current->left != NULL) {
    iterator->current = iterator->current->left;
  }

  return iterator->current;
}

kd_tree_node_data_t *kdtree_next_leaf_data(
    kd_tree_it_t *iterator
    )
{
  kd_tree_node_t *node;

  node = kdtree_next_leaf(iterator);
  if (node == NULL) {
    return NULL;
  }
  return &node->data;
}

/****************************************
 * Kd-tree advanced iteration tools
****************************************/

enum kdtree_next_colliding_state_e {
  KNCS_UNEXPLORED   = 0x00,
  KNCS_WENT_LEFT    = 0x01,
  KNCS_WENT_RIGHT   = 0x02,
  KNCS_EXPLORED     = 0x03,
  KNCS_RET_LOCAL    = 0x04,
  KNCS_UNORDERED    = 0x08,
};

enum kdtree_next_colliding_operation_e {
  KNCO_GO_LEFT      = 1,
  KNCO_GO_RIGHT     = 2,
  KNCO_GO_PARENT    = 3,
};

enum kdtree_hit_e {
  KNCH_NO_HIT     = 0x00,
  KNCH_HIT_LEFT   = 0x01,
  KNCH_HIT_RIGHT  = 0x02,
};

#define WENT_ONLY_RIGHT(s)  ((s & KNCS_EXPLORED) == KNCS_WENT_RIGHT)
#define WENT_ONLY_LEFT(s)   ((s & KNCS_EXPLORED) == KNCS_WENT_LEFT)
#define IS_EXPLORED(s)      ((s & KNCS_EXPLORED) == KNCS_EXPLORED)
#define IS_UNEXPLORED(s)    ((s & KNCS_EXPLORED) == KNCS_UNEXPLORED)
#define IS_UNORDERED(s)     ((s & KNCS_UNORDERED))
#define IS_ORDERED(s)       ((s & KNCS_UNORDERED) == 0)
#define RETURNED_LOCAL_TRIANGLES(s) ((s & KNCS_RET_LOCAL) != 0)

static int knc_explore_node(
    vect3_t *intersection,
    kd_tree_node_t const *node,
    ray_t const *ray
    )
{
  return bbox_aa_ray_intersects(
      intersection,
      &node->data.bbox,
      ray
      );
}

#ifdef PACKET_RAYS

static int knc_packet_explore_node(
    kd_tree_node_t const *node,
    ray_packet_t const *packet
    )
{
  int hit = 0;

  // TODO
#error "not implemented"

  return hit;
}

#endif // PACKET_RAYS

static int knc(
    kd_tree_it_ord_t *iterator,
    ray_t const *ray,
    bool collided,
    int (*explore_node_callback)(vect3_t *, kd_tree_node_t const *, ray_t const *)
    )
{
  (void) iterator;
  (void) ray;
  (void) collided;
  (void) explore_node_callback;
  return 0;
}

int kdtree_next_colliding(
    kd_tree_it_ord_t *iterator,
    ray_t const *ray,
    bool collided
    )
{
  return knc(iterator, ray, collided, knc_explore_node);
}

/***************************************
 * Unordered hit test callbacks
****************************************/

typedef int (*kncu_hit_test_callback_t)(kd_tree_node_t *, void const *);

static int kncu_ray_intersect_callback(
    kd_tree_node_t *node,
    void const *ray
    )
{
  vect3_t tmp;

  return knc_explore_node(&tmp, node, (ray_t const *)ray);
}

#ifdef PACKET_RAYS

static int kncu_packet_intersect_callback(
    kd_tree_node_t *node,
    void const *packet
    )
{
  return knc_packet_explore_node(node, (ray_packet_t const *)packet);
}

#endif // PACKET_RAYS

/****************************************
 * Unordered kd-tree traversal
****************************************/

static kd_tree_node_data_t *kncu_rec(
    kd_tree_it_t *iterator,
    kncu_hit_test_callback_t hit_test_callback,
    void const *hit_test_data
    )
{
  kd_tree_node_data_t *data;

  // we may already have something to return
  data = NULL;
  if (iterator->data.triangles.nb_triangles != 0) {
    data = &iterator->data;
  }

  if (hit_test_callback(iterator->current, hit_test_data)) {
    // we hit, add the triangles of the current node to
    // the list to return
    if (iterator->current->data.triangles.nb_triangles > 0) {
      if (data == NULL) {
        iterator->data = iterator->current->data;
        data = &iterator->data;
      } else {
        data->triangles.nb_triangles +=
            iterator->current->data.triangles.nb_triangles;
      }
    }

    // then try go left first
    if (iterator->current->left != NULL) {
      if (iterator->current->left->data.split_axis == AXIS_COUNT &&
          iterator->current->left->data.triangles.nb_triangles == 0) {
        // skip left node if empty
        // NOTE: it is extremly unlikely the right node will
        //       be empty too.
        iterator->current = iterator->current->right;
      } else {
        iterator->current = iterator->current->left;
      }
      return kncu_rec(iterator, hit_test_callback, hit_test_data);
    }
  } else if (data != NULL) {
    // if we do not hit but have something to return,
    // we will return it before the next iteration
    // and we mark iterator->data for cleanning before
    // the start of the next one
    iterator->clean_data = 1;
  }

  // go to the parent
  while (1) {
    if (iterator->current->parent == NULL) {
      // we came back at the root
      iterator->current = NULL;
      return data;
    } else if (iterator->current->parent->right != iterator->current &&
               (iterator->current->parent->right->data.split_axis != AXIS_COUNT
                || iterator->current->parent->right->data.triangles.nb_triangles != 0)) {
      // if we are not the node on the right and the node on
      // the right is not an empty leaf, go right
      iterator->current = iterator->current->parent->right;
      if (iterator->clean_data) {
        // we have something to return that is not adjacent to
        // the next nodes the futur iterations may yield.
        return data;
      } else {
        return kncu_rec(iterator, hit_test_callback, hit_test_data);
      }
    } else {
      // if we are the node on the right, go back
      iterator->current = iterator->current->parent;
    }
  }
}

static kd_tree_node_data_t *kncu(
    kd_tree_it_t *iterator,
    kncu_hit_test_callback_t callback,
    void const *data
    )
{
  kd_tree_node_data_t *ret;

  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  // pre-processing
  if (iterator->clean_data) {
    bzero(&iterator->data, sizeof(iterator->data));
    iterator->clean_data = 0;
  }

  ret = kncu_rec(iterator, callback, data);
  return ret;
}

kd_tree_node_data_t *kdtree_next_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_t const *ray
    )
{
  return kncu(iterator, kncu_ray_intersect_callback, ray);
}

#ifdef PACKET_RAYS

kd_tree_node_data_t *kdtree_next_packet_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_packet_t const *packet
    )
{
  return kncu(iterator, kncu_packet_intersect_callback, packet);
}

#endif // PACKET_RAYS

/****************************************
 * KDtree debug functions
****************************************/

typedef struct kd_tree_wfobj_face_s {
  size_t v[4];
} kd_tree_wfobj_face_t;

typedef struct kd_tree_wfobj_context_s
{
  size_t nb_vertices_max;
  size_t nb_vertices;
  size_t nb_faces_max;
  size_t nb_faces;
  vect3_t *vertices;
  kd_tree_wfobj_face_t *faces;
} kd_tree_wfobj_context_t;

static void kdtree_dtf_init_context(
    kd_tree_wfobj_context_t *context,
    kd_tree_t const *tree
    )
{
  size_t nb_leaves;

  nb_leaves = (size_t)pow(2, tree->depth);
  context->nb_vertices_max = 8 * nb_leaves;
  context->nb_faces_max = 6 * nb_leaves;
  context->nb_vertices = 0;
  context->nb_faces = 0;
  context->vertices = xmalloc(
      sizeof(*context->vertices) * context->nb_vertices_max
      );
  context->faces = xmalloc(
      sizeof(*context->faces) * context->nb_faces_max
      );
}

static void kdtree_dtf_cleanup_context(
    kd_tree_wfobj_context_t *context
    )
{
  free(context->vertices);
  free(context->faces);
  memset(context, 0, sizeof(*context));
}

static size_t kdtree_dtf_register_vertex(
    kd_tree_wfobj_context_t *context,
    vect3_t const *v
    )
{
  size_t i;

  // try to find a corresponding, existing vertex
  for (i = 0; i < context->nb_vertices; ++i) {
    vect3_t *o = &context->vertices[i];

    if (o->x == v->x && o->y == v->y && o->z == v->z) {
      return i;
    }
  }

  // allocate a new vertex
  if (i >= context->nb_vertices_max) {
    fprintf(
        stderr,
        "Not enough vertices pre-allocated: %zu\n",
        context->nb_vertices_max
        );
    abort();
  }
  context->nb_vertices++;
  context->vertices[i] = *v;
  return i;
}

static void kdtree_dtf_register_face(
    kd_tree_wfobj_context_t *context,
    kd_tree_wfobj_face_t const *f
    )
{
  size_t i;

  i = context->nb_faces++;
  if (context->nb_faces > context->nb_faces_max) {
    context->nb_faces--;
    fprintf(
        stderr,
        "Not enough faces pre-allocated: %zu\n",
        context->nb_faces_max
        );
    abort();
  }
  context->faces[i] = *f;
}

static void kdtree_dtf_add_bbox(
    kd_tree_wfobj_context_t *context,
    bbox_aa_t const *bbox,
    axis_t split_axis
    )
{
  size_t i;
  size_t c;
  size_t v[8];
  size_t axis;
  vect3_t origin_opposit[2];

  // unused (yet)
  (void) split_axis;

  // compute and register the height different vertices
  origin_opposit[0] = bbox->origin;
  VECT3_ADD(origin_opposit[1], bbox->origin, bbox->dimensions);
  for (i = 0; i < SIZEOF_ARRAY(v); ++i) {
    int x, y, z;
    vect3_t cur;

    x = i & 0x1;
    y = (i & 0x2) >> 1;
    z = (i & 0x4) >> 2;
    cur.x = origin_opposit[x].x;
    cur.y = origin_opposit[y].y;
    cur.z = origin_opposit[z].z;
    v[i] = kdtree_dtf_register_vertex(context, &cur);
  }

  // register the six different faces
  for (axis = 0; axis < AXIS_COUNT; ++axis) {
    size_t axis_p1 = (axis + 1) % 3;
    size_t axis_p2 = (axis + 2) % 3;

    for (c = 0; c < 2; ++c) {
      size_t index;
      kd_tree_wfobj_face_t face;

      for (i = 0; i < 4; ++i) {
        index = c << axis;
        index |= (i & 0x1) << axis_p1;
        index |= ((i & 0x2) >> 1) << axis_p2;

        // vertex index start at 1 in wf .obj
        face.v[i] = v[index] + 1;
      }
      // swap 3 with 2 so that we reproduce the sequence
      // 00 01 11 10
      index = face.v[2];
      face.v[2] = face.v[3];
      face.v[3] = index;
      kdtree_dtf_register_face(context, &face);
    }
  }
}

static void kdtree_dtf_write_context(
    FILE *fd,
    kd_tree_wfobj_context_t *context
    )
{
  size_t i;

  // small header
  fputs("# kdtree\n", fd);

  // print vertices
  fprintf(fd, "# %zu vertices\n", context->nb_vertices);
  for (i = 0; i < context->nb_vertices; ++i) {
    fprintf(fd, "v %f %f %f\n", VECT3_PF_EX(context->vertices[i]));
  }

  // print faces
  fprintf(fd, "# %zu faces\n", context->nb_faces);
  for (i = 0; i < context->nb_faces; ++i) {
    fprintf(fd, "f %zu %zu %zu %zu\n",
      context->faces[i].v[0],
      context->faces[i].v[1],
      context->faces[i].v[2],
      context->faces[i].v[3]
      );
  }
}

void kdtree_dump_to_file(kd_tree_t const *tree, char const *fname)
{
  kd_tree_wfobj_context_t context;
  kd_tree_node_data_t *node_data;
  kd_tree_it_t iterator;
  FILE *fd;

  // allocate context
  kdtree_dtf_init_context(&context, tree);

  // init context
  kdtree_iterate(&iterator, tree);
  while (NULL != (node_data = kdtree_next_leaf_data(&iterator))) {
    kdtree_dtf_add_bbox(&context, &node_data->bbox, node_data->split_axis);
  }

  // open file
  fd = fopen(fname, "w");
  if (fd == NULL) {
    kdtree_dtf_cleanup_context(&context);
    perror("kdtree_dump_to_file: Unable to open file");
    return;
  }

  // write to file
  kdtree_dtf_write_context(fd, &context);

  // close file and cleanup
  kdtree_dtf_cleanup_context(&context);
  fclose(fd);
  fd = NULL;
}

#endif /* USE_KDTREE_V2 */
