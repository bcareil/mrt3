#ifndef __OTREE_H__
# define __OTREE_H__

#include <stdint.h>

# include "bbox.h"
# include "vect/vect.h"

typedef struct o_tree_node_s {
	bbox_aa_p_t bounds;
	struct o_tree_node_s *children[8];
	void *data;
} o_tree_node_t;

typedef struct o_tree_s {
	o_tree_node_t *nodes;
	uint8_t *data;
#ifdef MULTI_THREAD
	o_tree_node_t *last_returned[16];
	int last_returned_counter;
// for profiling
//	int cache_hit;
//	int cache_miss;
#else
	o_tree_node_t *last_returned;
#endif
	size_t depth;
	size_t data_size;
	size_t nb_nodes;
	size_t total_data_size;
} o_tree_t;

/**
 * Initialize a quad tree
 *
 * @param data_size The size in bytes of the chunck of data allocated
 *                  for each leaves retrived by
 *                  qtree_get_node(tree, x, y, z)->data
 */
void otree_init(
    o_tree_t *tree,
    bbox_aa_t const *bounds,
    size_t depth,
    size_t data_size
    );

/**
 * Free memory allocated by quad_tree
 */
void clean_otree(o_tree_t *tree);

/**
 * Retrieve the node containing the given point
 */
void *otree_get_node_data(o_tree_t *tree, float x, float y, float z);

#endif /* __OTREE_H__ */
