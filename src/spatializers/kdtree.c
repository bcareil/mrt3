#ifndef USE_KDTREE_V2

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <strings.h>

#include "kdtree.h"

// Number of triangles at which point a node is not split.
// As to be tweaked based on the ray/aabb / ray/triangle cost ratio.
#define MIN_TRIANGLES_PER_LEAF 48

typedef struct triangle_precmp_s {
  triangle_ref_t triangle_ref;
  vect3_t min;
  vect3_t max;
  vect3_t center;
} triangle_precmp_t;

typedef int (*comparator_t)(void const *, void const *);

static int median_comparator(
    triangle_precmp_t const *a,
    triangle_precmp_t const *b,
    axis_t axis
    )
{
  return VECT_COMP(a->center, axis) - VECT_COMP(b->center, axis);
}

static int median_x_comparator(void const *a, void const *b)
{
  return median_comparator(
      (triangle_precmp_t const *)a,
      (triangle_precmp_t const *)b,
      AXIS_X
      );
}

static int median_y_comparator(void const *a, void const *b)
{
  return median_comparator(
      (triangle_precmp_t const *)a,
      (triangle_precmp_t const *)b,
      AXIS_Y
      );
}

static int median_z_comparator(void const *a, void const *b)
{
  return median_comparator(
      (triangle_precmp_t const *)a,
      (triangle_precmp_t const *)b,
      AXIS_Z
      );
}

static float reduce_max(
    triangle_precmp_t const *triangles,
    size_t nb_triangles,
    axis_t axis
    )
{
  size_t i;
  float max;

  assert(nb_triangles > 0);
  max = VECT_COMP(triangles->max, axis);
  for (i = 1; i < nb_triangles; ++i) {
    max = MAX(max, VECT_COMP(triangles[i].max, axis));
  }
  return max;
}

static float reduce_min(
    triangle_precmp_t const *triangles,
    size_t nb_triangles,
    axis_t axis
    )
{
  size_t i;
  float min;

  assert(nb_triangles > 0);
  min = VECT_COMP(triangles->min, axis);
  for (i = 1; i < nb_triangles; ++i) {
    min = MIN(min, VECT_COMP(triangles[i].min, axis));
  }
  return min;
}

static void calculate_bbox(
    bbox_aa_t *bbox,
    triangle_precmp_t *triangles,
    size_t nb_triangles
    )
{
  size_t i;
  vect3_t min = VECT3_POS_INF;
  vect3_t max = VECT3_MIN_INF;

  for (i = 0; i < nb_triangles; ++i) {
    triangle_precmp_t *t;

    t = &triangles[i];
    min.x = MIN(min.x, t->min.x);
    min.y = MIN(min.y, t->min.y);
    min.z = MIN(min.z, t->min.z);
    max.x = MAX(max.x, t->max.x);
    max.y = MAX(max.y, t->max.y);
    max.z = MAX(max.z, t->max.z);
  }
  bbox->origin = min;
  VECT3_SUB(bbox->dimensions, max, min);
}

static kd_tree_node_t *fill_rec(
    kd_tree_t *tree,
    bbox_aa_t const *bbox,
    triangle_precmp_t *triangles,
    size_t nb_triangles,
    size_t remaining_depth,
    size_t attempt,
    axis_t axis
    )
{
  comparator_t const median_comparators[] = {
    median_x_comparator,
    median_y_comparator,
    median_z_comparator
  };
  kd_tree_node_t *node;

  // debug
  //puts("=================================");
  //printf("fill_rec at depth %i\n", -(int)remaining_depth);
  //printf("received %zu triangles\n", nb_triangles);

  // fetch a pre-allocated node from the node pool
  assert(tree->node_pool_usage < tree->node_pool_size);
  node = &tree->node_pool[tree->node_pool_usage];
  tree->node_pool_usage += 1;

  // determine axis
  //axis = (axis_t)((remaining_depth + attempt) % AXIS_COUNT);
  //node->data.split_axis = axis;

  // compute bbox
  node->data.bbox = *bbox;

  // default values triangles
  node->data.nb_triangles = 0;
  node->data.triangles = NULL;

  if (remaining_depth == 0 ||
      nb_triangles < MIN_TRIANGLES_PER_LEAF ||
      attempt == AXIS_COUNT) {
    // this is a leaf node or forced to be a leaf if it cannot be split
    size_t i;

    if (attempt == AXIS_COUNT) {
      printf(
          "INFO: kdtree: too many attemps, creating leaf with %zu nodes\n",
          nb_triangles
          );
    }
    node->left = NULL;
    node->right = NULL;
    node->data.split_axis = AXIS_COUNT;
    node->data.nb_triangles = nb_triangles;
    if (nb_triangles == 0) {
      puts("INFO: kdtree: leaf has no nodes");
      node->data.triangles = 0;
    } else {
      node->data.triangles = xmalloc(sizeof(*node->data.triangles) * nb_triangles);
      for (i = 0; i < nb_triangles; ++i) {
        node->data.triangles[i] = triangles[i].triangle_ref;
      }
    }
  } else {
    float median;
    float median_percentage;
    size_t i;
    size_t nb_sub_triangles;
    bbox_aa_t sub_bbox;
    triangle_precmp_t *sub_triangles;

    // calculate median
    qsort(triangles, nb_triangles, sizeof(*triangles), median_comparators[axis]);
    median = VECT_COMP(triangles[nb_triangles / 2].center, axis);

    // ensure median is in bbox
    if (median < (VECT_COMP(bbox->origin, axis) + EPSILON) ||
        (VECT_COMP(bbox->origin, axis) +
          VECT_COMP(bbox->dimensions, axis) -
          EPSILON) < median) {
      // reclaim current node
      tree->node_pool_usage -= 1;
      // try on another axis
      return fill_rec(
          tree,
          bbox,
          triangles,
          nb_triangles,
          remaining_depth,
          attempt + 1,
          (axis + 1) % AXIS_COUNT
          );
    }

    // evaluate median percentage
    median_percentage =
      (median - VECT_COMP(bbox->origin, axis)) /
      VECT_COMP(bbox->dimensions, axis)
      ;
    // if median is close to the ends of the box and the selected axis
    // see if not all triangles are not on this end
    if (median_percentage < 0.333f) {
      float max = reduce_max(triangles, nb_triangles, axis);
      float max_pct =
        (max - VECT_COMP(bbox->origin, axis)) /
        VECT_COMP(bbox->dimensions, axis)
        ;

      if (max_pct < 0.5f) {
        // the vertex the farthest from the begining of the bbox is
        // in the first half of it, but all triangles in one bbox
        median = max + EPSILON;
      }
    } else if (median_percentage > 0.667f) {
      // same than above, but for the other side
      float min = reduce_min(triangles, nb_triangles, axis);
      float min_pct =
        (min - VECT_COMP(bbox->origin, axis)) /
        VECT_COMP(bbox->dimensions, axis)
        ;

      if (min_pct > 0.5f) {
        median = min - EPSILON;
      }
    }

    // allocate sub_triangles
    sub_triangles = xmalloc(sizeof(*sub_triangles) * nb_triangles);

    // the left node is lesser than median
    nb_sub_triangles = 0;
    for (i = 0; i < nb_triangles; ++i) {
      triangle_precmp_t *triangle;
      float min;

      triangle = &triangles[i];
      min = MIN(VECT_COMP(triangle->min, axis), VECT_COMP(triangle->max, axis));
      if (min <= median) {
        sub_triangles[nb_sub_triangles] = *triangle;
        nb_sub_triangles += 1;
      }
    }

    // compute left bbox
    sub_bbox = *bbox;
    VECT_COMP(sub_bbox.dimensions, axis) = median - VECT_COMP(sub_bbox.origin, axis);

    // fill rec left node
    node->left = fill_rec(
        tree,
        &sub_bbox,
        sub_triangles,
        nb_sub_triangles,
        remaining_depth - 1,
        0,
        (axis + 1) % AXIS_COUNT
        );
    node->left->parent = node;

    // right is greater than median
    nb_sub_triangles = 0;
    for (i = 0; i < nb_triangles; ++i) {
      triangle_precmp_t *triangle;
      float max;

      triangle = &triangles[i];
      max = MAX(VECT_COMP(triangle->min, axis), VECT_COMP(triangle->max, axis));
      if (max >= median) {
        sub_triangles[nb_sub_triangles] = *triangle;
        nb_sub_triangles += 1;
      }
    }

    // compute right bbox
    VECT_COMP(sub_bbox.origin, axis) = median;
    VECT_COMP(sub_bbox.dimensions, axis) =
      (VECT_COMP(bbox->origin, axis) + VECT_COMP(bbox->dimensions, axis)) - median;

    // fill rec right node
    node->right = fill_rec(
        tree,
        &sub_bbox,
        sub_triangles,
        nb_sub_triangles,
        remaining_depth - 1,
        0,
        (axis + 1) % AXIS_COUNT
        );
    node->right->parent = node;

    // cleanup
    free(sub_triangles);
  }

  // return the used node
  return node;
}

static void calculate_triangles(
    triangle_precmp_t *precmp_triangles,
    triangle_t const *triangles,
    size_t nb_triangles
    )
{
  size_t i;

  for (i = 0; i < nb_triangles; ++i) {
    triangle_t const *triangle;
    triangle_precmp_t *precmp_triangle;

    triangle = &triangles[i];
    precmp_triangle = &precmp_triangles[i];

    precmp_triangle->triangle_ref.index = i;
    precmp_triangle->triangle_ref.triangle = *triangle;
    precmp_triangle->min.x = MIN(triangle->v0.x, MIN(triangle->v1.x, triangle->v2.x));
    precmp_triangle->max.x = MAX(triangle->v0.x, MAX(triangle->v1.x, triangle->v2.x));
    precmp_triangle->min.y = MIN(triangle->v0.y, MIN(triangle->v1.y, triangle->v2.y));
    precmp_triangle->max.y = MAX(triangle->v0.y, MAX(triangle->v1.y, triangle->v2.y));
    precmp_triangle->min.z = MIN(triangle->v0.z, MIN(triangle->v1.z, triangle->v2.z));
    precmp_triangle->max.z = MAX(triangle->v0.z, MAX(triangle->v1.z, triangle->v2.z));
    VECT3_ADD(precmp_triangle->center, triangle->v0, triangle->v1);
    VECT3_ADD(precmp_triangle->center, precmp_triangle->center, triangle->v2);
    VECT3_DIV_S(precmp_triangle->center, precmp_triangle->center, 3.f);
  }
}

void kdtree_init(
    kd_tree_t *tree,
    triangle_t const *triangles,
    size_t nb_triangles
    )
{
  size_t max_depth;
  bbox_aa_t root_bbox;
  triangle_precmp_t *precmp_triangles;

  // pre-compute some value for triangle's process
  precmp_triangles = xmalloc(sizeof(*precmp_triangles) * nb_triangles);
  calculate_triangles(precmp_triangles, triangles, nb_triangles);

  // compute root node's bbox
  calculate_bbox(&root_bbox, precmp_triangles, nb_triangles);

  // pre-compute some values
  max_depth = MAX(1.f, 1.4f * logf(MAX(1, nb_triangles)));

  // allocate node pool
  tree->depth = max_depth;
  tree->node_pool_size = pow(2, max_depth);
  tree->node_pool_usage = 0;
  tree->node_pool = xmalloc(sizeof(*tree->node_pool) * tree->node_pool_size);

  // fill tree recursively
  tree->root = fill_rec(
      tree,
      &root_bbox,
      precmp_triangles,
      nb_triangles,
      max_depth - 1,
      0,
      AXIS_X
      );
  tree->root->parent = NULL;

  free(precmp_triangles);
}

void clean_kdtree(
    kd_tree_t *tree
    )
{
  kd_tree_it_t it;
  kd_tree_node_data_t *node_data;

  kdtree_iterate(&it, tree);
  while ((node_data = kdtree_next_leaf(&it)) != NULL) {
    free(node_data->triangles);
  }
  free(tree->node_pool);
  bzero(tree, sizeof(*tree));
}

/****************************************
 * Kd-tree standard iteration functions
****************************************/

void kdtree_iterate(
    kd_tree_it_t *iterator,
    kd_tree_t const *tree
    )
{
  assert(tree->depth < SIZEOF_ARRAY(iterator->states));

  bzero(iterator, sizeof(*iterator));
  iterator->current = tree->root;
#if USE_KDTREE_ORDERED_COLLISION_CHECK
  iterator->states[0].max_dist = INFINITY;
#endif
}

kd_tree_node_data_t *kdtree_next_node(
    kd_tree_it_t *iterator
    )
{
  kd_tree_node_data_t *data;

  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  // pointer to return
  data = &iterator->current->data;

  // try go left first
  if (iterator->current->left != NULL) {
    iterator->current = iterator->current->left;
    return data;
  }

  // we are at a leaf, go to the parent
  while (1) {
    if (iterator->current->parent == NULL) {
      // we came back at the root
      iterator->current = NULL;
      break;
    } else if (iterator->current->parent->right != iterator->current) {
      // if we are not the node on the right, go right
      iterator->current = iterator->current->parent->right;
      break;
    } else {
      // if we are the node on the right, go back
      iterator->current = iterator->current->parent;
    }
  }

  // return saved pointer
  return data;
}

kd_tree_node_data_t *kdtree_next_leaf(
    kd_tree_it_t *iterator
    )
{
  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  // if we cant go to the left
  if (iterator->current->left == NULL) {
    if (iterator->current->right == NULL) {
      // if we cant go to the right right-away, get to the parent
      kd_tree_node_t *old;

      do {
        old = iterator->current;
        iterator->current = old->parent;
        // if we got back to the root, return
        if (iterator->current == NULL) {
          return NULL;
        }
        // otherwise, continue to get back as long as we come from the right
      } while (iterator->current->right == old
          || iterator->current->right == NULL);
    }
    // if we can go to the right, go to the right once
    iterator->current = iterator->current->right;
  }

  // go to the leaf node the most to the left
  while (iterator->current->left != NULL) {
    iterator->current = iterator->current->left;
  }

  return &iterator->current->data;
}

/****************************************
 * Kd-tree advanced iteration tools
****************************************/

enum kdtree_next_colliding_state_e {
  KNCS_UNEXPLORED   = 0x00,
  KNCS_WENT_LEFT    = 0x01,
  KNCS_WENT_RIGHT   = 0x02,
  KNCS_EXPLORED     = 0x03,
  KNCS_UNORDERED    = 0x04,
};

enum kdtree_next_colliding_operation_e {
  KNCO_GO_LEFT      = 1,
  KNCO_GO_RIGHT     = 2,
  KNCO_GO_PARENT    = 3,
};

enum kdtree_hit_e {
  KNCH_NO_HIT     = 0x00,
  KNCH_HIT_LEFT   = 0x01,
  KNCH_HIT_RIGHT  = 0x02,
};

#define WENT_ONLY_RIGHT(s)  ((s & KNCS_EXPLORED) == KNCS_WENT_RIGHT)
#define WENT_ONLY_LEFT(s)   ((s & KNCS_EXPLORED) == KNCS_WENT_LEFT)
#define IS_EXPLORED(s)      ((s & KNCS_EXPLORED) == KNCS_EXPLORED)
#define IS_UNEXPLORED(s)    ((s & KNCS_EXPLORED) == KNCS_UNEXPLORED)
#define IS_UNORDERED(s)     ((s & KNCS_UNORDERED))
#define IS_ORDERED(s)       ((s & KNCS_UNORDERED) == 0)

static int knc_explore_node(
    vect3_t *left,
    vect3_t *right,
    kd_tree_node_t const *node,
    ray_t const *ray
    )
{
  int hit;

  hit = KNCH_NO_HIT;

  if (bbox_aa_ray_intersects(
      left,
      &node->left->data.bbox,
      ray
      )) {
    hit |= KNCH_HIT_LEFT;
  }

  if (bbox_aa_ray_intersects(
      right,
      &node->right->data.bbox,
      ray
      )) {
    hit |= KNCH_HIT_RIGHT;
  }

  return hit;
}

static int knc_packet_explore_node(
    kd_tree_node_t const *node,
    ray_packet_t const *packet
    )
{
  int hit;

  hit = KNCH_NO_HIT;

  if (bbox_aa_ray_packet_intersects(
        &node->left->data.bbox,
        packet
        )) {
    hit |= KNCH_HIT_LEFT;
  }

  if (bbox_aa_ray_packet_intersects(
        &node->right->data.bbox,
        packet
        )) {
    hit |= KNCH_HIT_RIGHT;
  }

  return hit;
}

/****************************************
 * Ordered kd-tree traversal
****************************************/

static kd_tree_node_data_t *knco_rec(
    int *is_ordered,
    float *max_dist,
    kd_tree_it_t *iterator,
    ray_t const *ray
    )
{
  size_t current_state;
  unsigned int state;

  // fetch state
  current_state = iterator->current_state;
  state = iterator->states[current_state].state;
  *max_dist = iterator->states[current_state].max_dist;
  //printf("state[%2zu]={state=%02x,max_dist=%.3f}\n", current_state, state, *max_dist);

  // if leaf node, return
  if (iterator->current->left == NULL) {
    kd_tree_node_data_t *data;

    //printf("return is_ordered=%i, max_dist=%.3f\n", IS_ORDERED(state), *max_dist);
    //puts("*********");
    *is_ordered = IS_ORDERED(state);
    data = &iterator->current->data;
    iterator->current = iterator->current->parent;
    iterator->current_state -= 1;
    return data;
  }

  // chose course of action
  if (WENT_ONLY_RIGHT(state)) {
    // go left
    iterator->current = iterator->current->left;
    current_state += 1;
    state |= KNCS_EXPLORED;
  } else if (WENT_ONLY_LEFT(state)) {
    // go right
    iterator->current = iterator->current->right;
    current_state += 1;
    state |= KNCS_EXPLORED;
  } else if (IS_EXPLORED(state)) {
    // go to parent
    iterator->current = iterator->current->parent;
    current_state -= 1;
  } else if (IS_UNEXPLORED(state)) {
    // explore node
    int hit;
    vect3_t point_left;
    vect3_t point_right;

    hit = knc_explore_node(&point_left, &point_right, iterator->current, ray);


    // if no hits, say the node is explored
    if (hit & KNCH_HIT_LEFT) {
      state |= KNCS_WENT_LEFT;
    }
    if (hit & KNCH_HIT_RIGHT) {
      state |= KNCS_WENT_RIGHT;
    }

    // choose course of action
    if (WENT_ONLY_LEFT(state)) {
      iterator->current = iterator->current->right;
      current_state += 1;
      state |= KNCS_EXPLORED;
    } else if (WENT_ONLY_RIGHT(state)) {
      iterator->current = iterator->current->left;
      current_state += 1;
      state |= KNCS_EXPLORED;
    } else if (IS_EXPLORED(state)) {
      iterator->current = iterator->current->parent;
      current_state -= 1;
    } else {
      // if both box were hit, we have to choose the closest one first
      float dist_left;
      float dist_right;
      float dir;
      vect3_t to_left;
      vect3_t to_right;

      VECT3_SUB(to_left, ray->org, point_left);
      VECT3_SUB(to_right, ray->org, point_right);
      dist_left = VECT3_LENGTH_SQ(to_left);
      dist_right = VECT3_LENGTH_SQ(to_right);
      dir = VECT_COMP(ray->dir, iterator->current->data.split_axis);

      //printf("actual_dist_left=%.3f;actual_dist_right=%.3f\n",
      //       sqrtf(dist_left), sqrtf(dist_right));

      //RAY_PRINTF("ray", (*ray));
      //VECT3_PRINTF("pl", point_left);
      //VECT3_PRINTF("pr", point_right);
      //printf("dist_left=%.3f;dist_right=%.3f;org=%.3f;dir=%.3f\n",
      //    dist_left, dist_right,
      //    VECT_COMP(ray->org, iterator->current->data.split_axis),
      //    dir);
      if (dist_left == dist_right) {
        // get into unordered mode
        //puts("switched to unordered");
        *max_dist = MIN(*max_dist, sqrtf(dist_left));
        state |= KNCS_UNORDERED;
      }
      if (dist_left < dist_right && dir > 0.f) {
        // left is closer
        //puts("go left");
        *max_dist = MIN(*max_dist, sqrtf(dist_right));
        iterator->current = iterator->current->left;
        current_state += 1;
        state |= KNCS_WENT_LEFT;
      } else {
        // right is closer
        //puts("go right");
        *max_dist = MIN(*max_dist, sqrtf(dist_left));
        iterator->current = iterator->current->right;
        current_state += 1;
        state |= KNCS_WENT_RIGHT;
      }
    }
  }

  // if we got back to root
  if (iterator->current == NULL) {
    iterator->current_state = 0;
    //puts("return null");
    //puts("*********");
    return NULL;
  }

  // init child node state
  if (current_state > iterator->current_state) {
    // set state to unexplored and the UNORDERED flag
    // according to its parent value
    iterator->states[current_state].state =
      KNCS_UNEXPLORED |
      (KNCS_UNORDERED & state)
      ;
    iterator->states[current_state].max_dist = *max_dist;
  }

  // update iterator and loop
  iterator->states[iterator->current_state].state = state;
  iterator->states[iterator->current_state].max_dist = *max_dist;
  iterator->current_state = current_state;
  return knco_rec(is_ordered, max_dist, iterator, ray);
}

kd_tree_node_data_t *kdtree_next_colliding_ordered(
    int *is_ordered,
    float *max_dist,
    kd_tree_it_t *iterator,
    ray_t const *ray
    )
{
  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  //puts("-- kdtree_next_colliding --");
  *is_ordered = 1;
  *max_dist = INFINITY;
  return knco_rec(is_ordered, max_dist, iterator, ray);
}

/****************************************
 * Unordered hit test callbacks
****************************************/

typedef int (*kncu_hit_test_callback_t)(kd_tree_node_t *, void const *);

static int kncu_ray_intersect_callback(
    kd_tree_node_t *node,
    void const *ray
    )
{
  vect3_t tmp_a;
  vect3_t tmp_b;

  return knc_explore_node(&tmp_a, &tmp_b, node, (ray_t const *)ray);
}

static int kncu_packet_intersect_callback(
    kd_tree_node_t *node,
    void const *packet
    )
{
  return knc_packet_explore_node(node, (ray_packet_t const *)packet);
}

/****************************************
 * Unordered kd-tree traversal
****************************************/

static kd_tree_node_data_t *kncu_rec(
    kd_tree_it_t *iterator,
    kncu_hit_test_callback_t hit_test_callback,
    void const *data
    )
{
  enum kdtree_next_colliding_operation_e operation;
  size_t current_state;
  unsigned int state;

  // if leaf
  if (iterator->current->left == NULL) {
    kd_tree_node_data_t *data;

    data = &iterator->current->data;
    iterator->current = iterator->current->parent;
    iterator->current_state -= 1;
    return data;
  }

  // fetch state
  current_state = iterator->current_state;
  state = iterator->states[current_state].state;

  // chose course of action.
  // NOTE: here, we always start by exploring the left node so
  //       KNCS_WENT_RIGHT will never be set before KNCS_WENT_LEFT.
  //       Therfore, there is no need to check WENT_ONLY_RIGHT.
  if (WENT_ONLY_LEFT(state)) {
    operation = KNCO_GO_RIGHT;
  } else if (IS_EXPLORED(state)) {
    operation = KNCO_GO_PARENT;
  } else {
    // explore node
    int hit;

    hit = hit_test_callback(iterator->current, data);
    if (hit & KNCH_HIT_LEFT) {
      operation = KNCO_GO_LEFT;
      if ((hit & KNCH_HIT_RIGHT) == 0) {
        state = KNCS_WENT_RIGHT;
      }
    } else if (hit & KNCH_HIT_RIGHT) {
      operation = KNCO_GO_RIGHT;
      state = KNCS_WENT_LEFT;
    } else {
      operation = KNCO_GO_PARENT;
    }
  }

  if (operation == KNCO_GO_PARENT) {
    // go up
    iterator->current = iterator->current->parent;
    iterator->current_state -= 1;

    // went back to root
    if (iterator->current == NULL) {
      return NULL;
    }
  } else {
    // go down
    if (operation == KNCO_GO_RIGHT) {
      iterator->current = iterator->current->right;
      state |= KNCS_WENT_RIGHT;
    } else {
      iterator->current = iterator->current->left;
      state |= KNCS_WENT_LEFT;
    }

    // update current state
    iterator->states[iterator->current_state].state = state;

    // init next state
    iterator->current_state += 1;
    iterator->states[iterator->current_state].state = KNCS_UNEXPLORED;
  }

  return kncu_rec(iterator, hit_test_callback, data);
}

static kd_tree_node_data_t *kncu(
    kd_tree_it_t *iterator,
    kncu_hit_test_callback_t callback,
    void const *data
    )
{
  // sanity check
  if (iterator->current == NULL) {
    return NULL;
  }

  return kncu_rec(iterator, callback, data);
}

kd_tree_node_data_t *kdtree_next_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_t const *ray
    )
{
  return kncu(iterator, kncu_ray_intersect_callback, ray);
}

kd_tree_node_data_t *kdtree_next_packet_colliding_unordered(
    kd_tree_it_t *iterator,
    ray_packet_t const *packet
    )
{
  return kncu(iterator, kncu_packet_intersect_callback, packet);
}

/****************************************
 * KDtree debug functions
****************************************/

typedef struct kd_tree_wfobj_face_s {
  size_t v[4];
} kd_tree_wfobj_face_t;

typedef struct kd_tree_wfobj_context_s
{
  size_t nb_vertices_max;
  size_t nb_vertices;
  size_t nb_faces_max;
  size_t nb_faces;
  vect3_t *vertices;
  kd_tree_wfobj_face_t *faces;
} kd_tree_wfobj_context_t;

static void kdtree_dtf_init_context(
    kd_tree_wfobj_context_t *context,
    kd_tree_t const *tree
    )
{
  size_t nb_leaves;

  nb_leaves = (size_t)pow(2, tree->depth);
  context->nb_vertices_max = 8 * nb_leaves;
  context->nb_faces_max = 6 * nb_leaves;
  context->nb_vertices = 0;
  context->nb_faces = 0;
  context->vertices = xmalloc(
      sizeof(*context->vertices) * context->nb_vertices_max
      );
  context->faces = xmalloc(
      sizeof(*context->faces) * context->nb_faces_max
      );
}

static void kdtree_dtf_cleanup_context(
    kd_tree_wfobj_context_t *context
    )
{
  free(context->vertices);
  free(context->faces);
  memset(context, 0, sizeof(*context));
}

static size_t kdtree_dtf_register_vertex(
    kd_tree_wfobj_context_t *context,
    vect3_t const *v
    )
{
  size_t i;

  // try to find a corresponding, existing vertex
  for (i = 0; i < context->nb_vertices; ++i) {
    vect3_t *o = &context->vertices[i];

    if (o->x == v->x && o->y == v->y && o->z == v->z) {
      return i;
    }
  }

  // allocate a new vertex
  if (i >= context->nb_vertices_max) {
    fprintf(
        stderr,
        "Not enough vertices pre-allocated: %zu\n",
        context->nb_vertices_max
        );
    abort();
  }
  context->nb_vertices++;
  context->vertices[i] = *v;
  return i;
}

static void kdtree_dtf_register_face(
    kd_tree_wfobj_context_t *context,
    kd_tree_wfobj_face_t const *f
    )
{
  size_t i;

  i = context->nb_faces++;
  if (context->nb_faces > context->nb_faces_max) {
    context->nb_faces--;
    fprintf(
        stderr,
        "Not enough faces pre-allocated: %zu\n",
        context->nb_faces_max
        );
    abort();
  }
  context->faces[i] = *f;
}

static void kdtree_dtf_add_bbox(
    kd_tree_wfobj_context_t *context,
    bbox_aa_t const *bbox,
    axis_t split_axis
    )
{
  size_t i;
  size_t c;
  size_t v[8];
  size_t axis;
  vect3_t origin_opposit[2];

  // unused (yet)
  (void) split_axis;

  // compute and register the height different vertices
  origin_opposit[0] = bbox->origin;
  VECT3_ADD(origin_opposit[1], bbox->origin, bbox->dimensions);
  for (i = 0; i < SIZEOF_ARRAY(v); ++i) {
    int x, y, z;
    vect3_t cur;

    x = i & 0x1;
    y = (i & 0x2) >> 1;
    z = (i & 0x4) >> 2;
    cur.x = origin_opposit[x].x;
    cur.y = origin_opposit[y].y;
    cur.z = origin_opposit[z].z;
    v[i] = kdtree_dtf_register_vertex(context, &cur);
  }

  // register the six different faces
  for (axis = 0; axis < AXIS_COUNT; ++axis) {
    size_t axis_p1 = (axis + 1) % 3;
    size_t axis_p2 = (axis + 2) % 3;

    for (c = 0; c < 2; ++c) {
      size_t index;
      kd_tree_wfobj_face_t face;

      for (i = 0; i < 4; ++i) {
        index = c << axis;
        index |= (i & 0x1) << axis_p1;
        index |= ((i & 0x2) >> 1) << axis_p2;

        // vertex index start at 1 in wf .obj
        face.v[i] = v[index] + 1;
      }
      // swap 3 with 2 so that we reproduce the sequence
      // 00 01 11 10
      index = face.v[2];
      face.v[2] = face.v[3];
      face.v[3] = index;
      kdtree_dtf_register_face(context, &face);
    }
  }
}

static void kdtree_dtf_write_context(
    FILE *fd,
    kd_tree_wfobj_context_t *context
    )
{
  size_t i;

  // small header
  fputs("# kdtree\n", fd);

  // print vertices
  fprintf(fd, "# %zu vertices\n", context->nb_vertices);
  for (i = 0; i < context->nb_vertices; ++i) {
    fprintf(fd, "v %f %f %f\n", VECT3_PF_EX(context->vertices[i]));
  }

  // print faces
  fprintf(fd, "# %zu faces\n", context->nb_faces);
  for (i = 0; i < context->nb_faces; ++i) {
    fprintf(fd, "f %zu %zu %zu %zu\n",
      context->faces[i].v[0],
      context->faces[i].v[1],
      context->faces[i].v[2],
      context->faces[i].v[3]
      );
  }
}

void kdtree_dump_to_file(kd_tree_t const *tree, char const *fname)
{
  kd_tree_wfobj_context_t context;
  kd_tree_node_data_t *node_data;
  kd_tree_it_t iterator;
  FILE *fd;

  // allocate context
  kdtree_dtf_init_context(&context, tree);

  // init context
  kdtree_iterate(&iterator, tree);
  while (NULL != (node_data = kdtree_next_leaf(&iterator))) {
    kdtree_dtf_add_bbox(&context, &node_data->bbox, node_data->split_axis);
  }

  // open file
  fd = fopen(fname, "w");
  if (fd == NULL) {
    kdtree_dtf_cleanup_context(&context);
    perror("kdtree_dump_to_file: Unable to open file");
    return;
  }

  // write to file
  kdtree_dtf_write_context(fd, &context);

  // close file and cleanup
  kdtree_dtf_cleanup_context(&context);
  fclose(fd);
  fd = NULL;
}

#endif /* NOT USE_KDTREE_V2 */
