#ifndef __COLOR_H__
# define __COLOR_H__

#include <stdint.h>

#include "utils.h"

typedef struct STRUCT_ALLIGNED color_s
{
  float		r;
  float		g;
  float		b;
  float		a;
}		color_t;

# define COLOR_PF_STR "(r=%.3f,g=%.3f,b=%.3f,a=%.3f)"

# define COLOR_PF_EX(c) \
	(c).r, (c).g, (c).b, (c).a

# define COLOR_PRINTF(name, c) \
	printf(name "=" COLOR_PF_STR "\n", COLOR_PF_EX(c))

# define COLOR_BLACK {0.f, 0.f, 0.f, 1.f}

# define COLOR_IS_BLACK(c) \
	(c.r == 0.f && c.g == 0.f && c.b == 0.f)

# define COLOR_INIT_BLACK(c) {	\
	c.r = 0.f;		\
	c.g = 0.f;		\
	c.b = 0.f;		\
	c.a = 1.f;		\
} while (0);

# define COLOR_INIT(c, r_, g_, b_) {	\
	c.r = r_;			\
	c.g = g_;			\
	c.b = b_;			\
	c.a = 1.f;			\
} while (0);

# define COLOR_MUL_S(c, s) {	\
	c.r *= s;		\
	c.g *= s;		\
	c.b *= s;		\
	c.a *= 1.f;		\
} while (0);

# define COLOR_MUL(r_, a_, b_) {	\
	r_.r = a_.r * b_.r;		\
	r_.g = a_.g * b_.g;		\
	r_.b = a_.b * b_.b;		\
	r_.a = a_.a * b_.a;		\
} while (0);

# define COLOR_ADD(a_, b_) {	\
	a_.r += b_.r;		\
	a_.g += b_.g;		\
	a_.b += b_.b;		\
	a_.a += b_.a;		\
} while (0);

# define COLOR_FLOAT_CLAMP(f) ((f) > 1.0 ? 1.0 : ((f) < 0.0 ? 0.0 : (f)))
# define COLOR_FLOAT_TO_UINT(f) ((uint32_t)(COLOR_FLOAT_CLAMP(f) * 255.0))

# define COLOR_TO_UINT32(c) (			\
	COLOR_FLOAT_TO_UINT(c.r) << 16 |	\
	COLOR_FLOAT_TO_UINT(c.g) << 24 |	\
	COLOR_FLOAT_TO_UINT(c.b) <<  8 |	\
	COLOR_FLOAT_TO_UINT(c.a) <<  0		\
	)

# define COLOR_MAX_COMPONENT(c)	\
	((c.r > c.g && c.r > c.b) ? c.r : ((c.g > c.b) ? c.g : c.b))

#endif
