#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include "../mrt3.h"

#include "camera.h"

#define MAX_WIDTH   65535
#define MAX_HEIGHT  65535
#define NEAR        0.1
#define FOV         0.873

void init_camera(
    camera_t *camera,
    size_t width, size_t height,
    float org_x, float org_y, float org_z,
    float dir_x, float dir_y, float dir_z,
    float vert_x, float vert_y, float vert_z
    )
{
  size_t ssaa_square;

  // sanity check
  if (width % 2 == 1 || height % 2 == 1) {
    fprintf(stderr, "init_camera: both width and height have to be even numbers\n");
    abort();
  }
  if (width > MAX_WIDTH || height > MAX_HEIGHT) {
    fprintf(stderr, "init_camera: resolution %zux%zu too high\n", width, height);
    abort();
  }

  // init components
  ssaa_square = POW2(SSAA_FACTOR);
  VECT3_INIT(camera->org, org_x, org_y, org_z);
  VECT3_INIT(camera->dir, dir_x, dir_y, dir_z);
  VECT3_INIT(camera->vert, vert_x, vert_y, vert_z);
  camera->width = width;
  camera->height = height;
  camera->nb_packets = 0;
  camera->vectors = xmalloc(
      sizeof(*camera->vectors) * width * height * ssaa_square
      );
  camera->packets = NULL;

  // normalize inputs
  VECT3_NORM(camera->dir);
  VECT3_NORM(camera->vert);

  // allocate and setup rays
  {
    size_t nb_pixels;
    size_t x;
    size_t y;
    size_t s;
    vect3_t u;
    vect3_t v;
    vect3_t o;
    float ul;
    float vl;

    nb_pixels = width * height;
    VECT3_MUL_S(o, camera->dir, NEAR);
    VECT3_CROSS(u, camera->dir, camera->vert);
    VECT3_NORM(u);
    VECT3_CROSS(v, camera->dir, u);
    VECT3_NORM(v);
    ul = 2.f * tan(FOV / 2.f) * NEAR;
    vl = ul * (float)height / (float)width;
    VECT3_MUL_S(u, u, ul);
    VECT3_MUL_S(v, v, vl);
    for (y = 0; y < height; ++y) {
      for (x = 0; x < width; ++x) {
	for (s = 0; s < ssaa_square; ++s) {
	  vect3_t a;
	  vect3_t b;
	  float fx;
	  float fy;
          float ssaa_x;
          float ssaa_y;

	  ssaa_x = s % SSAA_FACTOR;
	  ssaa_y = s / SSAA_FACTOR;
	  fx = ((float)x) - (width / 2);
	  fy = ((float)y) - (height / 2);
	  fx /= width;
	  fy /= height;
	  fx += ssaa_x / (width * SSAA_FACTOR);
	  fy += ssaa_y / (height * SSAA_FACTOR);
	  VECT3_MUL_S(a, u, fx);
	  VECT3_MUL_S(b, v, fy);
	  VECT3_ADD(a, a, b);
	  VECT3_ADD(a, a, o);
	  VECT3_NORM(a);
	  camera->vectors[y * width + x + s * nb_pixels] = a;
	}
      }
    }
  }

  // packet rays
#ifdef PACKET_RAYS
# if SSA_FACTOR > 1
#  error "SSA_FACTOR Not supported"
# endif
  {
    size_t i;
    size_t x;
    size_t y;
    ray_packet_t *packet;

    camera->nb_packets = width * height / 4;
    camera->packets = xmalloc(sizeof(*camera->packets) * camera->nb_packets);
    packet = camera->packets;

    for (y = 0; y < height; y += 2) {
      for (x = 0; x < width; x += 2) {
        for (i = 0; i < 4; ++i) {
          vect3_t *vector;

          packet->org.x[i] = camera->org.x;
          packet->org.y[i] = camera->org.y;
          packet->org.z[i] = camera->org.z;

          /*
          if (i < 2) {
            vector = &camera->vectors[y * width + x + i];
          } else {
            vector = &camera->vectors[(y + 1) * width + x + i % 2];
          }
          */
          vector = &camera->vectors[(y + i / 2) * width + x + i % 2];

          packet->dir.x[i] = vector->x;
          packet->dir.y[i] = vector->y;
          packet->dir.z[i] = vector->z;
        }
        packet += 1;
      }
    }
  }
#endif
}

void clean_camera(camera_t *camera)
{
  free(camera->vectors);
  free(camera->packets);
  bzero(camera, sizeof(*camera));
}
