#ifndef __CAMERA_H__
# define __CAMERA_H__

# include <stdlib.h>

# include "../vect/vect.h"

typedef struct camera_s {
	vect3_t	org;
	vect3_t	dir;
	vect3_t vert;
	size_t width;
	size_t height;
	size_t nb_packets;
	vect3_t *vectors;
	ray_packet_t *packets;
} camera_t;

void init_camera(
    camera_t *camera,
    size_t width, size_t height,
    float org_x, float org_y, float org_z,
    float dir_x, float dir_y, float dir_z,
    float vert_x, float vert_y, float vert_z
    );

void clean_camera(camera_t *camera);

#endif
