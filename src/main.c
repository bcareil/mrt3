#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include <SDL2/SDL_ttf.h>

#ifdef MULTI_THREAD
# include <omp.h>
#endif

#include "scene/scene.h"
#include "mrt3.h"
#include "utils.h"
#include "version.h"

static long int generate_seed()
{
  long int ret;

  // TODO: randomize seed again
  //ret = time(NULL);
  //ret += getpid();
  ret = 0x12345678;
  return ret;
}

static void print_kdtree_info(scene_t *scene)
{
  kd_tree_it_t it;
  kd_tree_node_data_t *data;
  size_t i;
  size_t min = (size_t)-1;
  size_t max = 0;
  size_t sum = 0;
  size_t nb_unique_triangles;
  size_t nb_leaves = 0;
  size_t nb_free_leaves;
  int *is_in_tree;

  is_in_tree = xmalloc(sizeof(*is_in_tree) * scene->nb_triangles);
  bzero(is_in_tree, sizeof(*is_in_tree) * scene->nb_triangles);

  nb_free_leaves =
      scene->kdtree.node_pool_size -
      scene->kdtree.node_pool_usage;

  kdtree_iterate(&it, &scene->kdtree);
  while ((data = kdtree_next_node(&it)) != NULL) {
    size_t nb_triangles;

    if (data->split_axis == AXIS_COUNT) {
      nb_leaves++;
    }

#ifdef USE_KDTREE_V2
    nb_triangles = data->triangles.nb_triangles;
#else
    nb_triangles = data->nb_triangles;
#endif

    sum += nb_triangles;
    if (nb_triangles < min) {
      min = nb_triangles;
    }
    if (nb_triangles > max) {
      max = nb_triangles;
    }

    for (i = 0; i < nb_triangles; ++i) {
#ifdef USE_KDTREE_V2
      is_in_tree[data->triangles.indices[i]] = 1;
#else
      is_in_tree[data->triangles[i].index] = 1;
#endif
    }
  }

  nb_unique_triangles = 0;
  for (i = 0; i < scene->nb_triangles; ++i) {
    if (is_in_tree[i] == 0) {
      printf("W triangle %zu is not in kdtree\n", i);
    } else {
      nb_unique_triangles++;
    }
  }

  printf("Kd-Tree:\n");
  printf(" - %8zu triangles registered\n", sum);
  printf(" - %8zu unique triangles\n", nb_unique_triangles);
  printf(" - %8zu leaves used\n", nb_leaves);
  printf(" - %8zu leaves free\n", nb_free_leaves);
  printf(" - %8zu max triangle in a leaf\n", max);
  printf(" - %8zu min triangle in a leaf\n", min);
  printf(" - %8.1f avg triangle per leaf\n", ((float)sum) / ((float)nb_leaves));

  free(is_in_tree);
}

static void print_info(scene_t *scene)
{
  printf("%i:%s\n", PROJECT_REVISION, PROJECT_CHANGESET);
  printf("Options:\n");
#if SSAA_FACTOR > 1
  printf(" - SSAA x%i\n", SSAA_FACTOR);
#else
  puts(" - anti-aliasing disabled");
#endif
#ifdef PACKET_RAYS
  puts(" - rays packed");
#else
  puts(" - rays traced one at a time");
#endif
#ifdef MULTI_THREAD
  printf(
      " - multithread enabled, %i threads max\n",
      omp_get_max_threads()
      );
#else
  puts(" - multithread disabled");
#endif
#ifdef TEST_CULLING
  puts(" - culling test enabled");
#else
  puts(" - culling test disabled");
#endif
#ifdef DIRECT_LIGHTNING_PREDICTION
  puts(" - direct lightning prediction enabled");
#else
  puts(" - direct lightning prediction disabled");
#endif
#ifdef USE_KDTREE
# ifdef USE_KDTREE_V2
  puts(" - kdtree enabled");
# else
  puts(" - kdtree v2 enabled");
# endif
# ifdef USE_KDTREE_ORDERED_COLLISION_CHECK
  puts(" - kdtree collision checks are ordered");
# else
  puts(" - kdtree collision checks are unordered");
# endif
#else
  puts(" - kdtree disabled");
#endif
#ifdef USE_AABB_INTERSECTION_V2
  puts(" - using aabb intersection v2");
#else
  puts(" - using aabb intersection v1");
#endif
  printf("Loaded scene:\n");
  printf(" - %8zu triangles\n", scene->nb_triangles);
  printf(" - %8zu objects\n", scene->nb_objects);
  printf(" - %8zu lights\n", scene->nb_lights);
  printf(" - %8zu materials\n", scene->nb_materials);
  printf(" - %8zu materials references\n", scene->nb_material_refs);
  print_kdtree_info(scene);
}

static sdl_context_t init_sdl()
{
  sdl_context_t sdl = { NULL, NULL };

  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    fprintf(stderr,
            "The initialization of the SDL failed : %s\n",
            SDL_GetError()
            );
    return sdl;
  }

  if (TTF_Init() == -1) {
    fprintf(stderr, "TTF_Init: %s\n", TTF_GetError());
    SDL_Quit();
    return sdl;
  }

  SDL_Window* window = SDL_CreateWindow(
      "mrt3",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED,
      SCREEN_WIDTH,
      SCREEN_HEIGHT,
      0
      );
  if (window == NULL) {
    fprintf(stderr,
        "The initialization of the window failed: %s\n",
        SDL_GetError()
        );
    TTF_Quit();
    SDL_Quit();
    return sdl;
  }

  SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
  if (renderer == NULL) {
    fprintf(stderr,
        "The initialization of the renderer failed: %s\n",
        SDL_GetError()
        );
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return sdl;
  }

  sdl.window = window;
  sdl.renderer = renderer;
  return sdl;
}

static int parse_args(int ac, char **av, args_t *args)
{
  if (ac <= 0) {
    my_strlcpy(args->error, "No arguments", sizeof(args->error));
    return 1;
  }
  if (ac == 1) {
    my_strlcpy(args->error, "Missing scene to render", sizeof(args->error));
    return 1;
  }
  args->filename = av[1];
  ac -= 2;
  av += 2;
  while (ac > 0) {
    char const *arg = av[0];
    // iteration count
    if (strcmp(arg, "-n") == 0) {
      if (ac <= 1) {
        my_strlcpy(args->error, "Missing argument after -n", sizeof(args->error));
        return 1;
      }
      char *max_iter_str = av[1];
      int max_iter = atoi(max_iter_str);
      if (max_iter <= 0 || max_iter > MAX_ITER_MAX) {
        snprintf(
            args->error, sizeof(args->error),
            "Invalid max iteration count '%s'. Expected integer between [1, %i].",
            max_iter_str, MAX_ITER_MAX
            );
        return 1;
      }
      args->max_iteration = max_iter;
      // consume argument
      --ac;
      ++av;
    // quit after rendering
    } else if (strcmp(arg, "-q") == 0) {
      args->quit_immediatly = 1;
    // unknown argument
    } else {
      snprintf(args->error, sizeof(args->error), "Unknown argument '%s'", arg);
    }
    // next argument
    --ac;
    ++av;
  }
  return 0;
}

void print_usage(int ac, char **av)
{
  char const *pname = "./mrt3";
  if (ac > 0) {
    pname = av[0];
  }
  fprintf(stderr, "Usage: %s SCENE_FILE [-n MAX_ITER] [-q]\n", pname);
}

int main(int ac, char **av)
{
  sdl_context_t sdl;
  scene_t scene;
  int error;
  args_t args;

  // process args
  memset(&args, 0, sizeof(args));
  if (parse_args(ac, av, &args)) {
    if (args.error[0] != '\0') {
      fputs(args.error, stderr);
      fputc('\n', stderr);
    }
    print_usage(ac, av);
    return 1;
  }
  // init seed
  srand48(generate_seed());
  // init sdl
  sdl = init_sdl();
  if (sdl.window == NULL) {
    return 1;
  }
  // load scene
  error = load_scene(&scene, args.filename);
  if (error) {
    fprintf(stderr, "Error %i: unable to load the scene %s.\n", error, args.filename);
    return 1;
  }
  print_info(&scene);
  // disable flush on new line for stdout
  error = setvbuf(stdout, NULL, _IOFBF, 0);
  if (error) {
    perror("setvbuf");
    // NOTE: non critical error
  }
  // launch rt
  mrt(sdl, &scene, &args);
  // cleanup
  scene_clear(&scene);
  SDL_DestroyRenderer(sdl.renderer);
  SDL_DestroyWindow(sdl.window);
  TTF_Quit();
  SDL_Quit();
  return 0;
}
