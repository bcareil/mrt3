#ifndef __BBOX_H__
# define __BBOX_H__

# include "vect/vect.h"

typedef struct bbox_aa_s {
	vect3_t origin;
	vect3_t dimensions;
} bbox_aa_t;

typedef struct bbox_aa_p_s {
	vect3_t origin;
	vect3_t opposit;
} bbox_aa_p_t;

# define BBOX_AA_PF_STR "(org=" VECT3_PF_STR ";dim=" VECT3_PF_STR ")"

# define BBOX_AA_PF_EX(b) VECT3_PF_EX((b).origin), VECT3_PF_EX((b).dimensions)

# define BBOX_AA_PRINTF(name, b) \
	printf(name "=" BBOX_AA_PF_STR "\n", BBOX_AA_PF_EX(b))

# define BBOX_AA_P_PF_STR "(org=" VECT3_PF_STR ";ops=" VECT3_PF_STR ")"

# define BBOX_AA_P_PF_EX(b) VECT3_PF_EX((b).origin), VECT3_PF_EX((b).opposit)

# define BBOX_AA_P_PRINTF(name, b) \
	printf(name "=" BBOX_AA_P_PF_STR "\n", BBOX_AA_P_PF_EX(b))

/**
 * Create a bbox from two points in space
 */
void bbox_from_points(
    bbox_aa_t *result,
    vect3_t const *a,
    vect3_t const *b
    );

/**
 * Grow the bbox in every direction of to_add / 2.
 *
 * @param to_add length to add to each dimensions
 *               of the bbox
 */
void bbox_grow(
    bbox_aa_t *bbox,
    float to_add
    );

/**
 * Find the closest point of the @a bbox from the given @a point
 * and store it in the argument @a closest
 */
void bbox_aa_closest_point(
    bbox_aa_t const *bbox,
    vect3_t const *point,
    vect3_t *closest
    );

/**
 * Compute the center of the bbox @a bbox and store it in the
 * vector @a bbox_center
 */
inline void bbox_aa_compute_center(
    vect3_t *bbox_center,
    bbox_aa_t const *bbox
    )
{
  VECT3_MUL_S(*bbox_center, bbox->dimensions, 0.5f);
  VECT3_ADD(*bbox_center, *bbox_center, bbox->origin);
}

int bbox_aa_ray_intersects(
    vect3_t *intersect,
    bbox_aa_t const *bbox,
    ray_t const *ray
    );

int bbox_aa_ray_packet_intersects(
    bbox_aa_t const *bbox,
    ray_packet_t const *packet_p
    );

#endif /* __BBOX_H__ */
