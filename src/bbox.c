#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "vect/sse.h"
#include "bbox.h"
#include "utils.h"

void bbox_from_points(
    bbox_aa_t *result,
    vect3_t const *a,
    vect3_t const *b
    )
{
  vect3_t min;
  vect3_t max;

  VECT3_MIN(min, *a, *b);
  VECT3_MAX(max, *a, *b);

  result->origin = min;
  VECT3_SUB(result->dimensions, max, min);
}

void bbox_grow(
    bbox_aa_t *bbox,
    float to_add
    )
{
  VECT3_ADD_S(bbox->dimensions, bbox->dimensions, to_add);
  VECT3_SUB_S(bbox->origin, bbox->origin, to_add / 2.f);
}

void bbox_aa_closest_point(
    bbox_aa_t const *bbox,
    vect3_t const *point,
    vect3_t *closest
    )
{
  vect3_t opposit;

  VECT3_ADD(opposit, bbox->origin, bbox->dimensions);
  closest->x = MIN(MAX(bbox->origin.x, point->x), opposit.x);
  closest->y = MIN(MAX(bbox->origin.y, point->y), opposit.y);
  closest->z = MIN(MAX(bbox->origin.z, point->z), opposit.z);
}

typedef union vec_u {
  __m128        m;
  float         f[4];
  vect3_t       v;
} vec_t;


#ifdef USE_AABB_INTERSECTION_V2
int bbox_aa_ray_intersects(
    vect3_t *intersect,
    bbox_aa_t const *bbox,
    ray_t const *ray
    )
{
  __m128 ray_org = VECT3_TO_SSE(ray->org);
  __m128 ray_dir = VECT3_TO_SSE(ray->dir);
  __m128 ray_dir_inv = _mm_rcp_ps(ray_dir);
  __m128 bbox_lo = VECT3_TO_SSE(bbox->origin);
  __m128 bbox_hi = VECT3_TO_SSE(bbox->dimensions);
  vec_t dim_lo;
  vec_t dim_hi;
  vec_t tmin;
  vec_t tmax;
  __m128 inter;
  float l;
  float h;

  // place box origin to ray origin
  dim_lo.m = _mm_sub_ps(bbox_lo, ray_org);
  dim_hi.m = _mm_add_ps(dim_lo.m, bbox_hi);

  //
  dim_lo.m = _mm_mul_ps(dim_lo.m, ray_dir_inv);
  dim_hi.m = _mm_mul_ps(dim_hi.m, ray_dir_inv);

  // swap where get the maximum of the minimum values and
  // the maximum of the minimum values
  tmin.m = _mm_min_ps(dim_lo.m, dim_hi.m);
  tmax.m = _mm_max_ps(dim_lo.m, dim_hi.m);
  l = MAX(MAX(tmin.v.x, tmin.v.y), tmin.v.z);
  h = MIN(MIN(tmax.v.x, tmax.v.y), tmax.v.z);

# if 0
  if (verbose) {
    BBOX_AA_PRINTF("bbox", *bbox);
    RAY_PRINTF("ray", *ray);
    VECT3_PRINTF("dim_lo    ", dim_lo.v);
    VECT3_PRINTF("dim_hi    ", dim_hi.v);
    VECT3_PRINTF("tmin      ", tmin.v);
    VECT3_PRINTF("tmax      ", tmax.v);
    printf("l=%.3f\nh=%.3f\nl > h = %c\n", l, h, (l > h ? 't' : 'f'));
  }
# endif

  // no intersection if nan (ray_dir or box dim is 0)
  if (h == NAN || h < 0.f || l == NAN || l > h) {
    return 0;
  }

  // compute the intersection point
  inter = _mm_add_ps(ray_org, _mm_mul_ps(ray_dir, _mm_load_ps1(&l)));
  _mm_store_ss(&intersect->x, inter);
  return 1;
}

# if 0
// use to compare intersection algorithm
int bbox_aa_ray_intersects2(
    vect3_t *intersect,
    bbox_aa_t const *bbox,
    ray_t const *ray
    )
{
  int hit;

  hit = bbox_aa_ray_intersect2_v(intersect, bbox, ray, false);
  if (hit != bbox_aa_ray_intersects(intersect, bbox, ray)) {
    vect3_t closest = VECT3_ZERO;

    printf(
        "WARN: Expected a %s, got a %s\n",
        (!hit ? "hit": "miss"),
        (hit ? "hit" : "miss")
        );
    bbox_aa_ray_intersect2_v(intersect, bbox, ray, true);
    bbox_aa_closest_point(bbox, intersect, &closest);
    VECT3_PRINTF("closest     ", closest);
    VECT3_PRINTF("intersection", *intersect);
    BBOX_AA_PRINTF("bbox", *bbox);
    if (fabs(intersect->x - closest.x) > 0.001f ||
        fabs(intersect->y - closest.y) > 0.001f ||
        fabs(intersect->z - closest.z) > 0.001f) {
      puts("ERROR: closest too far");
      exit(1);
    }
  }
  return hit;
}
# endif

#else
// AABB INTERSECTION V1

typedef enum quadrant_e {
  RIGHT,
  LEFT,
  MIDDLE,
} quadrant_t;


/*
 * Ray/AABB intersection by Andrew Woo
 */
int bbox_aa_ray_intersects(
    vect3_t *intersect,
    bbox_aa_t const *bbox,
    ray_t const *ray
    )
{
  size_t i;
  size_t which_plane;
  int inside;
  quadrant_t quadrant[AXIS_COUNT];
  float max_t[AXIS_COUNT];
  float candidate_plane[AXIS_COUNT];
  vect3_t ray_org;

  // move box to origin
  VECT3_SUB(ray_org, ray->org, bbox->origin);

  // find candidates plane
  inside = 1;
  for (i = 0; i < AXIS_COUNT; ++i) {
    if (VECT_COMP(ray_org, i) < 0.f) {
      quadrant[i] = LEFT;
      candidate_plane[i] = 0.f;
      inside = 0;
    } else if (VECT_COMP(ray_org, i) > VECT_COMP(bbox->dimensions, i)) {
      quadrant[i] = RIGHT;
      candidate_plane[i] = VECT_COMP(bbox->dimensions, i);
      inside = 0;
    } else {
      quadrant[i] = MIDDLE;
    }
  }

  // ray origin may be inside
  if (inside) {
    *intersect = ray->org;
    return 1;
  }

  // calculate t distance to candidate plane
  for (i = 0; i < AXIS_COUNT; ++i) {
    if (quadrant[i] != MIDDLE && VECT_COMP(ray->dir, i) != 0.f) {
      max_t[i] = (candidate_plane[i] - VECT_COMP(ray_org, i)) / VECT_COMP(ray->dir, i);
    } else {
      max_t[i] = -1.f;
    }
  }

  // get largest max_t
  which_plane = 0;
  for (i = 1; i < AXIS_COUNT; ++i) {
    if (max_t[which_plane] < max_t[i]) {
      which_plane = i;
    }
  }
  if (max_t[which_plane] < 0.f) {
    return 0;
  }

  // check final candidate actually inside box
  for (i = 0; i < AXIS_COUNT; ++i) {
    float coord;
    if (which_plane != i) {
      coord = VECT_COMP(ray_org, i) + max_t[which_plane] * VECT_COMP(ray->dir, i);
      if (coord < 0 || coord > VECT_COMP(bbox->dimensions, i)) {
        return 0;
      }
    } else {
      coord = candidate_plane[i];
    }
    VECT_COMP((*intersect), i) = coord;
  }

  // translate intersection
  intersect->w = 0;
  VECT3_ADD((*intersect), (*intersect), bbox->origin);

  return 1;
}

#endif /* else of USE_AABB_INTERSECTION_V2 */

int bbox_aa_ray_packet_intersects(
    bbox_aa_t const *bbox,
    ray_packet_t const *packet_p
    )
{
  sse_vect3_packet_t bbox_org, bbox_dim;
  sse_ray_packet_t packet;
  sse_vect3_packet_t inv_packet_dir;
  sse_vect3_packet_t l1, l2;
  __m128 tmax, tmin;
  __m128 mask;
  __m128 zero, tmp;

  // load to sse
  tmp = VECT3_TO_SSE(bbox->origin);
  VECT3_TO_SSE_VECT3_PACKET(bbox_org, tmp);
  tmp = VECT3_TO_SSE(bbox->dimensions);
  VECT3_TO_SSE_VECT3_PACKET(bbox_dim, tmp);
  RAY_PACKET_TO_SSE(packet, *packet_p);

  // l1 = bbox.org - ray.org
  l1.x = _mm_sub_ps(bbox_org.x, packet.org.x);
  l1.y = _mm_sub_ps(bbox_org.y, packet.org.y);
  l1.z = _mm_sub_ps(bbox_org.z, packet.org.z);

  // l2 = (bbox.org + dim) - ray.org
  l2.x = _mm_sub_ps(_mm_add_ps(bbox_org.x, bbox_dim.x), packet.org.x);
  l2.y = _mm_sub_ps(_mm_add_ps(bbox_org.y, bbox_dim.y), packet.org.y);
  l2.z = _mm_sub_ps(_mm_add_ps(bbox_org.z, bbox_dim.z), packet.org.z);

  // compute packet dir reciproc (rcp is as fast as mul, but div is way slower)
  inv_packet_dir.x = _mm_rcp_ps(packet.dir.x);
  inv_packet_dir.y = _mm_rcp_ps(packet.dir.y);
  inv_packet_dir.z = _mm_rcp_ps(packet.dir.z);

  // l1 = l1 / ray.dir
  l1.x = _mm_mul_ps(l1.x, inv_packet_dir.x);
  l1.y = _mm_mul_ps(l1.y, inv_packet_dir.y);
  l1.z = _mm_mul_ps(l1.z, inv_packet_dir.z);

  // l2 = l2 / ray.dir
  l2.x = _mm_mul_ps(l2.x, inv_packet_dir.x);
  l2.y = _mm_mul_ps(l2.y, inv_packet_dir.y);
  l2.z = _mm_mul_ps(l2.z, inv_packet_dir.z);

  // tmax = max(max[min(l1.x, l2.x), min(l1.y, l2.y)], min(l1.z, l2.z))
  tmax =
    _mm_max_ps(
        _mm_max_ps(
          _mm_min_ps(l1.x, l2.x),
          _mm_min_ps(l1.y, l2.y)
          ),
        _mm_min_ps(l1.z, l2.z)
        );

  // tmin = min(min[max(l1.x, l2.x), max(l1.y, l2.y)], max(l1.z, l2.z))
  tmin =
    _mm_min_ps(
        _mm_min_ps(
          _mm_max_ps(l1.x, l2.x),
          _mm_max_ps(l1.y, l2.y)
          ),
        _mm_max_ps(l1.z, l2.z)
        );

  // true if tmax >= 0 && tmax >= tmin
  zero = _mm_setzero_ps();
  mask = _mm_and_ps(
      _mm_cmpge_ps(tmax, zero),
      _mm_cmpge_ps(tmax, tmin)
      );

#if 0
  puts("--------------------");
  SSE_PRINTF("l1.x", l1.x);
  SSE_PRINTF("l1.y", l1.y);
  SSE_PRINTF("l1.z", l1.z);
  SSE_PRINTF("l2.x", l2.x);
  SSE_PRINTF("l2.y", l2.y);
  SSE_PRINTF("l2.z", l2.z);
  SSE_PRINTF("tmin", tmin);
  SSE_PRINTF("tmax", tmax);
  printf("mask=0x%02X\n", _mm_movemask_ps(mask));
#endif

  return (0 != _mm_movemask_ps(mask));
}
