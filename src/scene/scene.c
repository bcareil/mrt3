#include <stdio.h>
#include <strings.h>

#include "scene.h"

#include "clock/clock.h"
#include "waveform/waveform.h"

int load_scene(scene_t *scene, char const *fname)
{
  my_clock_t clock;
  FILE *file;
  waveform_obj_t wf_obj;
  int error;

  // open file
  file = fopen(fname, "r");
  if (file == NULL) {
    perror("Could not open scene");
    return 1;
  }

  // load it
  clock_init(&clock);
  error = waveform_obj_parse_file(&wf_obj, file);
  fclose(file);
  if (error) {
    clean_waveform_obj(&wf_obj);
    return error + 2;
  }
  printf("Waveform file parsed in %.6fs\n", clock_get_elapsed_seconds(&clock));

  // convert it
  waveform_obj_to_scene(scene, &wf_obj);
  clean_waveform_obj(&wf_obj);

  printf("Total scene loading from file took %.6fs\n",
      clock_get_elapsed_seconds(&clock));
  return 0;
}

void scene_clear(scene_t *scene)
{
  clean_camera(&scene->camera);
  clean_kdtree(&scene->kdtree);
  clean_otree(&scene->otree);
  free(scene->triangles);
  free(scene->triangle_normals);
  free(scene->lights);
  free(scene->objects);
  free(scene->materials);
  free(scene->material_refs);
  bzero(scene, sizeof(*scene));
}
