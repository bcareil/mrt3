#ifndef __SCENE_H__
# define __SCENE_H__

# include "bbox.h"
# include "spatializers/kdtree-impl.h"
# include "spatializers/otree.h"
# include "material.h"
# include "vect/vect.h"
# include "triangle/triangle.h"
# include "camera/camera.h"
# include "color/color.h"

typedef struct material_ref_s {
	material_t *material;
	size_t triangles_start;
	size_t triangles_stop;
} material_ref_t;

typedef struct light_s {
	material_t *material;
	size_t triangles_start;
	size_t triangles_stop;
	bbox_aa_t bbox;
} light_t;

typedef struct object_s {
	char name[64];
	int smooth;
	size_t triangles_start;
	size_t triangles_stop;
	size_t normals_start;
	size_t normals_stop;
	bbox_aa_t bbox;
} object_t;

typedef struct scene_s {
	camera_t camera;
	kd_tree_t kdtree;
	o_tree_t otree;
	size_t nb_triangles;
	triangle_t *triangles;
	size_t nb_triangle_normals;
	triangle_t *triangle_normals;
	size_t nb_lights;
	light_t *lights;
	size_t nb_objects;
	object_t *objects;
	size_t nb_materials;
	material_t *materials;
	size_t nb_material_refs;
	material_ref_t *material_refs;
} scene_t;

int load_scene(scene_t *scene, char const *fname);
void scene_clear(scene_t *scene);

#endif
