#ifndef __MATERIAL_H__
# define __MATERIAL_H__ 

# include "color/color.h"
# include "vect/vect.h"
# include "triangle/triangle.h"

typedef struct material_s {
	int is_light;
	color_t diffuse_color;
	color_t specular_color;
} material_t;

# define MATERIAL_PF_STR "(is_light=%i;Kd=" COLOR_PF_STR ";Ks=" COLOR_PF_STR ")"

# define MATERIAL_PF_EX(m) \
	(m).is_light, COLOR_PF_EX((m).diffuse_color), COLOR_PF_EX((m).specular_color)

# define MATERIAL_PRINTF(name, m) \
	printf(name "=" MATERIAL_PF_STR "\n", MATERIAL_PF_EX(m))

#endif /* __MATERIAL_H__ */
