#include <math.h>
#include <strings.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "mrt3.h"

#include "clock/clock.h"
#include "color/color.h"

color_t const SKY_COLOR = {0, 0, 0, 1};

static void init_context(
    context_t *context,
    scene_t *scene,
    int max_iteration
    )
{
  size_t i;
  color_t black = COLOR_BLACK;

  context->iteration = 0;
  context->max_iteration = MAX(1, max_iteration);
  context->scene = scene;
  context->nb_pixels = scene->camera.width * scene->camera.height;
  context->render_buffer = xmalloc(
      sizeof(*context->render_buffer) * context->nb_pixels
      );
  for (i = 0; i < context->nb_pixels; ++i) {
    context->render_buffer[i] = black;
  }
}

static void clean_context(
    context_t *context
    )
{
  free(context->render_buffer);
  bzero(context, sizeof(*context));
}

static void render_buffer_to_tex(
    SDL_Texture *tex,
    context_t const *context
    )
{
#ifndef PACKET_RAYS
  size_t i;
  size_t x;
  size_t y;
  size_t extra;
  uint32_t *pixels;
  int pitch;

  i = 0;
  SDL_LockTexture(tex, NULL, (void**)&pixels, &pitch);
  extra = pitch / 4U - context->scene->camera.width;
  for (y = 0; y < context->scene->camera.height; ++y) {
    for (x = 0; x < context->scene->camera.width; ++x) {
      pixels[i] = COLOR_TO_UINT32(context->render_buffer[i]);
      ++i;
    }
    i += extra;
  }
  SDL_UnlockTexture(tex);
#else
  int w;
  size_t i;
  size_t j;
  uint32_t *pixels;

  w = context->scene->camera.width;
  SDL_LockTexture(tex, NULL, (void**)&pixels, &pitch);
  for (i = 0; i < context->nb_pixels; i += 4) {
    uint32_t colors[4];
    int col;
    int row;

    for (j = 0; j < 4; ++j) {
      colors[j] = COLOR_TO_UINT32(context->render_buffer[i + j]);
    }

    col = i / 2;
    row = col / w;
    col = col % w;
    row *= 2;

    pixels[col + row * w] = colors[0];
    pixels[(col + 1) + row * w] = colors[1];
    pixels[col + (row + 1) * w] = colors[2];
    pixels[(col + 1) + (row + 1) * w] = colors[3];
  }
  SDL_UnlockTexture(tex);
#endif
}

static void render_text(
    sdl_context_t *sdl,
    char const *string,
    TTF_Font *font,
    SDL_Rect *text_area,
    SDL_Color color
    )
{
  SDL_Surface *text_srf;
  SDL_Texture *text_tex;

  text_srf = TTF_RenderUTF8_Solid(font, string, color);
  if (text_srf == NULL) {
    fprintf(stderr, "TTF_RenderUTF8_Solid: %s\n", TTF_GetError());
    abort();
  }

  text_tex = SDL_CreateTextureFromSurface(sdl->renderer, text_srf);

  SDL_RenderCopy(sdl->renderer, text_tex, NULL, text_area);

  SDL_DestroyTexture(text_tex);
  SDL_FreeSurface(text_srf);
 }

int mrt(sdl_context_t sdl, scene_t *scene, args_t const *args)
{
  context_t context;
  SDL_Event event;
  SDL_Texture *main_tex;
  SDL_Texture *text_bg_srf;
  SDL_Color text_color = {255, 255, 255, 255};
  TTF_Font *font;
  SDL_Rect text_area;
  SDL_Rect text_bg_area;
  char text[512];
  my_clock_t frame_timer;
  my_clock_t render_timer;
  int w, h, status;

  // retrieve the draw area size
  status = SDL_GetRendererOutputSize(sdl.renderer, &w, &h);
  if (status != 0) {
    fprintf(stderr, "Error: SDL_GetRendererOutputSize: %s\n", SDL_GetError());
    return 1;
  }

  // load font
  font = TTF_OpenFont("res/font/Terminus.ttf", 12);
  if (font == NULL) {
    fprintf(stderr, "Error: TTF_OpenFont: %s\n", TTF_GetError());
    return 1;
  }

  // create text background
  text_bg_srf = SDL_CreateTexture(
      sdl.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, 14
      );
  if (text_bg_srf == NULL) {
    fprintf(stderr, "Error: SDL_CreateTexture for textbg: %s\n", SDL_GetError());
    TTF_CloseFont(font);
    return 1;
  }

  // init text background
  SDL_SetRenderTarget(sdl.renderer, text_bg_srf);
  SDL_SetRenderDrawColor(sdl.renderer, 0, 0, 0, 0xff);
  SDL_RenderClear(sdl.renderer);
  SDL_SetRenderTarget(sdl.renderer, NULL);

  main_tex = SDL_CreateTexture(
      sdl.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, w, h
      );
  if (main_tex == NULL) {
    fprintf(stderr, "Error: SDL_CreateTexture for screen: %s\n", SDL_GetError());
    SDL_DestroyTexture(text_bg_srf);
    TTF_CloseFont(font);
    return 1;
  }

  // initial bg
  SDL_SetRenderDrawColor(sdl.renderer, 0xff, 0x11, 0xff, 0xff);
  SDL_RenderClear(sdl.renderer);

  // text area
  text_bg_area.w = w;
  text_bg_area.h = 14;
  text_bg_area.x = 0;
  text_bg_area.y = 0;
  text_area.w = w - 2;
  text_area.h = 12;
  text_area.x = 4;
  text_area.y = 1;

  // init context and clocks
  init_context(&context, scene, args->max_iteration);
  clock_init(&frame_timer);
  clock_init(&render_timer);

  while (context.iteration < context.max_iteration) {
    // process events
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        goto quit;
      }
    }

    // process and render scene
    mrt_process(&context);
    render_buffer_to_tex(main_tex, &context);
    SDL_RenderCopy(sdl.renderer, main_tex, NULL, NULL);

    // display iteration stats
    {
      float elapsed;

      elapsed = clock_get_elapsed_seconds(&frame_timer);
      clock_reset(&frame_timer);

      snprintf(
          text, sizeof(text), "SPF: %.3fs | it: %zu/%zu",
          elapsed, context.iteration, context.max_iteration
          );

      SDL_RenderCopy(sdl.renderer, text_bg_srf, NULL, &text_bg_area);
      render_text(&sdl, text, font, &text_area, text_color);
    }

    // commit
    SDL_RenderPresent(sdl.renderer);
  }

  // print summary for the whole rendering
  if (!args->quit_immediatly)
  {
    float elapsed;
    int hours, mins;
    float remaining_secs;

    elapsed = clock_get_elapsed_seconds(&render_timer);
    remaining_secs = elapsed;
    hours = elapsed / 3600;
    remaining_secs -= hours * 3600;
    mins = remaining_secs / 60;
    remaining_secs -= mins * 60;

    snprintf(
        text, sizeof(text), "AVG SPF: %.3fs | TOT: %02i:%02i:%06.3f",
        elapsed / context.iteration,
        hours, mins, remaining_secs
        );

    SDL_RenderCopy(sdl.renderer, text_bg_srf, NULL, &text_bg_area);
    render_text(&sdl, text, font, &text_area, text_color);
    SDL_RenderPresent(sdl.renderer);

    // wait for user input
    while (1) {
      if (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
          break;
        }
      }
    }
  }

quit:
  clean_context(&context);
  SDL_DestroyTexture(text_bg_srf);
  TTF_CloseFont(font);
  return 0;
}
