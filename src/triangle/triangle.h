#ifndef __TRIANGLE_H__
# define __TRIANGLE_H__

# include "../utils.h"
# include "../vect/vect.h"

typedef struct triangle_s {
	vect3_t v0;
	vect3_t v1;
	vect3_t v2;
} triangle_t;

# define TRIANGLE_PF_STR \
	"(v0=" VECT3_PF_STR ";v1=" VECT3_PF_STR ";v2=" VECT3_PF_STR ")"

# define TRIANGLE_PF_EX(t) \
	VECT3_PF_EX(t.v0), VECT3_PF_EX(t.v1), VECT3_PF_EX(t.v2)

# define TRIANGLE_PRINTF(name, t) \
	printf(name "=" TRIANGLE_PF_STR "\n", TRIANGLE_PF_EX(t))

typedef struct triangle_intersect_data_s {
	float distance;
	float u;
	float v;
} triangle_intersect_data_t;

typedef struct triangle_intersect_packet_data_s {
	float distance[4];
	float u[4];
	float v[4];
} triangle_intersect_packet_data_t;

int triangle_ray_intersects(
    triangle_intersect_data_t *data,
    ray_t const *ray,
    triangle_t const *triangle
    );

void triangle_ray_packet_intersects(
    triangle_intersect_packet_data_t *intersections,
    ray_packet_t const *packet,
    triangle_t const *triangle
    );

#endif
