#include "vect/sse.h"
#include "triangle.h"

#include "mrt3.h"

/**
 * Moller-Trumbore ray/triangle intersection implementation
 */
int triangle_ray_intersects(
    triangle_intersect_data_t *data,
    ray_t const *ray,
    triangle_t const *triangle
    )
{
  __m128 v0, v1, v2;
  __m128 e1, e2;
  __m128 p, q, t;
  __m128 ray_dir, ray_org;
  __m128 epsilon, zero;
  __m128 u, v;
  __m128 det, inv_det;
  __m128 dist;
#ifndef TEST_CULLING
  __m128 one;

  one = _mm_set_ps1(1.f);
#endif
  // load constants
  zero = _mm_setzero_ps();
  epsilon = _mm_set_ps1(EPSILON);
  // load triangle
  v0 = VECT3_TO_SSE(triangle->v0);
  v1 = VECT3_TO_SSE(triangle->v1);
  v2 = VECT3_TO_SSE(triangle->v2);
  // load ray
  ray_dir = VECT3_TO_SSE(ray->dir);
  ray_org = VECT3_TO_SSE(ray->org);
  // compute edges
  e1 = _mm_sub_ps(v1, v0);
  e2 = _mm_sub_ps(v2, v0);
  // get the normal of the plan defined by e2 and the ray's direction
  SSE_VECT3_CROSS_PRODUCT(p, ray_dir, e2);
  // compute the determinent
  det = SSE_VECT3_DOT_PRODUCT(e1, p);

#ifdef TEST_CULLING
  if (_mm_ucomilt_ss(det, epsilon)) {
    return 0;
  }

  // calculate the distance from v0 to the ray origin
  t = _mm_sub_ps(ray_org, v0);

  // calculate parameter u
  u = SSE_VECT3_DOT_PRODUCT(t, p);
  if (_mm_ucomilt_ss(u, zero) || _mm_ucomigt_ss(u, det)) {
    return 0;
  }

  SSE_VECT3_CROSS_PRODUCT(q, t, e1);

  v = SSE_VECT3_DOT_PRODUCT(ray_dir, q);
  if (_mm_ucomilt_ss(v, zero) || _mm_ucomigt_ss(_mm_add_ss(u, v), det)) {
    return 0;
  }

  dist = SSE_VECT3_DOT_PRODUCT(e2, q);
  if (_mm_ucomilt_ss(dist, epsilon)) {
    return 0;
  }

  inv_det = _mm_rcp_ss(det);
  dist = _mm_mul_ss(dist, inv_det);
  u = _mm_mul_ss(u, inv_det);
  v = _mm_mul_ss(v, inv_det);
#else
  // if the determinent is 0, p is parralell to e1
  if (_mm_ucomigt_ss(_mm_sub_ss(det, epsilon), zero) &&
      _mm_ucomilt_ss(det, epsilon)) {
    return 0;
  }

  // compute the inverse of the determinent
  inv_det = _mm_rcp_ss(det);

  // calculate the distance from v0 (the vertice origin of e1 and e2) to the ray
  // origin
  t = _mm_sub_ps(ray_org, v0);

  // calculate u parameter
  u = SSE_VECT3_DOT_PRODUCT(t, p);
  u = _mm_mul_ss(u, inv_det);
  if (_mm_ucomilt_ss(u, zero) || _mm_ucomigt_ss(u, one))
    return 0;
  }

  // get the normal of the plan defined by t and e1
  SSE_VECT3_CROSS_PRODUCT(q, t, e1);

  // calculate v parameter
  v = SSE_VECT3_DOT_PRODUCT(ray_dir, q);
  v = _mm_mul_ss(v, inv_det);
  if (_mm_ucomilt_ss(v, zero) || _mm_ucomigt_ss(_mm_add_ss(u, v), one)) {
    return 0;
  }

  // compute the distance from the ray origin to the intersection point along
  // the ray direction
  dist = SSE_VECT3_DOT_PRODUCT(e2, q);
  dist = _mm_mul_ss(dist, inv_det);
  if (_mm_ucomilt(dist, epsilon)) {
    return 0;
  }
#endif

  _mm_store_ss(&data->distance, dist);
  _mm_store_ss(&data->u, u);
  _mm_store_ss(&data->v, v);

  return 1;
}

void triangle_ray_packet_intersects(
    triangle_intersect_packet_data_t *intersections,
    ray_packet_t const *packet,
    triangle_t const *triangle
    )
{
#ifdef TEST_CULLING
  __m128 epsilon;
  __m128 v0;
  __m128 e1, e2;
  sse_vect3_packet_t e1_p, e2_p;
  sse_vect3_packet_t v0_p;
  sse_ray_packet_t rp;
  sse_vect3_packet_t p, q, t;
  __m128 det, inv_det;
  __m128 u, v;
  __m128 dist;
  __m128 mask;

  // compute edges e1 and e2
  {
    __m128 v1, v2;

    v0 = VECT3_TO_SSE(triangle->v0);
    v1 = VECT3_TO_SSE(triangle->v1);
    v2 = VECT3_TO_SSE(triangle->v2);
    e1 = _mm_sub_ps(v1, v0);
    e2 = _mm_sub_ps(v2, v0);
  }

  // load ray
  RAY_PACKET_TO_SSE(rp, *packet);

  // get the normal defined by e2 and the ray's direction
  // that is, a cross product between e2 and ray_dir_
  VECT3_TO_SSE_VECT3_PACKET(e2_p, e2);
  SSE_PACKED_CROSS_PRODUCT_PV(p, rp.dir, e2_p);

  // compute the determinent
  VECT3_TO_SSE_VECT3_PACKET(e1_p, e1);
  det = SSE_PACKED_DOT_PRODUCT(e1_p, p);

  // check det >= EPSILON
  epsilon = _mm_set_ps1(EPSILON);
  if (0xF == _mm_movemask_ps(_mm_cmplt_ps(det, epsilon))) {
    dist = _mm_setzero_ps();
    return;
  }

  // compute the distance form v0 to the ray's origin
  VECT3_TO_SSE_VECT3_PACKET(v0_p, v0);
  SSE_PACKED_SUB(t, rp.org, v0_p);

  // compute u parameter
  u = SSE_PACKED_DOT_PRODUCT(t, p);

  // check 0 <= u <= det
  if (0xF == _mm_movemask_ps(
	_mm_or_ps(
	  _mm_cmplt_ps(u, _mm_setzero_ps()),
	  _mm_cmplt_ps(det, u)
	  ))) {
    dist = _mm_setzero_ps();
    return;
  }

  // compte the cross product of t and e1
  SSE_PACKED_CROSS_PRODUCT_PV(q, t, e1_p);

  // compute v parameter
  v = SSE_PACKED_DOT_PRODUCT(rp.dir, q);

  // TODO: check 0 <= v && (u + v) <= det

  // compute distance from intersection
  dist = SSE_PACKED_DOT_PRODUCT(q, e2_p);

  // create a mask where each element is init to:
  // v < 0 || (u + v) > det || dist < EPSILON
  mask =
    _mm_or_ps(
      _mm_or_ps(
	_mm_cmplt_ps(v, _mm_setzero_ps()),
	_mm_cmpgt_ps(_mm_add_ps(u, v), det)
	),
      _mm_cmplt_ps(dist, epsilon)
      );

  // set all dist and det of those verifying the above condition to 0
  // and 1 respectively
  dist = _mm_blendv_ps(dist, _mm_setzero_ps(), mask);
  det = _mm_blendv_ps(det, _mm_set_ps1(1.f), mask);

  // scale results using 1/det
  inv_det = _mm_rcp_ps(det);
  dist = _mm_mul_ps(dist, inv_det);
  u = _mm_mul_ps(u, inv_det);
  v = _mm_mul_ps(v, inv_det);

  // put result in arrays
  _mm_store_ps(intersections->distance, dist);
  _mm_store_ps(intersections->u, u);
  _mm_store_ps(intersections->v, v);

#else /* TEST_CULLING */
# error "Only test culling is implmented"
#endif /* TEST_CULLING */
}
