cmake_minimum_required(VERSION 2.6)

project(mrt3_triangle_intersection_test)

set (CMAKE_BUILD_TYPE Debug)
set (CMAKE_VERBOSE_MAKEFILE on)

# cflags
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pipe -O3 -W -Wall -march=native")

set (LIBS m)

# includes
include_directories("../../src" "./src")

# sources
set (SOURCES_COMMON
	src/common.c
	../../src/utils.c
	../../src/triangle/triangle.c
	../../src/clock/clock.c
	)

set (SOURCES_SR ${SOURCES_COMMON} src/test_single_ray.c)
set (SOURCES_RP ${SOURCES_COMMON} src/test_ray_packet.c)

# defines
add_definitions( -DTEST_CULLING )
add_definitions( -DINLINE_VECT )
add_definitions( -DUSE_SSE )

# targets
add_executable(test_single_ray ${SOURCES_SR})
target_link_libraries(test_single_ray ${LIBS})

add_executable(test_ray_packet ${SOURCES_RP})
target_link_libraries(test_ray_packet ${LIBS})
