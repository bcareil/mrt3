#ifndef __COMMON_H__
# define __COMMON_H__ 

# include <stdlib.h>

# include "vect/vect.h"
# include "triangle/triangle.h"

typedef enum ray_hit_kind_e {
	RHK_HIT_TRIANGLE,
	RHK_MISS_TRIANGLE,
	RHK_OPPOSITE_TRIANGLE,
	RHK_MISS_AND_OPPOSITE,

	RHK_COUNT
} ray_hit_kind_t;

void generate_ray(
    ray_t *ray,
    vect3_t const *org_gen_center,
    float org_gen_radius,
    triangle_t const *triangle,
    ray_hit_kind_t hit
    );

void init_rays_weighted_random(
    triangle_t *triangle,
    ray_t *rays,
    size_t nb_rays,
    float phit
    );

#endif /* __COMMON_H__ */
