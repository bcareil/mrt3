#include <stdlib.h>
#include <stdio.h>

#include "clock/clock.h"
#include "vect/vect.h"
#include "triangle/triangle.h"
#include "utils.h"

#include "common.h"

#define NB_RAYS 50000000

static void run_test(
    triangle_t const * triangle,
    ray_t const *rays,
    size_t nb_rays
    )
{
  triangle_intersect_data_t data;
  my_clock_t clock;
  double elapsed;
  size_t nb_hit;
  size_t i;

  printf("Starting test... ");
  fflush(stdout);

  clock_init(&clock);
  nb_hit = 0;
  for (i = 0; i < nb_rays; ++i) {
    nb_hit += (0 != triangle_ray_intersects(&data, &rays[i], triangle));
  }
  elapsed = clock_get_elapsed_seconds(&clock);

  puts("DONE");
  printf(" - hit rate:     %12.2f %%\n", ((float)nb_hit) / ((float)nb_rays) * 100.f);
  printf(" - elapsed time: %12.3f s\n", elapsed);
  printf(" - speed:        %12.1f ray/s\n", ((double)nb_rays) / elapsed);
}

static void test_single_ray_intersection()
{
  ray_t *rays;
  triangle_t triangle;

  rays = xmalloc(NB_RAYS * sizeof(*rays));

  puts("Weighted random (50/50 hit/miss rate)");
  printf("Generating %u samples... ", NB_RAYS); fflush(stdout);
  init_rays_weighted_random(&triangle, rays, NB_RAYS, 0.5);
  puts("DONE");
  run_test(&triangle, rays, NB_RAYS);

  puts("Weighted random (10/90 hit/miss rate)");
  printf("Generating %u samples... ", NB_RAYS); fflush(stdout);
  init_rays_weighted_random(&triangle, rays, NB_RAYS, 0.1);
  puts("DONE");
  run_test(&triangle, rays, NB_RAYS);
}

int main(int ac, char **av)
{
  (void) ac;
  (void) av;

  test_single_ray_intersection();

  return 0;
}

