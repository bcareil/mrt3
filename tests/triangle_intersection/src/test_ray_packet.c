#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "clock/clock.h"
#include "vect/vect.h"
#include "triangle/triangle.h"
#include "utils.h"

#include "common.h"

#define NB_RAYS 16777216

static void convert_rays_to_ray_packets(
    ray_packet_t *packets,
    ray_t const *rays,
    size_t nb_rays
    )
{
  size_t i, j;
  ray_packet_t *packet;

  assert((nb_rays % 4) == 0);
  packet = packets;
  for (i = 0; i < nb_rays; i += 4) {
    for (j = 0; j < 4; ++j) {
      packet->org.x[j] = rays[i + j].org.x;
      packet->org.y[j] = rays[i + j].org.y;
      packet->org.z[j] = rays[i + j].org.z;
      packet->dir.x[j] = rays[i + j].dir.x;
      packet->dir.y[j] = rays[i + j].dir.y;
      packet->dir.z[j] = rays[i + j].dir.z;
    }
    packet++;
  }
}

static void run_test(
    triangle_t const * triangle,
    ray_packet_t const *rays,
    size_t nb_packets
    )
{
  triangle_intersect_data_t data[4];
  my_clock_t clock;
  double elapsed;
  size_t nb_hit;
  size_t i;

  printf("Starting test... ");
  fflush(stdout);

  clock_init(&clock);
  nb_hit = 0;
  for (i = 0; i < nb_packets; ++i) {
    triangle_ray_packet_intersects(data, &rays[i], triangle);
    nb_hit += data[0].distance > 0.f;
    nb_hit += data[1].distance > 0.f;
    nb_hit += data[2].distance > 0.f;
    nb_hit += data[3].distance > 0.f;
  }
  elapsed = clock_get_elapsed_seconds(&clock);

  puts("DONE");
  printf(" - hit rate:     %12.2f %%\n", ((float)nb_hit) / ((float)(4 * nb_packets)) * 100.f);
  printf(" - elapsed time: %12.3f s\n", elapsed);
  printf(" - speed:        %12.1f it/s\n", ((double)nb_packets) / elapsed);
  printf(" - speed:        %12.1f ray/s\n", 4.0 * ((double)nb_packets) / elapsed);
}

static void test_single_ray_intersection()
{
  ray_t *rays;
  ray_packet_t *ray_packets;
  triangle_t triangle;
  size_t nb_packets;

  assert((NB_RAYS % 4) == 0);
  nb_packets = NB_RAYS / 4;
  rays = xmalloc(NB_RAYS * sizeof(*rays));
  ray_packets = xmalloc(nb_packets * sizeof(*ray_packets));

  puts("Weighted random (50/50 hit/miss rate)");
  printf("Generating %u samples... ", NB_RAYS); fflush(stdout);
  init_rays_weighted_random(&triangle, rays, NB_RAYS, 0.5);
  convert_rays_to_ray_packets(ray_packets, rays, NB_RAYS);
  puts("DONE");
  run_test(&triangle, ray_packets, nb_packets);

  puts("Weighted random (10/90 hit/miss rate)");
  printf("Generating %u samples... ", NB_RAYS); fflush(stdout);
  init_rays_weighted_random(&triangle, rays, NB_RAYS, 0.1);
  convert_rays_to_ray_packets(ray_packets, rays, NB_RAYS);
  puts("DONE");
  run_test(&triangle, ray_packets, nb_packets);
}

int main(int ac, char **av)
{
  (void) ac;
  (void) av;

  test_single_ray_intersection();

  return 0;
}

