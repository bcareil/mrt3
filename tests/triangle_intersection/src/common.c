#include <math.h>

#include "common.h"

static void compute_triangle_normal(
    vect3_t *normal,
    triangle_t const *triangle
    )
{
  vect3_t n, e1, e2;

  VECT3_SUB(e1, triangle->v1, triangle->v0);
  VECT3_SUB(e2, triangle->v2, triangle->v0);
  VECT3_CROSS(n, e1, e2);

  VECT3_NORM(n);
  *normal = n;
}

static void compute_triangle_barycenter(
    vect3_t *barycenter,
    triangle_t const *triangle
    )
{
  vect3_t b;

  VECT3_ADD(b, triangle->v0, triangle->v1);
  VECT3_ADD(b, b, triangle->v2);
  VECT3_DIV_S(b, b, 3);
  *barycenter = b;
}

void generate_vector_inside_sphere(
    vect3_t *vect,
    vect3_t const *sphere_center,
    float sphere_radius
    )
{
  double r, theta, phi;

  r = drand48() * sphere_radius;
  theta = drand48() * (M_PI / 2.0);
  phi = drand48() * M_PI;

  vect->x = (float)(r * sin(theta) * cos(phi));
  vect->y = (float)(r * sin(theta) * sin(phi));
  vect->z = (float)(r * cos(theta));
  vect->w = 0;

  VECT3_ADD((*vect), (*vect), (*sphere_center));
}

void generate_triangle(triangle_t *triangle)
{
  vect3_t sphere_center;

  VECT3_INIT(sphere_center, 0, 0, 0);
  generate_vector_inside_sphere(&triangle->v0, &sphere_center, 0.2);
  VECT3_INIT(sphere_center, 1, 0, 0);
  generate_vector_inside_sphere(&triangle->v1, &sphere_center, 0.2);
  VECT3_INIT(sphere_center, 0, 1, 0);
  generate_vector_inside_sphere(&triangle->v2, &sphere_center, 0.2);
}

void generate_ray(
    ray_t *ray,
    vect3_t const *org_gen_center,
    float org_gen_radius,
    triangle_t const *triangle,
    ray_hit_kind_t hit
    )
{
  vect3_t e1, e2;
  vect3_t point, tmp;
  float u, v;

  generate_vector_inside_sphere(&ray->org, org_gen_center, org_gen_radius);
  VECT3_SUB(e1, triangle->v1, triangle->v0);
  VECT3_SUB(e2, triangle->v2, triangle->v0);

  if ((hit & RHK_MISS_TRIANGLE) == 0) {
    u = drand48();
    v = drand48() * (1.f - u);
  } else {
    u = 0.1f + drand48() * 2.f;
    v = 0.1f + drand48() + 1.f - u;
  }

  VECT3_MUL_S(point, e1, u);
  VECT3_MUL_S(tmp, e2, v);
  VECT3_ADD(point, point, tmp);

  if ((hit & RHK_OPPOSITE_TRIANGLE) == 0) {
    VECT3_SUB(ray->dir, point, ray->org);
  } else {
    VECT3_SUB(ray->dir, ray->org, point);
  }

  VECT3_NORM(ray->dir);
}

void init_rays_weighted_random(
    triangle_t *triangle,
    ray_t *rays,
    size_t nb_rays,
    float phit
    )
{
  size_t i;
  vect3_t t_normal;
  vect3_t sphere_center;

  generate_triangle(triangle);
  compute_triangle_normal(&t_normal, triangle);
  compute_triangle_barycenter(&sphere_center, triangle);

  VECT3_MUL_S(t_normal, t_normal, 2.0f);
  VECT3_ADD(sphere_center, sphere_center, t_normal);

  for (i = 0; i < nb_rays; ++i) {
    generate_ray(
        &rays[i], 
        &sphere_center, 
        0.45f, 
        triangle, 
        (drand48() < phit ? RHK_HIT_TRIANGLE : 1 + (lrand48() % 3))
        );
  }
}

