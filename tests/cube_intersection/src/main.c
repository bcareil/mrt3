#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include "clock/clock.h"
#include "vect/vect.h"
#include "bbox.h"
#include "utils.h"

typedef int (*bbox_ray_inter_callback_t)(
    vect3_t *,          // intersection
    bbox_aa_t const *,  // bbox
    ray_t const *       // ray
    );


static long int generate_seed()
{
  long int ret;

  // TODO: randomize seed again
  ret = time(NULL);
  ret += getpid();
  return ret;
}

/*
 * generate a float within [-1, 1)
 */
static float random_float()
{
  return (drand48() - 0.5f) * 2.f;
}

static void generate_aa_bbox(bbox_aa_t *bbox)
{
  bbox->origin.x = random_float();
  bbox->origin.y = random_float();
  bbox->origin.z = random_float();
  bbox->dimensions.x = 0.1f + drand48() * 2.f;
  bbox->dimensions.y = 0.1f + drand48() * 2.f;
  bbox->dimensions.z = 0.1f + drand48() * 2.f;
}

static void generate_vector_within_sphere(
    vect3_t *vect,
    vect3_t const *sphere_center,
    float sphere_radius,
    float bias_percent,
    float bias_phi,
    float bias_theta
    )
{
  // invert the bias percentage
  float const inv_bias_percent = 1.f - bias_percent;

  // use spherical coordinate system
  // randomize orientation (0 <= phi < 2Pi; 0 <= theta <= Pi)
  float phi = bias_phi;
  float theta = bias_theta;

  phi += inv_bias_percent * 2.f * M_PI * drand48();
  phi = fmodf(phi, 2.f * M_PI);

  theta += inv_bias_percent * M_PI * drand48();
  theta = fmodf(theta, M_PI);

  // to cartezian coordinates
  vect->x = sphere_center->x + sphere_radius * (sin(theta) * cos(phi));
  vect->y = sphere_center->y + sphere_radius * (sin(theta) * sin(phi));
  vect->z = sphere_center->z + sphere_radius * (cos(theta));

  //printf("bias (phi= %.3f, theta=%.3f, pct=%.3f); generated (phi=%.3f, theta=%.3f)\n",
  //      bias_phi, bias_theta, bias_percent, phi, theta);
  //VECT3_PRINTF("vect", *vect);
}

static void randomize_rays(ray_t *rays, size_t nb_rays, vect3_t const *bbox_center)
{
  vect3_t v_zero = VECT3_ZERO;
  vect3_t v_origin;
  vect3_t ray_dir_bias;
  float bias_phi;
  float bias_theta;

  VECT3_INIT(v_origin, 2.f, 2.f, 2.f);
  VECT3_SUB(ray_dir_bias, *bbox_center, v_origin);
  VECT3_NORM(ray_dir_bias);
  bias_phi = atan2f(ray_dir_bias.y, ray_dir_bias.x);
  bias_theta = acosf(ray_dir_bias.z / VECT3_LENGTH(ray_dir_bias));
  for (size_t i = 0; i < nb_rays; ++i) {
    rays[i].org = v_origin;
    generate_vector_within_sphere(&rays[i].dir, &v_zero, 1.f, 0.0f, bias_phi, bias_theta);
  }
}

static void run_test(
    char *hits,
    bbox_ray_inter_callback_t callback,
    bbox_aa_t const *bbox,
    ray_t const *rays,
    size_t nb_rays)
{
  vect3_t dummy = VECT3_ZERO;
  size_t miss = 0;
  size_t hit = 0;

  for (size_t i = 0; i < nb_rays; ++i) {
    if (callback(&dummy, bbox, &rays[i])) {
      hits[i] = 1;
      hit += 1;
    } else {
      hits[i] = 0;
      miss += 1;
    }
  }
  printf("%zu hits, %zu miss ; %.3f hit rate\n",
         hit, miss, ((double)hit) / ((double)(nb_rays)));
}

static void print_usage(char const *pname)
{
  printf("Usage: %s [NB_RAYS]\n", pname);
}

int main(int ac, char **av)
{
  bbox_aa_t bbox = {0};
  size_t nb_rays = 1000000UL;
  ray_t *rays;

  // parse args
  if (ac == 1) {
    ; // pass
  } else if (ac == 2) {
    // parse nb rays
    unsigned long int res;
    char *end = NULL;

    res = strtoul(av[1], &end, 10);
    if (end == NULL || *end != '\0') {
      perror("Error parsing integer");
      print_usage(av[0]);
      return 2;
    }
    if (res <= 0 || res > ULONG_MAX) {
      printf("Invalid number of ray to try, should be within [1, %zu]\n", ULONG_MAX);
      return 2;
    }
    nb_rays = (size_t)res;
  } else {
    print_usage(av[0]);
    return 1;
  }

  // init seed
  srand48(generate_seed());

  // init bbox
  generate_aa_bbox(&bbox);
  BBOX_AA_PRINTF("bbox", bbox);

  #if 0
  {
    vect3_t c = VECT3_ZERO;
    vect3_t o;
    vect3_t v;
    vect3_t d;
    float phi;
    float theta;

    VECT3_INIT(o, 2, 2, 2);
    VECT3_SUB(d, c, o);
    theta = acosf(d.z / VECT3_LENGTH(d));
    phi = atan2f(d.y, d.x);
    VECT3_PRINTF("d", d);
    generate_vector_within_sphere(&v, &c, 1.0f, 1.f, phi, theta);
    return 0;
  }
  #endif

  // allocate and init rays
  {
    my_clock_t init_timer;
    vect3_t bbox_center;

    bbox_aa_compute_center(&bbox_center, &bbox);
    clock_init(&init_timer);
    rays = xmalloc(sizeof(*rays) * nb_rays);
    randomize_rays(rays, nb_rays, &bbox_center);
    printf("Initialization done in %.3fs\n", clock_get_elapsed_seconds(&init_timer));
  }

  // run test
  {
    char *hits1, *hits2;
    my_clock_t test_timer;
    double test_time;
    double rps;

    hits1 = xmalloc(sizeof(*hits1) * nb_rays);
    hits2 = xmalloc(sizeof(*hits2) * nb_rays);

    clock_init(&test_timer);
    run_test(hits1, bbox_aa_ray_intersects, &bbox, rays, nb_rays);
    test_time = clock_get_elapsed_seconds(&test_timer);
    rps = ((double)nb_rays) / test_time;
    printf("%zu rays in %.3fs ; %.3frps\n", nb_rays, test_time, rps);

    clock_init(&test_timer);
    run_test(hits2, bbox_aa_ray_intersects2, &bbox, rays, nb_rays);
    test_time = clock_get_elapsed_seconds(&test_timer);
    rps = ((double)nb_rays) / test_time;
    printf("%zu rays in %.3fs ; %.3frps\n", nb_rays, test_time, rps);

    {
      size_t matches = 0;

      for (size_t i = 0; i < nb_rays; ++i) {
        matches += (hits1[i] == hits2[i] ? 1 : 0);
      }
      printf("%zu (%.2f)%% hits matches\n", matches, (float)(matches) / (float)(nb_rays) * 100.f);
    }

    free(hits1);
    free(hits2);
  }

  // cleanup
  free(rays);
  return 0;
}
