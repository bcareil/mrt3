import re
import sys

r = re.compile(r"([01]\.[0-9]+)[^c]*cache hit")
v = [float(m.group(1)) for m in [r.search(l) for l in sys.stdin] if m is not None]
print(v)
print("Average: %f" % (sum(v) / len(v)))

