IT:=256
SOURCE_DIR=$(PWD)
BUILD_DIR=../MRT3_BUILD_RELEASE
BINARY=mrt3

CMAKE=cmake
CMAKE_OPTIONS=-G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

MKDIR=mkdir -p

SCENE:=stanford-dragon.obj

.PHONY: all build run clean valgrind

all: build

$(BUILD_DIR):
	[ -d $(@) ] || $(MKDIR) $(@)

$(BUILD_DIR)/CMakeCache.txt: $(BUILD_DIR) $(SOURCE_DIR)/CMakeLists.txt
	cd $(BUILD_DIR) && $(CMAKE) $(CMAKE_OPTIONS) $(SOURCE_DIR) 


$(BUILD_DIR)/$(BINARY): $(BUILD_DIR)/CMakeCache.txt $(BUILD_DIR)/build.ninja
	cd $(BUILD_DIR) && $(CMAKE) --build .

build: $(BUILD_DIR)/$(BINARY)

clean:
	$(RM) -r $(BUILD_DIR)

run: $(BUILD_DIR)/$(BINARY)
	$(BUILD_DIR)/$(BINARY) $(SCENE) -n $(IT)

valgrind: $(BUILD_DIR)/$(BINARY)
	valgrind --tool=callgrind $(BUILD_DIR)/$(BINARY) $(SCENE) -n 1 -q

